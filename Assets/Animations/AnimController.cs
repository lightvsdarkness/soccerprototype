﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace Soccer {
    public class AnimController : MonoBehaviour {
        //[Header("Input")]
        //public const int MiddleMouseButton = 2;

        [Header("Animator Params")]
        [SerializeField] private Animator _animator;
        private float __speed;
        private float __Direction;
        private float __Sprinting;
        private float __jump;
        private bool __att;

        [Header("Links")]
        public PhotonView _PhotonView;
        public FootballPlayer _FootballPlayer;

        public void Start() {
            if(_animator == null)
            _animator = GetComponent<Animator>();
            if (_FootballPlayer == null) {
                _FootballPlayer.GetComponentInParent<FootballPlayer>();
            }

            if (_PhotonView == null)
                _PhotonView = GetComponentInParent<PhotonView>();

            _FootballPlayer._FootballNetPlayer.ShouldWatchReplayChanged += SwitchAnimatorState;
        }

        private void OnDestroy() {
            _FootballPlayer._FootballNetPlayer.ShouldWatchReplayChanged -= SwitchAnimatorState;
        }

        public void Update() {
            if (!_PhotonView.IsMine || !MasterManager.GameSettings.IsGamePlayed) return;

            __speed = Input.GetAxis("Vertical");
            __Direction = Input.GetAxis("Horizontal");
            Sprinting();
            KickHeadAnim();
            Fire();
        }

        // TODO: ACHTUNG: Animation's Input should be in PlayerInput
        public void FixedUpdate() {
            if (!_PhotonView.IsMine || !MasterManager.GameSettings.IsGamePlayed) return;
            if (_FootballPlayer.State == FootballPlayerState.AdminAssumedControl &&
                !_FootballPlayer._PlayerInput.AdminAssumedControlInputModeMoving) return;

            _animator.SetFloat("Speed", __speed);
            _animator.SetFloat("Direction", __Direction);
            _animator.SetFloat("Sprinting", __Sprinting);
            _animator.SetFloat("Jump", __jump);
            _animator.SetBool("Attack", __att);
        }

        public void Sprinting() {
            if (Input.GetKey(KeyCode.LeftShift)) {
                __Sprinting = 1.0f;
            }
            else {
                __Sprinting = 0.0f;
            }
        }

        public void KickHeadAnim() {
            if (Input.GetMouseButton(_FootballPlayer._PlayerInput.KickHeadMouseButton)) {
                __jump = 1.0f;
            }
            else {
                __jump = 0.0f;
            }
        }

        public void Fire() {
            if (!MasterManager.GameSettings.MenuOpened && Input.GetMouseButton(0)) {
                __att = true;
            }
            else {
                __att = false;
            }
        }

        public void SwitchAnimatorState(bool state) {
            _animator.enabled = state;
        }
    }
}