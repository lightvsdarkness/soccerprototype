﻿using ExitGames.Client.Photon;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SerializePlayer : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private SoccerNetPlayer _customSerialization = new SoccerNetPlayer();
    [SerializeField]
    private bool _sendAsTyped = true;

    private void Start() {
        PhotonPeer.RegisterType(typeof(SoccerNetPlayer), (byte)'M', SoccerNetPlayer.Serialize, SoccerNetPlayer.Deserialize);
    }

    private void Update() {
        if (_customSerialization.RoleInt != -1)
        {
            SendCustomSerialization(_customSerialization, _sendAsTyped);
            _customSerialization.RoleInt = -1;
            _customSerialization.Nickname = string.Empty;
        }
    }

    /// <summary>
    /// Sends an instance of MyCustomSerialization.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="typed"></param>
    private void SendCustomSerialization(SoccerNetPlayer data, bool typed) {
        if (!typed)
            base.photonView.RPC("RPC_ReceiveMyCustomSerialization", RpcTarget.AllViaServer, SoccerNetPlayer.Serialize(_customSerialization));
        else
            base.photonView.RPC("RPC_TypedReceiveMyCustomSerialization", RpcTarget.AllViaServer, _customSerialization);
    }

    /// <summary>
    /// Receives MyCustomSerialization as a byte array.
    /// </summary>
    /// <param name="datas"></param>
    [PunRPC]
    private void RPC_ReceiveMyCustomSerialization(byte[] datas) {
        SoccerNetPlayer result = (SoccerNetPlayer)SoccerNetPlayer.Deserialize(datas);
        print("Received byte array: " + result.RoleInt + ", " + result.Nickname);
    }

    /// <summary>
    /// Receives MyCustomSerialization as a type.
    /// </summary>
    /// <param name="datas"></param>
    [PunRPC]
    private void RPC_TypedReceiveMyCustomSerialization(SoccerNetPlayer datas) {
        print("Received typed: " + datas.RoleInt + ", " + datas.Nickname);
    }


}
