﻿using System;
using System.Linq;
using System.Text;

[System.Serializable]
public class SoccerNetPlayer
{
    public int RoleInt = -1;
    public string Nickname = string.Empty;

    public static byte[] Serialize(object obj)
    {
        //MyNumber.
        SoccerNetPlayer data = (SoccerNetPlayer)obj;
        byte[] myNumberBytes = BitConverter.GetBytes(data.RoleInt);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(myNumberBytes);
        //MyString.
        byte[] myStringBytes = Encoding.ASCII.GetBytes(data.Nickname);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(myStringBytes);

        return JoinBytes(myNumberBytes, myStringBytes);
    }

    public static object Deserialize(byte[] bytes)
    {
        SoccerNetPlayer data = new SoccerNetPlayer();
        //MyNumber.
        byte[] myNumberBytes = new byte[4];
        Array.Copy(bytes, 0, myNumberBytes, 0, myNumberBytes.Length);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(myNumberBytes);
        data.RoleInt = BitConverter.ToInt32(myNumberBytes, 0);

        //MyString.
        byte[] myStringBytes = new byte[bytes.Length - 4];
        if (myStringBytes.Length > 0)
        {
            Array.Copy(bytes, 4, myStringBytes, 0, myStringBytes.Length);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(myStringBytes);
            data.Nickname = Encoding.UTF8.GetString(myStringBytes);
        }
        else
        {
            data.Nickname = string.Empty;
        }

        return data;
    }


    private static byte[] JoinBytes(params byte[][] arrays)
    {
        byte[] rv = new byte[arrays.Sum(a => a.Length)];
        int offset = 0;
        foreach (byte[] array in arrays)
        {
            System.Buffer.BlockCopy(array, 0, rv, offset, array.Length);
            offset += array.Length;
        }
        return rv;
    }

}