﻿using System;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Soccer {
    public class PlayerListingsMenu : MonoBehaviourPunCallbacks {
        public GameObject GameplayUI;

        [SerializeField] private Transform _content;
        [SerializeField] private PlayerListingItem _playerListingItemAdmin;
        [SerializeField] private PlayerListingItem _playerListingItem;
        [SerializeField] private Text _readyUpText;

        public List<PlayerListingItem> _listings = new List<PlayerListingItem>();
        //private RoomsCanvases _roomsCanvases;
        [Space] private bool _ready = false;

        public Action EventShowGameStats;


        public override void OnEnable() {
            base.OnEnable();
            SetReadyUp(false);
            TurnOffGameplayUI();

            //GetCurrentRoomPlayers();
        }

        public override void OnDisable() {
            Clear();
            base.OnDisable();

            PhotonRoomMatch.I.EventStartGame -= Initialize;
        }

        public void Start() {
            PhotonRoomMatch.I.EventStartGame += Initialize;
        }

        /// <summary>
        /// NOTE: Input
        /// </summary>
        public void Update() {
            if (!PhotonNetwork.InRoom || !PhotonRoomMatch.I.InRoom) return;

        }

        public void SwitchPlayerListingMenu() {
            GetCurrentRoomPlayers();
            SwitchGameplayUIState(GameplayUI);

            EventShowGameStats?.Invoke();
        }

        private void Clear() {
            for (int i = 0; i < _listings.Count; i++)
                Destroy(_listings[i].gameObject);

            _listings.Clear();
        }

        private void Initialize() {
            //_roomsCanvases = canvases;
        }

        private void SetReadyUp(bool state) {
            _ready = state;
            //if (_ready)
            //    _readyUpText.text = "R";
            //else
            //    _readyUpText.text = "N";
        }

        [ContextMenu("GetCurrentRoomPlayers")]
        public void GetCurrentRoomPlayers() {
            if (!PhotonNetwork.IsConnected) return;
            if (PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.Players == null) return;
            Debug.LogWarning("PlayerListingsMenu. GetCurrentRoomPlayers");

            //foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players) {
            //    AddPlayerListing(playerInfo.Value);
            //}

            Clear();
            PhotonRoomMatch.I.GetFootballNetPlayers();  // TODO: NOTE: Checking for now

            foreach (var player in PhotonRoomMatch.I.FootballNetPlayers) {
                AddPlayerListing(player);
            }
        }

        private void AddPlayerListing(FootballNetPlayer player) {
            int index = _listings.FindIndex(x => x.Player == player);
            if (index != -1) {
                _listings[index].SetPlayerInfo(player);
            }
            else {
                PlayerListingItem listing;
                if (PhotonNetwork.LocalPlayer.IsMasterClient) {
                    listing = Instantiate(_playerListingItemAdmin, _content);
                }
                else
                    listing = Instantiate(_playerListingItem, _content);

                if (listing != null) {
                    listing.SetPlayerInfo(player);
                    _listings.Add(listing);
                }
            }
        }

        public override void OnMasterClientSwitched(Player newMasterClient) {
            //_roomsCanvases.CurrentRoomCanvas.LeaveRoomMenu.OnClick_LeaveRoom();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newPlayer"></param>
        public override void OnPlayerEnteredRoom(Player newPlayer) {
            //AddPlayerListing(newPlayer);
            GetCurrentRoomPlayers();
        }

        //public override void OnPlayerLeftRoom(Player otherPlayer)
        //{
        //    int index = _listings.FindIndex(x => x.Player == otherPlayer);
        //    if (index != -1)
        //    {
        //        Destroy(_listings[index].gameObject);
        //        _listings.RemoveAt(index);
        //    }
        //}

        public void OnClick_StartGame() {
            if (PhotonNetwork.IsMasterClient) {
                //for (int i = 0; i < _listings.Count; i++)
                //{
                //    if (_listings[i].Player != PhotonNetwork.LocalPlayer)33
                //    {
                //        if (!_listings[i].Ready)
                //            return;
                //    }
                //}

                //PhotonNetwork.CurrentRoom.IsOpen = false;
                //PhotonNetwork.CurrentRoom.IsVisible = false;
                PhotonNetwork.LoadLevel(1);
            }
        }

        public void OnClick_ReadyUp() {
            if (!PhotonNetwork.IsMasterClient) {
                SetReadyUp(!_ready);
                base.photonView.RPC("RPC_ChangeReadyState", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer, _ready);
            }
        }

        [PunRPC]
        private void RPC_ChangeReadyState(FootballNetPlayer player, bool ready) {
            int index = _listings.FindIndex(x => x.Player == player);
            if (index != -1)
                _listings[index].Ready = ready;
        }

        public void SwitchGameplayUIState(GameObject panel) {
            panel.SetActive(!panel.activeSelf);

        }

        public void TurnOffGameplayUI() {
            GameplayUI.SetActive(false);
        }
    }
}