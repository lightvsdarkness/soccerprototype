﻿using System;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Soccer {
    public class PlayersWhoWatchReplayListingsMenu : MonoBehaviourPunCallbacks {
        public bool Debugging;

        public GameObject UIContainer;

        [SerializeField] private Transform _content;
        [SerializeField] private PlayerWhoWatchesReplayListingItem _playerListingItem;

        [Space] public List<PlayerWhoWatchesReplayListingItem> Listing = new List<PlayerWhoWatchesReplayListingItem>();

        public Dictionary<FootballNetPlayer, bool> PlayersDictionary = new Dictionary<FootballNetPlayer, bool>();


        //private bool _ready = false;

        //public Action EventShowGameStats;


        public override void OnEnable() {
            base.OnEnable();

            //TurnOffGameplayUI();

            //GetCurrentRoomPlayers();
        }

        public override void OnDisable() {
            base.OnDisable();

            Clear();
            //PhotonRoomMatch.I.EventStartGame -= Initialize;
        }

        public void Start() {
            //PhotonRoomMatch.I.EventStartGame += Initialize;
        }

        //public void Update() {
        //    if (!PhotonNetwork.InRoom || !PhotonRoomMatch.I.InRoom) return;

        //}


        public void SwitchReplayPlayerListingAdmin() {
            //Debug.LogWarning("PlayersWhoWatchReplayListingsMenu. MasterManager.GameSettings.MyFootballNetPlayer.RoleByte: " + MasterManager.GameSettings.MyFootballNetPlayer.RoleByte, this);
            //Debug.LogWarning("PlayersWhoWatchReplayListingsMenu. MasterManager.GameSettings._GameMode == GameSettings.GameMode.Pause: " + (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Pause), this);


            if (MasterManager.GameSettings.MyFootballNetPlayer.RoleByte == 0 &&
                MasterManager.GameSettings._GameMode == GameSettings.GameMode.Pause)
                // PhotonNetwork.LocalPlayer.IsMasterClient
            {
                Debug.LogWarning("PlayersWhoWatchReplayListingsMenu. SwitchReplayPlayerListingAdmin +");
                UIContainer.SetActive(true);
                GetCurrentRoomPlayers();
            }
            else {
                UIContainer.SetActive(false);
            }
        }

        private void Clear() {
            for (int i = 0; i < Listing.Count; i++)
                Destroy(Listing[i].gameObject);

            Listing.Clear();
        }

        private void Initialize() {
            //_roomsCanvases = canvases;
        }

        public void GamesPaused() {
            SwitchReplayPlayerListingAdmin();
        }

        public void GamesUnPaused() {
            SwitchReplayPlayerListingAdmin();
        }

        [ContextMenu("GetCurrentRoomPlayers")]
        public void GetCurrentRoomPlayers() {
            if (!PhotonNetwork.IsConnected) return;
            if (PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.Players == null) return;
            if (Debugging)
                Debug.LogWarning("PlayerListingsMenu. GetCurrentRoomPlayers");

            //foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players) {
            //    AddPlayerListing(playerInfo.Value);
            //}

            Clear();
            foreach (var player in PhotonRoomMatch.I.FootballNetPlayers) {
                AddPlayerListing(player);
            }
        }

        private void AddPlayerListing(FootballNetPlayer player) {
            //if(PlayersDictionary)
            FootballNetPlayer playerInDict =
                PlayersDictionary.Keys.FirstOrDefault(x => x._FootballPlayer._FootballNetPlayer == player);
            var index = Listing.FindIndex(x => x.NetPlayer == player);

            if (playerInDict == null) {
                PlayersDictionary.Add(player, true);
            }


            if (index != -1) {
                Listing[index].SetPlayerInfo(this, player);
            }
            else {
                PlayerWhoWatchesReplayListingItem listing = Instantiate(_playerListingItem, _content);

                if (listing != null) {
                    listing.SetPlayerInfo(this, player);
                    Listing.Add(listing);

                }
            }
        }

        public override void OnMasterClientSwitched(Player newMasterClient) {
            //_roomsCanvases.CurrentRoomCanvas.LeaveRoomMenu.OnClick_LeaveRoom();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="newPlayer"></param>
        public override void OnPlayerEnteredRoom(Player newPlayer) {
            //AddPlayerListing(newPlayer);
            GetCurrentRoomPlayers();
        }

        //public override void OnPlayerLeftRoom(Player otherPlayer)
        //{
        //    int index = _listings.FindIndex(x => x.Player == otherPlayer);
        //    if (index != -1)
        //    {
        //        Destroy(_listings[index].gameObject);
        //        _listings.RemoveAt(index);
        //    }
        //}




        [PunRPC]
        private void RPC_ChangeWatchState(FootballNetPlayer player, bool ready) {
            int index = Listing.FindIndex(x => x.NetPlayer == player);
            //if (index != -1)
            //    _listings[index].Ready = ready;
        }

        //public void SwitchGameplayUIState(GameObject panel) {
        //    panel.SetActive(!panel.activeSelf);

        //}
        //public void TurnOffGameplayUI() {
        //    UIContainer.SetActive(false);
        //}
    }
}