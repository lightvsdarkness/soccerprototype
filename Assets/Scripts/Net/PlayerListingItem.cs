﻿using System;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Soccer {
    public class PlayerListingItem : MonoBehaviour {
        public string BoardName;

        [SerializeField] private TextMeshProUGUI roleText;
        [SerializeField] private TMP_Dropdown _roleDropdown;

        [SerializeField] private TMP_Dropdown _teamDropdown;
        [SerializeField] private TextMeshProUGUI _teamText;

        [SerializeField] private TMP_InputField _soccerNumberInputField;
        [SerializeField] private TextMeshProUGUI _soccerNumberText;

        [SerializeField] private Toggle _goalkeeperToggle;

        [SerializeField] private TextMeshProUGUI _NameText;

        public FootballNetPlayer _player;

        public FootballNetPlayer Player {
            get { return _player; }
            private set { _player = value; }
        }

        public bool Ready = false;



        public void SetPlayerInfo(FootballNetPlayer player) {
            Player = player;

            //int result = -1;
            //if (player.CustomProperties.ContainsKey("RandomNumber"))
            //    result = (int)player.CustomProperties["RandomNumber"];
            //_text.text = result.ToString() + ", " + player.NickName;

            List<string> strList = player.Nickname.Split(new char[] {'_'}).ToList();
            string visibleName = "";
            //foreach (var partOfName in strList) {
            //    visibleName += partOfName;
            //}
            for (int i = 0; i < strList.Count - 1; i++) {
                visibleName += strList[i];
            }
            
            _NameText.text = visibleName;

            switch (_player.RoleByte) {
                case 0:
                    if (roleText != null)
                        roleText.text = "Admin";
                    if (_roleDropdown != null)
                        _roleDropdown.value = 0;
                    TurnOffTeamstuff();
                    break;
                case 1:
                    if (roleText != null)
                        roleText.text = "Player";
                    if (_roleDropdown != null)
                        _roleDropdown.value = 1;
                    TurnOnTeamstuff();
                    break;
                case 2:
                    if (roleText != null)
                        roleText.text = "Ghost";
                    if (_roleDropdown != null)
                        _roleDropdown.value = 2;
                    TurnOffTeamstuff();
                    break;
                case 3:
                    if (roleText != null)
                        roleText.text = "Ghost";
                    if (_roleDropdown != null)
                        _roleDropdown.value = 2;
                    TurnOffTeamstuff();
                    break;
                default:
                    if (roleText != null)
                        roleText.text = "AdminControlled";
                    if (_roleDropdown != null)
                        _roleDropdown.value = 3;
                    TurnOffTeamstuff();
                    break;
            }

            switch (_player.Team) {
                case 0:
                    if (_teamText != null && _player.RoleByte == 1)
                        _teamText.text = PhotonRoomMatch.I.Team1Name;
                    if (_teamDropdown != null)
                        _teamDropdown.value = 0;
                    break;
                case 1:
                    if (_teamText != null && _player.RoleByte == 1)
                        _teamText.text = PhotonRoomMatch.I.Team2Name;
                    if (_teamDropdown != null)
                        _teamDropdown.value = 1;
                    break;
                default:
                    if (_teamText != null)
                        _teamText.text = "";
                    break;
            }

            if (_soccerNumberInputField != null)
                _soccerNumberInputField.text = _player.Number.ToString();
            if (_soccerNumberText != null)
                _soccerNumberText.text = _player.Number.ToString();


            _goalkeeperToggle.isOn = _player.IsGoalkeeper;


        }

        private void TurnOffTeamstuff() {
            //teamText.enabled = false;
            if (_teamDropdown != null)
                _teamDropdown.interactable = false; //gameObject.SetActive(false);
            if (_teamText != null)
                _teamText.color = Color.white;

            if (_soccerNumberInputField != null) {
                //_soccerNumberInputField.color = Color.white;
                _soccerNumberInputField.interactable = false;
            }
            if (_soccerNumberText != null)
                _soccerNumberText.color = Color.white;

            if (_goalkeeperToggle != null)
                _goalkeeperToggle.interactable = false; //gameObject.SetActive(false);
        }

        private void TurnOnTeamstuff() {
            if (_teamDropdown != null)
                _teamDropdown.interactable = true;
            if (_teamText != null)
                _teamText.color = Color.black;

            if (_soccerNumberInputField != null) {
                //_soccerNumberInputField.color = Color.black;
                _soccerNumberInputField.interactable = true;
            }
            if (_soccerNumberText != null)
                _soccerNumberText.color = Color.black;

            if (_goalkeeperToggle != null)
                _goalkeeperToggle.interactable = true; //gameObject.SetActive(true);
        }

        public void SetPlayerRole(int roleIndex) {
            if (Player.RoleByte == 0) return; // Admin can't deny being Admin

            Player.RoleByte = (byte) roleIndex;
            Debug.Log("PlayerListing. Role: " + (GameSettings.Role) roleIndex);

            var photonPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == _player.Nickname);
            if (photonPlayer != null)
                _player._PhotonView.RPC("RPC_ChangeFNPRole", photonPlayer, Player.RoleByte);
                    // NOTE: change to photonView?
            else {
                PhotonRoomMatch.I.GetFootballNetPlayers();

                photonPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == _player.Nickname);
                if (photonPlayer != null)
                    _player._PhotonView.RPC("RPC_ChangeFNPRole", photonPlayer, Player.RoleByte);
                else
                    Debug.LogError("PlayerListing. SetPlayerRole: photonPlayer is null");
            }


        }

        public void SetPlayerTeam(int teamIndex) {
            Player.Team = (byte) teamIndex;
            Debug.Log("PlayerListing. Team: " + teamIndex);

            var photonPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == _player.Nickname);
            _player._PhotonView.RPC("RPC_ChangeFNPTeam", photonPlayer, Player.Team); // NOTE: change to photonView?

        }

        public void SetPlayerNumber() {
            string playerNumberString = _soccerNumberInputField.text;
            int charCount = playerNumberString.ToCharArray().Count();
            if (charCount > 2) {
                playerNumberString =  playerNumberString.Substring(playerNumberString.Length - 2);
                _soccerNumberInputField.text = playerNumberString;
            }
            Player.Number = (byte) Byte.Parse(playerNumberString);
            Debug.Log("PlayerListing. PlayerNumber: " + playerNumberString);

            var photonPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == _player.Nickname);
            _player._PhotonView.RPC("RPC_ChangeFNPNumber", photonPlayer, Player.Number);
                // NOTE: change to photonView?

        }

        public void SetIsGoalkeeper(bool state) {
            Player.IsGoalkeeper = state;
            Debug.Log("PlayerListing. IsGoalkeeper: " + state);

            var photonPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == _player.Nickname);
            _player._PhotonView.RPC("RPC_ChangeFNPIsGoalkeeper", photonPlayer, Player.IsGoalkeeper);
                // NOTE: change to photonView?

            // Visuals
            if (_goalkeeperToggle.isOn) {
                //_goalkeeperAnimator.Play("ToggleOn");
            }
            else {
                //_goalkeeperAnimator.Play("ToggleOff");
            }
        }

        public void ButtonAssumeControl() {
            Debug.Log("PlayerListingItem. Assuming Control");
        }
    }
}