﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IG;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice.Unity;
using Soccer;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotonRoomMatch : SingletonMonoBehaviourPunCallbacks<PhotonRoomMatch>, IInRoomCallbacks {
    public PhotonView _PhotonView;

    //public FootballPlayer LocalFootballPlayer;

    public bool InRoom;
    public bool IsGameLoaded;
    public int CurrentScene; // In what scene we are playing now

    public int Team1Score;
    public int Team2Score;
    public string Team1Name;
    public string Team2Name;

    //[SerializeField] private TextMeshProUGUI Team1Menu;
    //[SerializeField] private TextMeshProUGUI Team2Menu;
    [SerializeField] private TMP_InputField Team1Menu;
    [SerializeField] private TMP_InputField Team2Menu;

    public Transform ChoosingModelPoint = null;
    public List<Transform> SpawnChoosingPoints = new List<Transform>(); 
    public List<Transform> SpawnPlayingPoints = new List<Transform>(); 

    // Player info
    public Player[] PhotonPlayers;
    public int PlayersInRoom;
    public int MyNumberInRoom;

    public int PlayersInGame;

    public bool readyToStart = false;

    public Room CurrentRoom;
    public string CurrentRoomName;

    // References
    public Ball _Ball;
    public List<FootballNetPlayer> FootballNetPlayers = new List<FootballNetPlayer>();
    public List<FootballPlayer> FootballPlayers = new List<FootballPlayer>();
    public FootballPlayer AdminFootballPlayer = null;

    [Space]
    [SerializeField] private Recorder _recorder;

    // Actions
    public Action EventStartGame;
    public Action EventEndGame;


    private void Start() {
        if (_PhotonView == null)
            _PhotonView = GetComponent<PhotonView>();

        if (ChoosingModelPoint == null)
            ChoosingModelPoint = transform;

        if (_Ball == null)
            _Ball = FindObjectOfType<Ball>();

        //if (PlayerPrefsUtils.LoadString(PlayerPrefsUtils.LastTeam1Name) != string.Empty)
        Team1Menu.text = PlayerPrefsUtils.LoadString(PlayerPrefsUtils.LastTeam1Name);
        //if (PlayerPrefsUtils.LoadString(PlayerPrefsUtils.LastTeam2Name) != string.Empty)
        Team2Menu.text = PlayerPrefsUtils.LoadString(PlayerPrefsUtils.LastTeam2Name);

        _recorder.ReactOnSystemChanges = true;
    }


    public override void OnEnable() {
        base.OnEnable();

        PhotonNetwork.AddCallbackTarget(this);
        //SceneManager.sceneLoaded += OnSceneFinishedLoading;
        //PhotonNetwork.CurrentRoom.IsOpen = false;
    }
    public override void OnDisable() {
        base.OnDisable();

        PhotonNetwork.RemoveCallbackTarget(this);
        //SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }
    private void Update() {
        // NOTE: Optimize
        CurrentRoom = PhotonNetwork.CurrentRoom;
        if (CurrentRoom != null)
            CurrentRoomName = CurrentRoom.ToString();
    }


    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode) {
        CurrentScene = scene.buildIndex;
        if (CurrentScene == MasterManager.GameSettings.MultiplayerScene1) {
            IsGameLoaded = true;
            //if(GameProperties.MultiplayerSetting.DelayStart) {
            //_PhotonView("RPC_LoadedGameScene", RpcTarget.MasterClient);
            //}
            //else
            //CreatePlayer();
        }
    }

    [PunRPC]
    private void RPC_LoadedGameScene() {
        //PlayersInGame++;
        //if(PlayersInGame == PhotonNetwork.PlayerList.Length) {
        _PhotonView.RPC("RPC_CreatePlayer", RpcTarget.All);
        //}
    }


    public override void OnJoinRoomFailed(short returnCode, string message) {
        Debug.Log("PhotonRoomMatch. OnJoinRoomFailed:" + returnCode + " " + message);
        //CreateRoom();
    }
    //public override void OnJoinedRoom() {
    //    //base.OnJoinedRoom();
    //    Debug.Log("PhotonRoomMatch. OnJoinedRoom");

    //    InRoom = true;
    //    PhotonPlayers = PhotonNetwork.PlayerList;
    //    PlayersInRoom = PhotonPlayers.Length;
    //    MyNumberInRoom = PlayersInRoom;
    //    //PhotonNetwork.NickName = PhotonLobby.I.InputFieldUsername.text;

    //    //if (!PhotonNetwork.IsMasterClient) return;

    //    StartGame();
    //    CreatePlayer();
    //}
    public override void OnJoinedRoom() {
        //base.OnJoinedRoom();
        Debug.Log("PhotonRoomMatch. OnJoinedRoom");
        CreatePlayer();

        InRoom = true;
        //if (!PhotonNetwork.IsMasterClient) return;

        //var _soccerNetPlayer = new SoccerNetPlayer();
        //_soccerNetPlayer.RoleInt = (int)GameSettings.Role.Administrator;
        //_soccerNetPlayer.Nickname = MasterManager.GameSettings.Nickname;
        //SoccerNetPlayers.Add(_soccerNetPlayer);
        if (PhotonNetwork.IsMasterClient)
            RefreshTeamNames();
    }

    private void CreatePlayer() {
        var player = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Photon_FPPlayer"), SpawnChoosingPoints[0].position,
            SpawnChoosingPoints[0].rotation, 0);
        player.name = "FPlayer_" + PhotonNetwork.LocalPlayer.NickName;
        MasterManager.GameSettings.MyFootballPlayer = player.GetComponent<FootballPlayer>();

        var footballPlayer = MasterManager.GameSettings.MyFootballPlayer._FootballNetPlayer;
        footballPlayer.Nickname = PhotonNetwork.LocalPlayer.NickName;

        if (PhotonNetwork.IsMasterClient) {
            footballPlayer.RoleByte = (int)GameSettings.Role.Administrator;

        }
        else {
            footballPlayer.Team = (byte)UnityEngine.Random.Range(0, 2);
            footballPlayer.RoleByte = (int)GameSettings.Role.Player;
        }
        MasterManager.GameSettings.MyFootballNetPlayer = footballPlayer;
        MasterManager.GameSettings.InitMatchStart();

        Invoke("GetFootballNetPlayers", 0.1f);
    }
    

    public void GetFootballNetPlayers() {
        FootballPlayers = FindObjectsOfType<FootballPlayer>().ToList();
        FootballNetPlayers = FindObjectsOfType<FootballNetPlayer>().ToList();

        for (int i = 0; i < ReplayManager.I.ReplayRecorders.Count; i++) {
            if(ReplayManager.I.ReplayRecorders[i] == null)
                ReplayManager.I.ReplayRecorders.RemoveAt(i);
        }
        if(AdminFootballPlayer == null)
            AdminFootballPlayer = FootballPlayers.Find(x => x._FootballNetPlayer.RoleByte == 0);

        if (Debugging) Debug.Log("PhotonRoomMatch. GetFootballNetPlayers");
    }

    // Creates Player network controller (but not player character prefab)
    //    private void CreatePlayer(Player newPlayer) {
    //        var player = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Photon_FPPlayer"), transform.position,
    //Quaternion.identity, 0);
    //        //_PhotonView.RPC("RPC_AddPlayer", RpcTarget.AllBuffered);
    //        //RPC_AddPlayer(newPlayer);

    //        FootballPlayers = FindObjectsOfType<FootballPlayer>().ToList();

    //        var _soccerNetPlayer = player.GetComponent<SoccerNetPlayer>();
    //        _soccerNetPlayer.PhotonPlayer = newPlayer;
    //        if(newPlayer.IsMasterClient)
    //            _soccerNetPlayer.Role = GameSettings.Role.Administrator;
    //        else
    //        {
    //            _soccerNetPlayer.Role = GameSettings.Role.Player;
    //        }
    //        SoccerNetPlayers.Add(_soccerNetPlayer);
    //    }

    // Вызывается на Мастере, когда другой игрок заходит. Стоп. Нет.
    // Наверное, вызывается на всех других машинах, когда другой игрок заходит
    public override void OnPlayerEnteredRoom(Player newPlayer) {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("PhotonRoomMatch. OnPlayerEnteredRoom");
        //if (!PhotonNetwork.IsMasterClient) return;

        PhotonPlayers = PhotonNetwork.PlayerList;
        PlayersInRoom = PhotonPlayers.Length;
        MyNumberInRoom = PlayersInRoom;
        //PhotonNetwork.NickName = PhotonLobby.I.InputFieldUsername.text;

        //var _soccerNetPlayer = new FootballNetPlayer();
        //_soccerNetPlayer.RoleInt = (int)GameSettings.Role.Player;
        //_soccerNetPlayer.Nickname = newPlayer.NickName;
        //SoccerNetPlayers.Add(_soccerNetPlayer);

        
        Invoke("GetFootballNetPlayers", 0.2f);

        //object[] players = SoccerNetPlayers.ToArray();
        // NOTE: COOOOOL!
        //_PhotonView.RPC("RPC_AddPlayer", RpcTarget.AllViaServer, players);
        //_PhotonView.RPC("RPC_AddPlayer", RpcTarget.AllViaServer);
    }


    //[PunRPC]
    //public void RPC_AddPlayer() {
    //    if (PhotonNetwork.IsMasterClient) return;


    //    FootballPlayers = FindObjectsOfType<FootballPlayer>().ToList();
    //}
    [PunRPC]
    public void RPC_AddPlayer(object[] players) {
        if (PhotonNetwork.IsMasterClient) return;

        FootballNetPlayers.Clear();
        for (int i = 0; i < players.Length; i++)
        {
            FootballNetPlayers.Add((FootballNetPlayer)players[i]);
        }

        FootballPlayers = FindObjectsOfType<FootballPlayer>().ToList();
    }

    public void ButtonStartGame() {
        if (!PhotonNetwork.IsMasterClient) return;

        //if(GameProperties.MultiplayerSetting.DelayStart) {
        //PhotonNetwork.LoadLevel(GameSettings.MultiplayerScene1);
        //}
        //CreatePlayers();
        
        _PhotonView.RPC("RPC_StartGame", RpcTarget.AllViaServer);
    }

    [PunRPC]
    public void RPC_StartGame() {
        MasterManager.GameSettings.InitMatchStart();
        EventStartGame?.Invoke();
    }


    //[PunRPC]
    //public void RPC_AddPlayer(Player newPlayer) {
    //    //var soccerNetPlayer = player.GetComponent<SoccerNetPlayer>();
    //    //SoccerNetPlayers.Add(soccerNetPlayer);
    //    SoccerNetPlayers.Clear();
    //    FootballPlayers = FindObjectsOfType<FootballPlayer>().ToList();
    //    SoccerNetPlayers = FindObjectsOfType<SoccerNetPlayer>().ToList();

    //    foreach (var player in SoccerNetPlayers) {
    //        if (player.PhotonPlayer.IsMasterClient)    //SoccerNetPlayers.Count == 1 //PhotonNetwork.IsMasterClient
    //            player.Role = GameSettings.Role.Administrator;
    //        else
    //        {
    //            player.Role = GameSettings.Role.Player;
    //        }
    //    }

    //}
    public void UnpauseGame() {
        foreach (var footballPlayer in FootballPlayers) {
            footballPlayer.enabled = true;
        }
    }
    public void ReplayGame() {
        // Set Visuals object in FootballPlayer hieararchy Active
        foreach (var footballPlayer in FootballPlayers) {
            footballPlayer.ChangeVisualRepresentationIsActive(true);
        }
    }
    
    public void LeaveRoom() {
        FindObjectOfType<GameplayEscMenu>().TurnEscMenuOff();
        MasterManager.GameSettings.MenuOpened = false;

        PhotonNetwork.LeaveRoom();
        
        PhotonLobby.I.ButtoExitGame();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer) {
        base.OnPlayerLeftRoom(otherPlayer);
        Debug.Log("PhotonRoomMatch. " + otherPlayer.NickName + " has left the game");

        Invoke("GetFootballNetPlayers", 0.2f);
    }

    public override void OnLeftRoom() {
        InRoom = false;
    }

    // Only Admin can call that method
    public void RefreshTeamNames() {
        if (!PhotonNetwork.IsMasterClient) return;

        PlayerPrefsUtils.SaveString(PlayerPrefsUtils.LastTeam1Name, Team1Menu.text);
        PlayerPrefsUtils.SaveString(PlayerPrefsUtils.LastTeam2Name, Team2Menu.text);
        _PhotonView.RPC("RPC_RefreshTeamNames", RpcTarget.AllBufferedViaServer, Team1Menu.text, Team2Menu.text);
    }
    [PunRPC]
    public void RPC_RefreshTeamNames(string team1Name, string team2Name) {
        Team1Name = team1Name;
        Team2Name = team2Name;
    }

    public void ButtonEndGame() {
        if (!PhotonNetwork.IsMasterClient) return;

        _PhotonView.RPC("RPC_ButtonEndGame", RpcTarget.AllBufferedViaServer);
    }
    [PunRPC]
    public void RPC_ButtonEndGame() {
        EventEndGame?.Invoke();

        PhotonNetwork.LeaveRoom();
        Application.Quit();
    }

    [PunRPC]
    public void RPC_EnforceNewUserSettings() {
        MasterManager.GameSettings.EnforceNewUserSettings();
    }
}
