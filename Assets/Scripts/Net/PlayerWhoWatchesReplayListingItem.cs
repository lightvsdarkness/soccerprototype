﻿using System;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Soccer {
    public class PlayerWhoWatchesReplayListingItem : MonoBehaviour {
        public bool Debugging;

        public string BoardName;

        [SerializeField] private TextMeshProUGUI _NameText;
        [SerializeField] private Toggle ToggleShouldWatch;

        public FootballNetPlayer _netPlayer;

        public FootballNetPlayer NetPlayer {
            get { return _netPlayer; }
            private set { _netPlayer = value; }
        }

        public PlayersWhoWatchReplayListingsMenu _PlayersWhoWatchReplayListingsMenu = null;



        public void SetPlayerInfo(PlayersWhoWatchReplayListingsMenu playersWhoWatchReplayListingsMenu,
            FootballNetPlayer player) {
            _PlayersWhoWatchReplayListingsMenu = playersWhoWatchReplayListingsMenu;

            NetPlayer = player;

            _NameText.text = player.Nickname;

            ToggleShouldWatch.isOn = player.ShouldWatchReplay;


        }


        public void SetShouldWatchToggle(bool state) {
            //PlayersDictionary ??

            if (Debugging)
                Debug.Log("PlayerWhoWatchesReplayListingItem. SetShouldWatchToggle: " + state);

            var photonPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == _netPlayer.Nickname);
            _PlayersWhoWatchReplayListingsMenu.PlayersDictionary.Keys.First(x => x.Nickname == NetPlayer.Nickname)
                .ShouldWatchReplay = state;
            _netPlayer._PhotonView.RPC("RPC_ChangeShouldWatchReplay", photonPlayer, state);
                // NOTE: change to photonView?

        }
    }
}