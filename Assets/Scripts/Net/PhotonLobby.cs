using System.Collections;
using System.Collections.Generic;
using IG;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice.PUN;
using Photon.Voice.Unity;
using TMPro;
using UnityEngine;

namespace Soccer {
    public class PhotonLobby : SingletonMonoBehaviourPunCallbacks<PhotonLobby> {

        public string _roomName;

        public GameObject LobbyCamera;

        [Header("UI Links")] public GameObject AdminUI;
        public GameObject LobbyUI;

        public TMP_InputField InputFieldUsername;
        public TMP_InputField InputFieldPassword;

        public GameObject BattleButton;
        public GameObject CancelButton;

        // Admin
        public TMP_InputField InputFieldRoomName;
        //public GameObject CreateRoomButton;

        [Space]
        [SerializeField] private MasterManager _masterManager;
        
        public RoomOptions _RoomOptions = new RoomOptions();


        protected void Start() {
            MasterManager.GameSettings.Initialize();

            //BattleButton.SetActive(false);
            //CancelButton.SetActive(false);
            AdminUI.SetActive(false);
            LobbyUI.SetActive(false);
            LobbyCamera.SetActive(true);
            _roomName = InputFieldRoomName.text = PlayerPrefsUtils.LoadString(PlayerPrefsUtils.LastMatchName);

            //Debug.Log("PhotonLobby. Connecting to Photon...", this);

            //AuthenticationValues authValues = new AuthenticationValues("0");
            //PhotonNetwork.AuthValues = authValues;

            var userId = MasterManager.GameSettings.RandomizeAndSetNickname(MasterManager.GameSettings.Nickname);

            AuthenticationValues authValues = new AuthenticationValues();
            // do not set authValues.Token or authentication will fail
            authValues.AuthType = CustomAuthenticationType.Custom;
            authValues.AddAuthParameter("user", userId);
            //authValues.AddAuthParameter("pass", pass);
            authValues.UserId = userId;
            // this is required when you set UserId directly from client and not from web service
            PhotonNetwork.AuthValues = authValues;

            PhotonNetwork.SendRate = 80; //20.
            PhotonNetwork.SerializationRate = 40; //10.
            //PhotonNetwork.AutomaticallySyncScene = true;

            // Because of Pause.
            PhotonNetwork.MinimalTimeScaleToDispatchInFixedUpdate = 0.0005f;
            PhotonVoiceNetwork.Instance.MinimalTimeScaleToDispatchInFixedUpdate = 0.0005f;
            //VoiceConnection.MinimalTimeScaleToDispatchInFixedUpdate = 0.0005f;

            PhotonNetwork.NickName = MasterManager.GameSettings.Nickname;
            PhotonNetwork.GameVersion = MasterManager.GameSettings.GameVersion;
            PhotonNetwork.ConnectUsingSettings();

            Time.timeScale = 1f;
        }

        public override void OnConnectedToMaster() {
            Debug.Log("PhotonLobby. OnConnectedToMaster", this);
            //PhotonNetwork.AutomaticallySyncScene = true;
            if (!PhotonNetwork.InLobby)
                PhotonNetwork.JoinLobby();

            AdminUI.SetActive(true);
            LobbyUI.SetActive(true);
            BattleButton.SetActive(true);

        }

        public void ButtonCreateRoomClicked() {
            if (!PhotonNetwork.IsConnected) {
                Debug.LogError("PhotonLobby. Not connected to PhotonNetwork");
                return;
            }

            _roomName = InputFieldRoomName.text;
            PlayerPrefsUtils.SaveString(PlayerPrefsUtils.LastMatchName, _roomName);

            PhotonNetwork.NickName = MasterManager.GameSettings.RandomizeAndSetNickname(InputFieldUsername.text);

            _RoomOptions.PublishUserId = true;
            _RoomOptions.MaxPlayers = 28;
            _RoomOptions.IsOpen = true;
            _RoomOptions.IsVisible = true;

            PhotonNetwork.CreateRoom(_roomName, _RoomOptions, null);

            //LobbyUI.SetActive(false);
            //LobbyCamera.SetActive(false);
        }

        private void CreateRoom() {
            int randomRoomName = Random.Range(0, 100000);
            RoomOptions roomOptions = new RoomOptions() {IsVisible = true, IsOpen = true, MaxPlayers = 10};
            PhotonNetwork.CreateRoom(_roomName + randomRoomName, roomOptions);
        }

        public override void OnCreateRoomFailed(short returnCode, string message) {
            Debug.LogError("PhotonRoomMatch. OnCreateRoomFailed", this);
            Debug.LogError("PhotonRoomMatch. OnCreateRoomFailed" + returnCode + " " + message, this);

            //CreateRoom();
        }

        public override void OnCreatedRoom() {
            if (Debugging)
                Debug.Log("PhotonRoomMatch. OnCreatedRoom", this);

            //CreateRoom();
        }

        public void ButtonJoinClicked() {
            if (!PhotonNetwork.IsConnected) {
                Debug.LogError("PhotonLobby. Not connected to PhotonNetwork");
                return;
            }
            if (Debugging) Debug.Log("PhotonLobby. ButtonJoinClicked", this);

            _roomName = InputFieldPassword.text;

            if (_roomName == "") return;

            PhotonNetwork.NickName = MasterManager.GameSettings.RandomizeAndSetNickname(InputFieldUsername.text);

            //PhotonNetwork.JoinOrCreateRoom(_roomName, _RoomOptions, null);
            PhotonNetwork.JoinRoom(_roomName, null);
            //PhotonNetwork.JoinRandomRoom();

            //LobbyCamera.enabled = false;
        }

        public void ButtonCancelClicked() {
            BattleButton.SetActive(true);
            CancelButton.SetActive(false);

            PhotonNetwork.LeaveRoom();

            AdminUI.SetActive(true);
            LobbyUI.SetActive(true);
            LobbyCamera.SetActive(true);
        }

        public override void OnJoinRoomFailed(short returnCode, string message) {
            if (Debugging)
                Debug.Log("PhotonLobby. OnJoinRoomFailed: " + returnCode + " " + message);
            //CreateRoom();
        }

        public override void OnJoinedRoom() {
            if (Debugging)
                Debug.Log("PhotonLobby. OnJoinedRoom", this);

            // NOTE: dsff
            AdminUI.SetActive(false);
            LobbyUI.SetActive(false);
            LobbyCamera.SetActive(false);
        }

        public override void OnLeftRoom() {
            LobbyUI.SetActive(true);
            LobbyCamera.SetActive(true);
            Application.Quit();
        }

        public void ButtoExitGame() {
            Debug.Log("ButtoExitGame");
            Application.Quit();
        }
    }
}