﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIAdminReplay : MonoBehaviour
{
    public List<Button> ButtonsClips = new List<Button>();
    public List<TMP_Text> TextClips = new List<TMP_Text>();
    [Space]
    public TMP_FontAsset HoldFont;
    public TMP_FontAsset DefaultFont;
    [Space]
    public Sprite HoldSprite;
    public Sprite DefaultSprite;
    void Start() {
        DefaultSprite = ButtonsClips[0].image.sprite;
    }


    public void MarkSelectedButton(int clipIndex) {
        foreach (var TMP_Text in TextClips) {
            TMP_Text.font = DefaultFont;
        }
        TextClips[clipIndex].font = HoldFont;

        foreach (var button in ButtonsClips) {
            button.image.sprite = DefaultSprite;
        }
        ButtonsClips[clipIndex].image.sprite = HoldSprite;
    }
}
