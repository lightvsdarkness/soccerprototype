﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PanelAdmin : MonoBehaviour
{
    public GameObject PanelAdminUI;

    public GameObject Actions;

    public TMP_InputField InputFieldAdminPassword = null;

    public string Password = "pass";

    void Start()
    {
        PanelAdminUI.SetActive(false);
        Actions.SetActive(false);
    }

    public void UIPassFieldChanged() {
        if (InputFieldAdminPassword.text == Password)
        {
            PlayerPrefsUtils.SaveString(PlayerPrefsUtils.AdminPassword, InputFieldAdminPassword.text);
            Actions.SetActive(true);
        }
        else
        {
            Actions.SetActive(false);
        }
    }

    public void SwitchAdminPanel()
    {
        PanelAdminUI.SetActive(!PanelAdminUI.activeSelf);
        InputFieldAdminPassword.text = PlayerPrefsUtils.LoadString(PlayerPrefsUtils.AdminPassword);


    }

}