﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonPauseGame : MonoBehaviour {
    [SerializeField] private bool GamePaused; // for debugging

    public string GameIsPlayedText = "Pause the match";
    public string GameIsPausedText = "Unpause the match";

    [SerializeField] private Button ButtonPauseUnpause = null;
    public TextMeshProUGUI TextButtonPause = null;

    void Awake() {
        if (ButtonPauseUnpause == null)
            ButtonPauseUnpause = GetComponent<Button>();
        if (TextButtonPause == null)
            TextButtonPause = GetComponentInChildren<TextMeshProUGUI>();
    }

    void Start() {


        TextButtonPause.text = GameIsPlayedText;
    }

    private void OnEnable() {
        ButtonPauseUnpause.interactable = true;
    }

    public void ButtonPauseGameSwitch() {
        GamePaused = !GamePaused;

        if (GamePaused) {
            TextButtonPause.text = GameIsPausedText;
        }
        else {
            TextButtonPause.text = GameIsPlayedText;
        }

        StartCoroutine(ButtonInteractionCor());
    }

    private IEnumerator ButtonInteractionCor() {
        ButtonPauseUnpause.interactable = false;
        yield return new WaitForSecondsRealtime(1f);

        ButtonPauseUnpause.interactable = true;
    }
}
