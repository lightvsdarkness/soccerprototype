﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using TMPro;

namespace Soccer {
    public class GameplayEscMenu : MonoBehaviour {
        public bool IsActive;
        [SerializeField] private GameObject EscMenuContainer;
        [SerializeField] private GameObject SettingsContainer;
        [SerializeField] private GameObject ControlsContainer;
        [SerializeField] private TextMeshProUGUI LabelName;

        [Header("Admin")] public UIAdminActions AdminActions;

        public PlayersWhoWatchReplayListingsMenu _PlayersWhoWatchReplayListingsMenu;


        void Start() {
            EscMenuContainer.SetActive(false);
            SettingsContainer.SetActive(false);
            ControlsContainer.SetActive(false);

        }

        /// <summary>
        /// NOTE: Input
        /// </summary>
        void Update() {
            if (!PhotonRoomMatch.I.InRoom) return;

            if (IsActive)
                LabelName.text = MasterManager.GameSettings.Nickname;

            AdminActions.SwitchAdminActions();
        }

        public void SwitchEscMenu() {
            IsActive = !IsActive;

            if (IsActive) {
                TurnEscMenuOn();
            }
            else {
                TurnEscMenuOff();
            }
        }

        public void TurnEscMenuOn() {
            EscMenuContainer.SetActive(true);

            _PlayersWhoWatchReplayListingsMenu.SwitchReplayPlayerListingAdmin();
        }

        public void TurnEscMenuOff() {
            EscMenuContainer.SetActive(false);
            PhotonRoomMatch.I._PhotonView.RPC("RPC_EnforceNewUserSettings", RpcTarget.AllBufferedViaServer);
        }


        public void SwitchSettingsContainer() {
            SettingsContainer.SetActive(!SettingsContainer.activeSelf);

            if (SettingsContainer.activeSelf) {

            }
        }

        public void SwitchControlsContainer() {
            ControlsContainer.SetActive(!ControlsContainer.activeSelf);

            if (ControlsContainer.activeSelf) {

            }
        }

        public void ControlsBack() {
            ControlsContainer.SetActive(false);
        }
    }
}