﻿using System.Collections;
using System.Collections.Generic;
using Soccer;
using TMPro;
using UnityEngine;

namespace Soccer
{
    public class UIBall : MonoBehaviour
    {
        public FootballPlayer _FootballPlayer;
        [SerializeField] private UnityEngine.UI.Image _UIBallImage;
        [SerializeField] private TextMeshProUGUI _UIBallText;

        private void Start() {
            if (_UIBallImage == null)
                _UIBallImage = gameObject.GetComponentInChildren<UnityEngine.UI.Image>(true);
            if (_UIBallText == null)
                _UIBallText = gameObject.GetComponentInChildren<TextMeshProUGUI>(true);
        }

        public void SwitchUIBall(bool state) {
            _UIBallText.enabled = state;
            _UIBallImage.enabled = state;

            CheckUIBallImage();
        }

        public void CheckUIBallImage() {
            if (_FootballPlayer._CameraView == CameraView.ThirdPersonView) {
                //Debug.LogWarning($"_FootballPlayer._CameraView {_FootballPlayer._CameraView}", this);
                _UIBallImage.enabled = false;
            }
            else {
                if(_FootballPlayer._Ball != null)
                    _UIBallImage.enabled = true;
            }
        }

    }

}

