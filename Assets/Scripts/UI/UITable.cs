﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class UITable : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI Team1Table;
    [SerializeField] private TextMeshProUGUI Team2Table;


    [SerializeField] private TextMeshProUGUI UITimer;

    public bool gameIsPlayed;
    public float elapsedTime;

    private WaitForSeconds wait01 = new WaitForSeconds(0.25f);

    //private void OnEnable() {
    //    PhotonRoomMatch.I.EventStartGame += UITableStartGame;
    //}
    private void OnDisable() {
        PhotonRoomMatch.I.EventStartGame -= UITableStartGame;
        PhotonRoomMatch.I.EventEndGame -= UITableEndGame;
    }
    private void Start() {
        PhotonRoomMatch.I.EventStartGame += UITableStartGame;
        PhotonRoomMatch.I.EventEndGame += UITableEndGame;

        UITimer.text = "00 : 00.00";
    }
    private void Update() {
        // NOTE: Optimize
        if (PhotonNetwork.InRoom) {
            Team1Table.text = PhotonRoomMatch.I.Team1Name;
            Team2Table.text = PhotonRoomMatch.I.Team2Name;
        }

    }

    public void UITableStartGame() {
        gameIsPlayed = true;
        StartCoroutine(UpdateTimer());
    }
    public void UITableEndGame() {
        gameIsPlayed = false;
        StopAllCoroutines();
    }

    private IEnumerator UpdateTimer() {
        while (gameIsPlayed) {
            elapsedTime += Time.deltaTime;
            var timePlaying = TimeSpan.FromSeconds(elapsedTime);
            string timePlayingStr = timePlaying.ToString("hh': 'mm'.'ss");
            UITimer.text = timePlayingStr;

            yield return null;
        }
    }
}
