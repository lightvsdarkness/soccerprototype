﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIKickTrajectory : MonoBehaviour {
    public Button UIButton;
    void Start()
    {
        
    }

    public void IndicateDefaultTrajectory() {
        //UIText.color = Color.white;
        UIButton.interactable = false;
    }
    public void IndicateSelectedTrajectory() {
        //UIText.color = Color.blue;
        UIButton.interactable = true;
    }
}
