﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIPanelLobby : MonoBehaviour
{
    public TMP_InputField InputFieldNickname;

    void Start()
    {
        InputFieldNickname.text = PlayerPrefsUtils.LoadString(PlayerPrefsUtils.LastPlayerNickname);
        //Debug.Log($"InputFieldNickname.text: {InputFieldNickname.text}");
    }

    public void UIInputFieldNicknameUsed() {
        if (InputFieldNickname.text != "") {
            PlayerPrefsUtils.SaveString(PlayerPrefsUtils.LastPlayerNickname, InputFieldNickname.text);
            //Debug.Log($"InputFieldNickname.text: {InputFieldNickname.text}");
        }
        else {

        }
    }
}
