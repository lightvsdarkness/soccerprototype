﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Soccer {
    public class UIAdminRecord : MonoBehaviour
    {
        [SerializeField]
        private Button ButtonRecord;
        //[Space]
        //public TMP_FontAsset HoldFont;
        //public TMP_FontAsset DefaultFont;
        //[Space]
        //public Sprite HoldSprite;
        //public Sprite DefaultSprite;
        void Start() {
            //DefaultSprite = ButtonsClips[0].image.sprite;
        }


        //public void MarkSelectedButton(int clipIndex) {
        //    foreach (var TMP_Text in TextClips) {
        //        TMP_Text.font = DefaultFont;
        //    }
        //    TextClips[clipIndex].font = HoldFont;

        //    foreach (var button in ButtonsClips) {
        //        button.image.sprite = DefaultSprite;
        //    }
        //    ButtonsClips[clipIndex].image.sprite = HoldSprite;
        //}

        /// <summary>
        /// True - enabled
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public void SwitchButtonRecordAccessability(bool state) {
            ButtonRecord.interactable = state;
        }
    }

}

