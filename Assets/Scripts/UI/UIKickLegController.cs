﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIKickLegController : MonoBehaviour
{
    public List<UIKickStrength> UIKickStrengths = new List<UIKickStrength>();
    public List<UIKickTrajectory> UIKickTrajectories = new List<UIKickTrajectory>();

    void Start()
    {
        
    }


    public void IndicateChangedStrength(int strengthValue) {
        foreach (var UIKickStrength in UIKickStrengths) {
            UIKickStrength.IndicateDefaultStrength();
        }
        UIKickStrengths[strengthValue].IndicateSelectedStrength();
    }

    public void IndicateChangedTrajectory(int trajectoryValue) {
        foreach (var _UIKickTrajectory in UIKickTrajectories)
        {
            _UIKickTrajectory.IndicateDefaultTrajectory();
        }
        UIKickTrajectories[trajectoryValue].IndicateSelectedTrajectory();
    }
}
