﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace Soccer {
    public class PanelSettings : MonoBehaviour {
        public Slider SliderCameraDistanceModifier;
        public Slider SliderCameraDistanceBallModifier;

        void Start() {
            SliderCameraDistanceModifier.value = MasterManager.GameSettings.CameraDistanceModifier;

            SliderCameraDistanceBallModifier.value = MasterManager.GameSettings.CameraDistanceBallModifier;
        }

        [ContextMenu("GetCustomProperty_CameraDistanceModifier")]
        public void GetCustomProperty_CameraDistanceModifier() {
            Debug.LogWarning("CameraDistanceModifier: " +
                             (float) PhotonNetwork.MasterClient.CustomProperties["CameraDistanceModifier"]);
        }

        [ContextMenu("GetCustomProperty_CameraDistanceBallModifier")]
        public void GetCustomProperty_CameraDistanceBallModifier() {
            Debug.LogWarning("CameraDistanceBallModifier: " +
                             (float) PhotonNetwork.MasterClient.CustomProperties["CameraDistanceBallModifier"]);
        }


        //void Update()
        //{

        //}
    }
}