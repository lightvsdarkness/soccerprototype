﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace Soccer {
    public class UIAdminActions : MonoBehaviour {

        [SerializeField] private GameObject AdminRecordGroup;

        [Space] [SerializeField] private GameObject AdminButtonsGroup;

        [Header("Buttons")] [SerializeField] private GameObject ButtonStartGame;
        [SerializeField] private GameObject ButtonPauseGame;
        [SerializeField] private GameObject ButtonEndGame;

        [Space] [SerializeField] private GameObject ButtonSettings;

        [Space] [SerializeField] private bool firstInit = true;

        public void OnEnable() {
            //AdminButtonsGroup.SetActive(false);
            if (PhotonNetwork.LocalPlayer.IsMasterClient) {
                AdminButtonsGroup.SetActive(true);
            }
            else {
                AdminButtonsGroup.SetActive(false);
            }

            if (!firstInit) {
                PhotonRoomMatch.I.EventStartGame += GameStarted;
            }
        }

        public void OnDisable() {
            PhotonRoomMatch.I.EventStartGame -= GameStarted;
        }

        private void Start() {
            firstInit = false;
            PhotonRoomMatch.I.EventStartGame += GameStarted;

            AdminRecordGroup.SetActive(false);

            ButtonStartGame.SetActive(true);
            ButtonPauseGame.SetActive(false);
            ButtonEndGame.SetActive(false);
        }


        private void GameStarted() {
            AdminRecordGroup.SetActive(true);

            ButtonStartGame.SetActive(false);
            ButtonPauseGame.SetActive(true);
            ButtonEndGame.SetActive(true);
        }

        //public void SwitchGameplayUIState(GameObject panel) {
        //    panel.SetActive(!panel.activeSelf);

        //    if (PhotonNetwork.LocalPlayer.IsMasterClient) {
        //        AdminButtonsGroup.SetActive(true);
        //    }
        //    else {
        //        AdminButtonsGroup.SetActive(false);
        //    }
        //}

        public void SwitchAdminActions() {
            //AdminButtonsGroup.SetActive(!AdminButtonsGroup.activeSelf);

            if (MasterManager.GameSettings.MyFootballNetPlayer.RoleByte == 0) {
                // PhotonNetwork.LocalPlayer.IsMasterClient
                AdminButtonsGroup.SetActive(true);
                ButtonSettings.SetActive(true);
            }
            else {
                AdminButtonsGroup.SetActive(false);
                ButtonSettings.SetActive(false);
            }
        }
    }
}