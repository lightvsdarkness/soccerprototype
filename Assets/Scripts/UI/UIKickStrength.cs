﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIKickStrength : MonoBehaviour {
    public TextMeshProUGUI UIText;
    void Start()
    {
        
    }

    public void IndicateDefaultStrength() {
        UIText.color = Color.white;
    }
    public void IndicateSelectedStrength()
    {
        UIText.color = Color.blue;
    }
}
