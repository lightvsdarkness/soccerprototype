﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsUtils
{
    public static string AdminPassword = "AdminPassword";

    public static string LastPlayerNickname = "LastPlayerNickname";
    public static string LastMatchName = "GreatGame";
    public static string LastTeam1Name = "Team1";
    public static string LastTeam2Name = "Team2";

    public static string CameraDistanceModifier = "CameraDistanceModifier";
    public static string CameraDistanceBallModifier = "CameraDistanceBallModifier";

    public static void SaveString(string key, string value) {
        
        PlayerPrefs.SetString(key, value);
        PlayerPrefs.Save();
    }
    public static void SaveFloat(string key, float value) {

        PlayerPrefs.SetFloat(key, value);
        PlayerPrefs.Save();
    }

    public static string LoadString(string key) {

        if (PlayerPrefs.HasKey(key)) {
            return PlayerPrefs.GetString(key);
        }
        return string.Empty;
    }

    public static float? LoadFloat(string key) {

        if (PlayerPrefs.HasKey(key)) {
            return PlayerPrefs.GetFloat(key);
        }
        return null;
    }

    public void SaveAllStrings() {
        string key = "MyGame";
        int score = 0;
        float time = 0;
        PlayerPrefs.SetInt(key, score);
        PlayerPrefs.SetFloat(key, time);
        PlayerPrefs.Save();
    }
}
