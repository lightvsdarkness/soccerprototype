﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gates : MonoBehaviour {
    public bool Team1GetsScore;
    public bool Team2GetsScore;
    void Start()
    {
        
    }



    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Ball"))
        {
            Debug.Log("entered");
            if (Team1GetsScore) {
                PhotonRoomMatch.I.Team1Score++;
            }
            else {
                PhotonRoomMatch.I.Team2Score++;
            }
        }
    }
}
