﻿using System.Collections;
using System.Collections.Generic;
using REM;
using UnityEngine;

public class ManagerGame : SingletonManager<ManagerGame> {
    public Transform Center;

    [Space]
    public Transform TrackedTransform;
    public Transform SpectatorInitialPosition;
    public Transform SpectatorPosition2;
    public Transform SpectatorPosition3;
    public Transform SpectatorPosition4;

    protected override void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
