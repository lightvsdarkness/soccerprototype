﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;


namespace Soccer {
    public class BallTrigger : MonoBehaviour {
        public Ball _Ball;
        public bool _capturedByPlayer = true;
        //[SerializeField] private float stayCount = 0.0f;
        [SerializeField] private float stayNeededTime = 0.15f;
        [SerializeField] private float departTime = 0.5f;

        public bool TriggerForGoalKeeper; // Should be true for bigger Goalkeeper's trigger and false for ordinyary one

        void Start() {
            if (_Ball == null)
                _Ball = GetComponentInParent<Ball>();
        }

        void Update() {

        }

        private void OnTriggerStay(Collider other) {
            if (other.tag != "Player") return;

            FootballPlayer contactedFootballPlayer = other.GetComponent<FootballPlayer>();

            // If too much contacts - uncomment
            if (PhotonNetwork.LocalPlayer.NickName != contactedFootballPlayer._FootballNetPlayer.Nickname) return;

            // If triggering collision with the player who already control the ball
            if (contactedFootballPlayer == _Ball.CurrentPlayer) return;

            // If the player doesn't want to capture the ball
            if (!contactedFootballPlayer._FootballNetPlayer.ShouldCaptureBall) return;

            // If not enough time has passed since previous capture
            if (
                !((Time.realtimeSinceStartup - _Ball.TimeBallWasPreviouslyCaptured) > _Ball.PeriodBallCanBeCapturedAgain)) {
                Debug.Log($"BallTrigger. Not enough time: {Time.realtimeSinceStartup - _Ball.TimeBallWasPreviouslyCaptured}" );
                return;
            }

            // Goalkeeper trigger
            if (TriggerForGoalKeeper) {
                if (!contactedFootballPlayer._FootballNetPlayer.IsGoalkeeper)
                    return;
            }


            if (contactedFootballPlayer.BallStayCount > stayNeededTime)
            {
                // If the ball is already captured - the other player will get the ball in the 50% of cases
                if (_capturedByPlayer)
                {
                    int dice = Random.Range(0, 2);
                    if (dice == 0)
                    {
                        // Previous player looses the ball
                        //_Ball._PhotonView.RPC("RPC_BallCapturedFromCurrentPlayer", RpcTarget.AllViaServer);
                        if (_Ball.CurrentPlayer != null)
                        {
                            var previousPhotonPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == _Ball.CurrentPlayer._FootballNetPlayer.Nickname);
                            contactedFootballPlayer._PhotonView.RPC("RPC_TurnBallUI", previousPhotonPlayer, false); // NOTE: change to photonView?
                            _Ball.CurrentPlayer._Ball = null;
                        }


                        // New player gets the ball
                        _Ball.AlignWithPlayer(contactedFootballPlayer);
                    }
                }
                else
                {
                    Debug.Log("BallTrigger. Ball was taken by: " + contactedFootballPlayer._FootballNetPlayer.Nickname, this);
                    if(_Ball.CurrentPlayer != null)
                        _Ball.CurrentPlayer._Ball = null;
                    _capturedByPlayer = true;
                    _Ball.AlignWithPlayer(contactedFootballPlayer);
                }
                contactedFootballPlayer.BallStayCount = 0f;
            }
            else {
                contactedFootballPlayer.BallStayCount = contactedFootballPlayer.BallStayCount + Time.deltaTime;
            }
            
        }

        private void OnTriggerExit(Collider other) {
            FootballPlayer contactedFootballPlayer = other.GetComponent<FootballPlayer>();

            if (_Ball.CurrentPlayer != null && (contactedFootballPlayer == _Ball.CurrentPlayer)) 
                _Ball.CurrentPlayer.BallStayCount = 0f;
        }

        public void DepartPlayer() {
            StartCoroutine(DepartPlayerCor());
        }

        public IEnumerator DepartPlayerCor() {

            Debug.Log("BallTrigger. DepartPlayerCor _shouldStay: " + _capturedByPlayer, this);
            yield return new WaitForSeconds(departTime);

            _Ball.gameObject.layer = LayerMask.NameToLayer("Default");
            gameObject.layer = LayerMask.NameToLayer("Default");
            _capturedByPlayer = false;
            Debug.Log("BallTrigger. DepartPlayerCor _shouldStay: " + _capturedByPlayer, this);
        }
    }
}