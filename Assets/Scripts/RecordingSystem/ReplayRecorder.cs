﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Soccer {
    public class ReplayRecorder : MonoBehaviour {
        public bool Debugging;
        public bool ShouldOnlyRecordActivity; // TODO: Implement / Optimize recorder. Check length

        public int RecorderId = 0;
        //public List<ReplayManager.Clip> Records = new List<ReplayManager.Clip>();

        [Space] public bool SnapshotFramesIsNull;
        public List<Frame> SnapshotFrames = new List<Frame>();
        
        //public List<AnimationRecord> AnimationRecords = new List<AnimationRecord>();

        [Space]
        //public int MaxLength;
        public int Length;

        [SerializeField] private int FrameIndex = -1;

        [Header("Links")]
        public ReplayManager _ReplayManager;
        public Animator _Animator;


        void Start() {
            //Debugging = true;
            if (_ReplayManager == null)
                _ReplayManager = ReplayManager.I;

            //SnapshotFrames = new List<Frame> { null };

            //_ReplayManager.CheckIfRecorderWithThisIdIsRegistered(RecorderId)

            RecorderId = _ReplayManager.AddRecorderToListAndAssignId(this);
        }

        private void Update() {
            if (SnapshotFrames == null)
                SnapshotFramesIsNull = true;
            else
                SnapshotFramesIsNull = false;
        }

        public void TakeSnapshot() {
            List<AnimationRecord> AnimationRecords = new List<AnimationRecord>();

            if (_Animator != null) {
                foreach (AnimatorControllerParameter item in _Animator.parameters) {
                    string name = item.name;
                    if (item.type == AnimatorControllerParameterType.Bool) {
                        AnimationRecords.Add(new AnimationRecord(name, _Animator.GetBool(name), item.type));
                    }
                    else if (item.type == AnimatorControllerParameterType.Float) {
                        AnimationRecords.Add(new AnimationRecord(name, _Animator.GetFloat(name), item.type));
                    }
                    else if (item.type == AnimatorControllerParameterType.Int) {
                        AnimationRecords.Add(new AnimationRecord(name, _Animator.GetInteger(name), item.type));
                    }
                }
            }

            Frame frame = new Frame(this.gameObject, transform.position, transform.rotation, transform.localScale,
                AnimationRecords);
            Debug.Log("ReplayRecorder. TakeSnapshot, Frame: " + frame, this);
            SnapshotFrames.Add(frame);
            //Debug.Log("ReplayRecorder. TakeSnapshot, SnapshotFrames: " + SnapshotFrames, this);
        }

        public void ReturnToSnapshotFrame() {
            Frame SnapshotFrame = null;
            if (SnapshotFrames?.Count > 0) {
                SnapshotFrame = SnapshotFrames[0];
            }

            if (SnapshotFrame != null) {
                gameObject.SetActive(SnapshotFrame.State);
                transform.position = SnapshotFrame.Position;
                transform.rotation = SnapshotFrame.Rotation;
                transform.localScale = SnapshotFrame.Scale;
                if (SnapshotFrame.AnimationRecords == null) return;

                foreach (var animationRecord in SnapshotFrame.AnimationRecords) {
                    string name = animationRecord.Name;
                    if (animationRecord.Type == AnimatorControllerParameterType.Bool) {
                        _Animator.SetBool(name, animationRecord.BoolValue);
                        continue;
                    }
                    else if (animationRecord.Type == AnimatorControllerParameterType.Int) {
                        _Animator.SetInteger(name, animationRecord.IntValue);
                        continue;
                    }
                    else if (animationRecord.Type == AnimatorControllerParameterType.Float) {
                        _Animator.SetFloat(name, animationRecord.FloatValue);
                        continue;
                    }
                }
                SnapshotFrames.Clear();
            }
        }


        public void CaptureFrame() {
            List<AnimationRecord> AnimationRecords = new List<AnimationRecord>();
            if (_Animator != null) {
                foreach (AnimatorControllerParameter item in _Animator.parameters) {
                    string name = item.name;
                    if (item.type == AnimatorControllerParameterType.Bool) {
                        AnimationRecords.Add(new AnimationRecord(name, _Animator.GetBool(name), item.type));
                    }
                    else if (item.type == AnimatorControllerParameterType.Float) {
                        AnimationRecords.Add(new AnimationRecord(name, _Animator.GetFloat(name), item.type));
                    }
                    else if (item.type == AnimatorControllerParameterType.Int) {
                        AnimationRecords.Add(new AnimationRecord(name, _Animator.GetInteger(name), item.type));
                    }
                }
            }


            Frame frame = new Frame(this.gameObject, transform.position, transform.rotation, transform.localScale,
                AnimationRecords);

            SaveFrame(frame);

        }

        public void SaveFrame(Frame frame) {
            ReplayManager.RecorderData recorderData = null;
            _ReplayManager.SelectedClip.RecorderRecording.TryGetValue(RecorderId, out recorderData);

            // If someone joined after the some recording was done
            if(recorderData == null)
                _ReplayManager.SelectedClip.RecorderRecording.Add(RecorderId, new ReplayManager.RecorderData(RecorderId));
            _ReplayManager.SelectedClip.RecorderRecording.TryGetValue(RecorderId, out recorderData);

            if (Length < ReplayManager.I.MaxLength) {
            }
            else {
                recorderData.Frames.RemoveAt(0);

                Length = ReplayManager.I.MaxLength - 1;
            }
            recorderData.Frames.Add(frame);
            Length++;
        }

        public void Replay() {
            Frame frame = null;
            frame = ReadFrame();

            if (frame != null) {
                gameObject.SetActive(frame.State);
                transform.position = frame.Position;
                transform.rotation = frame.Rotation;
                transform.localScale = frame.Scale;
                foreach (var animationRecord in frame.AnimationRecords) {
                    string name = animationRecord.Name;
                    if (animationRecord.Type == AnimatorControllerParameterType.Bool) {
                        _Animator.SetBool(name, animationRecord.BoolValue);
                        continue;
                    }
                    else if (animationRecord.Type == AnimatorControllerParameterType.Int) {
                        _Animator.SetInteger(name, animationRecord.IntValue);
                        continue;
                    }
                    else if (animationRecord.Type == AnimatorControllerParameterType.Float) {
                        _Animator.SetFloat(name, animationRecord.FloatValue);
                        continue;
                    }
                }
            }
            else {
                if (Debugging) Debug.LogWarning("ReplayRecorder. Play. frame == null");
                //MasterManager.GameSettings._GameMode = GameSettings.GameMode.Pause;
            }
        }

        public Frame ReadFrame() {
            FrameIndex++;

            if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Pause) {
                FrameIndex--;
            }
            else {

            }

            if (FrameIndex >= Length) {
                if(FrameIndex >= _ReplayManager.CurrentClipMaxLength)
                    MasterManager.GameSettings._GameMode = GameSettings.GameMode.Pause;
                if (Debugging) Debug.LogWarning($"RecorderId: {RecorderId} FrameIndex: {FrameIndex} >= Length: {Length} ", this);

                FrameIndex = Length - 1;
            }
            if (FrameIndex == -1) {
                FrameIndex = Length - 1;
            }

            if (!_ReplayManager.SelectedClip.RecorderRecording.ContainsKey(RecorderId)) {
                Debug.LogWarning($"!_ReplayManager.SelectedClip.RecorderRecording.ContainsKey(RecorderId) RecorderId: {RecorderId}", this);

                foreach (var key in _ReplayManager.SelectedClip.RecorderRecording.Keys) {
                    if (Debugging) Debug.LogWarning($"Key present: " + key, this);
                }
                return null;
            }

            if (_ReplayManager.SelectedClip.RecorderRecording[RecorderId].Frames.Count <= FrameIndex) {
                if (Debugging) Debug.LogWarning($"Frames.Count <= FrameIndex", this);
                return null;
            }
                

            return _ReplayManager.SelectedClip.RecorderRecording[RecorderId].Frames[FrameIndex];
        }

        public void SetFrame(int value) {
            FrameIndex = value;
        }

        public int GetCurrentFrameIndex() {
            return FrameIndex;
        }
    }
}