﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace Soccer
{
    public class AdminCameraPosRotTransmitter : MonoBehaviour
    {
        public ReplayRecorder AdminWatchRecorder;
        public GameObject AdminWatchTransmitter;
        public GameObject TrueAdminWatchTransmitter;
        [SerializeField] private PhotonView _adminWatchTransmitterPhotonView;

        public CameraReplay _CameraReplay;
        public FootballPlayer MyFootballPlayer;
        public CameraSpectator _CameraSpectator;

        private void Start() {
            _CameraReplay = FindObjectOfType<CameraReplay>();

            if (_CameraSpectator == null)
                _CameraSpectator = GetComponentInParent<CameraSpectator>();

            Invoke("Initialize", 1.1f);
        }

        private void Initialize() {
            if (PhotonRoomMatch.I.AdminFootballPlayer._FootballNetPlayer.RoleByte == 0) {
                Debug.Log("AdminCameraPosRotTransmitter. Initialize", this);
                AdminWatchRecorder.enabled = true;
                AdminWatchTransmitter.SetActive(true);

                ReplayManager.I.AdminReplayRecorder = AdminWatchRecorder.GetComponent<ReplayRecorder>();
            }

            if (_adminWatchTransmitterPhotonView.IsMine) {
                ReplayManager.I._AdminCameraPosRotTransmitter = this;

                TrueAdminWatchTransmitter = PhotonRoomMatch.I.AdminFootballPlayer._cameraSpectator.GetComponent<AdminCameraPosRotTransmitter>().AdminWatchTransmitter;

            }
        }

        /// <summary>
        /// True if Replaying
        /// </summary>
        /// <param name="state"></param>
        public void SwitchAdminWatchTransmitter(bool state) {
            AdminWatchTransmitter.SetActive(state);
            _CameraSpectator._camera.enabled = !state;
            _CameraReplay._Camera.enabled = state;

            if (!state) {
                AdminWatchRecorder.transform.localPosition = Vector3.zero;
                AdminWatchRecorder.transform.localRotation = Quaternion.identity;
                AdminWatchTransmitter.transform.localPosition = Vector3.zero;
                AdminWatchTransmitter.transform.localRotation = Quaternion.identity;
                StartCoroutine(ReturnToPlayableStateAfterReplayCor());

                MyFootballPlayer.ReturnToPlayableStateAfterReplay();

            }
        }
        IEnumerator ReturnToPlayableStateAfterReplayCor() {
            yield return null;
            AdminWatchRecorder.transform.localPosition = Vector3.zero;
            AdminWatchRecorder.transform.localRotation = Quaternion.identity;
            AdminWatchTransmitter.transform.localPosition = Vector3.zero;
            AdminWatchTransmitter.transform.localRotation = Quaternion.identity;
        }

        public void SpecateReplay() {
            //Debug.LogWarning($"Sync AdminWatchTransmitter.transform.position: {AdminWatchTransmitter.transform.position} AdminWatchTransmitter.transform.rotation: {AdminWatchTransmitter.transform.rotation}", this);
            _CameraReplay.transform.position = TrueAdminWatchTransmitter.transform.position;
            _CameraReplay.transform.rotation = TrueAdminWatchTransmitter.transform.rotation;
        }
    }
}