﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IG;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace Soccer {
    public class ReplayManager : SingletonMonoBehaviourPunCallbacks<ReplayManager> {
        [Serializable]
        public class Clip {
            public int ClipId;
            public Dictionary<int, RecorderData> RecorderRecording = new Dictionary<int, RecorderData>();

            // public RecorderData _RecorderData;
            // DEBUG ONLY!!!
            public RecorderData RecordingDataToList = new RecorderData(0);
        }

        [Serializable]
        public class RecorderData {
            public int RecorderId;

            public List<Frame> Frames = new List<Frame>();

            public RecorderData(int recorderId) {
                RecorderId = recorderId;
            }
        }


        //
        public List<Clip> Clips = new List<Clip>(10);

        public Clip SelectedClip => Clips[_selectedClipIndex];
        [SerializeField] private int _selectedClipIndex = -1;

        public int SelectedClipIndex {
            get {
                return _selectedClipIndex;
            }
            private set {
                _UIAdminReplay.MarkSelectedButton(value);
                _selectedClipIndex = value;
            }
        }


        [SerializeField] private PlayersWhoWatchReplayListingsMenu _PlayersWhoWatchReplayListingsMenu;


        public int MaxLength = 1000000;
        [SerializeField]
        private int _currentClipMaxLength;

        public int CurrentClipMaxLength {
            get {
                if (SelectedClip.RecorderRecording.Count > 0)
                    _currentClipMaxLength = SelectedClip.RecorderRecording.Values.Max(x => x.Frames.Count);
                else
                    _currentClipMaxLength = 0;
                return _currentClipMaxLength;
            }
        }

        [Header("Recorders Liks")]
        public List<ReplayRecorder> ReplayRecorders;
        //
        public ReplayRecorder AdminReplayRecorder;
        public AdminCameraPosRotTransmitter _AdminCameraPosRotTransmitter;

        [Header("Links")]
        [SerializeField] private UIAdminReplay _UIAdminReplay;
        [SerializeField] private UIAdminRecord _UIAdminRecord;
        public GameObject UIRecordGroup;
        public GameObject UIReplayGroup;

        [Header("Slider")]
        public bool SliderIsMoving;
        public Slider _Slider;

        public PhotonView _PhotonView;

        public override void OnEnable() {
            base.OnEnable();

            //ButtonPlay();
            ReplayRecorders = new List<ReplayRecorder>();
        }

        private void Start() {
            if (_PhotonView == null)
                _PhotonView = GetComponent<PhotonView>();
            if (_UIAdminReplay == null)
                _UIAdminReplay = FindObjectOfType<UIAdminReplay>();

            //MasterManager.GameSettings._GameMode = GameSettings.GameMode.Play;
            UIReplayGroup.SetActive(false);
        }

        /// <summary>
        /// Main logic of recording and replaying
        /// </summary>
        void Update() {
            // NOTE: FOR DEBUG ONLY. COMMENT this OUT!!!
            //foreach (var clip in Clips) {
            //    if(clip.RecorderRecording.ContainsKey(1))
            //        clip.RecordingDataToList = clip.RecorderRecording[1];
            //}
            
            if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Record) {
                // TODO: If Recorder isn't null? How properly it delets when player goes offline?
                foreach (var recorder in ReplayRecorders) {
                    recorder.CaptureFrame();
                }
            }

            if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Pause) {
                foreach (ReplayRecorder recorder in ReplayRecorders) {

                }
            }

            if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Replay)
            {
                // If NOT Admin && player shouldn't watch replay - do nothing
                if (MasterManager.GameSettings.MyFootballNetPlayer.RoleByte != 0 &&
                    !MasterManager.GameSettings.MyFootballNetPlayer.ShouldWatchReplay) {
                    //if (Debugging) Debug.LogWarning("MasterManager.GameSettings.MyFootballNetPlayer.ShouldWatchReplay: " + MasterManager.GameSettings.MyFootballNetPlayer.ShouldWatchReplay);

                    // Make animators dormant?

                    return;
                }

                foreach (ReplayRecorder recorder in ReplayRecorders) {
                    // Slider
                    if (SliderIsMoving) {
                        Debug.Log("ReplayManager. Setting Frame: " + Convert.ToInt32(_Slider.value));
                        recorder.SetFrame(Convert.ToInt32(_Slider.value));
                    }
                    else {
                        _Slider.value = recorder.GetCurrentFrameIndex();
                        //_Slider.maxValue = MaxLength;
                    }

                    recorder.Replay();
                }

                // Set Camera pos to current Admin Camera pos
                //if (!PhotonNetwork.IsMasterClient) {
                //    Debug.LogWarning("Starting to _cameraSpectator.SpectateReplay to sync", this);
                //    //MasterManager.GameSettings.MyFootballPlayer._cameraSpectator.SpectateReplay(_AdminCameraPosRotTransmitter.transform.position, _AdminCameraPosRotTransmitter.transform.rotation);
                //}
                Debug.LogWarning("Starting to _AdminCameraPosRotTransmitter.SpectateReplay to sync", this);
                _AdminCameraPosRotTransmitter.SpecateReplay();
            }


        }

        //public bool CheckIfRecorderWithThisIdIsRegistered(int id) {
        //    if()
        //}
        public int AddRecorderToListAndAssignId(ReplayRecorder replayRecorder) {
            ReplayRecorders.Add(replayRecorder);
            int maxIndexNow = ReplayRecorders.Max(x => x.RecorderId) + 1;
            return maxIndexNow;
        }

        //[PunRPC]
        //public void RPC_SetGameToPlay() {

        //}
        public void ButtonReplay() {

            _PhotonView.RPC("RPC_ButtonReplay", RpcTarget.All);
        }

        [PunRPC]
        public void RPC_ButtonReplay() {
            _AdminCameraPosRotTransmitter.SwitchAdminWatchTransmitter(true);

            SwitchPlayerAnimators(MasterManager.GameSettings.MyFootballNetPlayer.ShouldWatchReplay);

            if (!MasterManager.GameSettings.MyFootballNetPlayer.ShouldWatchReplay) return;

            Debug.Log("ReplayManager. RPC_ButtonReplay", this);

            PhotonRoomMatch.I.ReplayGame();

            foreach (var recorder in ReplayRecorders) {
                if (recorder.SnapshotFrames == null ||
                    (recorder.SnapshotFrames != null && recorder.SnapshotFrames.Count == 0))
                    recorder.TakeSnapshot();
            }

            MasterManager.GameSettings._GameMode = GameSettings.GameMode.Replay;
            _Slider.maxValue = ReplayRecorders[0].Length;

            foreach (var recorder in ReplayRecorders) {
                recorder.SetFrame(Convert.ToInt32(_Slider.value));

                if (ReplayRecorders[0].GetCurrentFrameIndex() >= ReplayRecorders[0].Length - 1) {
                    recorder.SetFrame(0);
                    _Slider.value = 0;
                }
            }
        }

        private void SwitchPlayerAnimators(bool state) {
            foreach (var footballPlayers in PhotonRoomMatch.I.FootballPlayers) {
                footballPlayers.TurnOffAnimators(state);
            }
        }

        public void ButtonStopRecord() {
            _UIAdminRecord.SwitchButtonRecordAccessability(true);
            _PhotonView.RPC("RPC_StopRecord", RpcTarget.All);
        }

        [PunRPC]
        public void RPC_StopRecord() {
            MasterManager.GameSettings._GameMode = GameSettings.GameMode.Play;
        }


        public void ButtonRecord() {
            _UIAdminRecord.SwitchButtonRecordAccessability(false);
            _PhotonView.RPC("RPC_StartToRecord", RpcTarget.All);
        }

        [PunRPC]
        public void RPC_StartToRecord() {
            // Setting for every recorder to know
            MasterManager.GameSettings._GameMode = GameSettings.GameMode.Record;

            //
            IncrementRecordIndexForWriting();

            foreach (var recorder in ReplayRecorders) {
                if (SelectedClip.RecorderRecording.ContainsKey(recorder.RecorderId))
                    SelectedClip.RecorderRecording[recorder.RecorderId].Frames.Clear();
                recorder.Length = 0;
                recorder.SetFrame(0);
            }
        }

        public void ButtonPauseGameSwitch() {
            if (!PhotonNetwork.IsMasterClient) return;

            if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Play) {
                ButtonPause();
            }
            else if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Pause) {
                ButtonPlay();
            }
        }

        public void ButtonPlay() {
            _PhotonView.RPC("RPC_UnPauseGame", RpcTarget.All);

            UIRecordGroup.SetActive(true);
            UIReplayGroup.SetActive(false);

            MasterManager.GameSettings.InitMatchStart();

            _PlayersWhoWatchReplayListingsMenu.GamesUnPaused();
        }

        public void ButtonPause() {
            _PhotonView.RPC("RPC_PauseGame", RpcTarget.All);

            UIRecordGroup.SetActive(false);
            UIReplayGroup.SetActive(true);

            MasterManager.GameSettings.InitMatchStart();

            _PlayersWhoWatchReplayListingsMenu.GamesPaused();
        }

        [PunRPC]
        public void RPC_PauseGame() {
            //Debug.Log("PhotonRoomMatch. RPC_PauseGame", this);
            Time.timeScale = 0.0004f;
            MasterManager.GameSettings._GameMode = GameSettings.GameMode.Pause;
        }

        [PunRPC]
        public void RPC_UnPauseGame() {
            Debug.Log("PhotonRoomMatch. RPC_UnPauseGame", this);

            _AdminCameraPosRotTransmitter.SwitchAdminWatchTransmitter(false);
            Time.timeScale = 1.0f;

            if (ReplayRecorders != null)
                foreach (var recorder in ReplayRecorders) {
                    if (recorder.SnapshotFrames != null && recorder.SnapshotFrames.Count > 0)
                        recorder.ReturnToSnapshotFrame();
                }
            MasterManager.GameSettings._GameMode = GameSettings.GameMode.Play;

            SwitchPlayerAnimators(true);
            PhotonRoomMatch.I.UnpauseGame();
        }

        public void SliderClick() {
            SliderIsMoving = true;
        }

        public void SliderRelease() {
            SliderIsMoving = false;
        }

        private int IncrementRecordIndexForWriting() {
            var tempIndex = SelectedClipIndex + 1;
            if (tempIndex >= 10)
                tempIndex = 0;

            SelectedClipIndex = tempIndex;
            ButtonChangeSelectedClip(SelectedClipIndex);
            return SelectedClipIndex;
        }

        public void ButtonChangeSelectedClip(int clipIndex) {
            _PhotonView.RPC("RPC_ChangeSelectedClip", RpcTarget.AllBufferedViaServer, clipIndex);
        }

        [PunRPC]
        private void RPC_ChangeSelectedClip(int clipIndex) {
            SelectedClipIndex = clipIndex;
        }
    }
}