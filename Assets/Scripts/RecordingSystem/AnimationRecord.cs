﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationRecord {
    public string Name;
    public float FloatValue;
    public int IntValue;
    public bool BoolValue;

    public AnimatorControllerParameterType Type;

    public AnimationRecord(string n, float val, AnimatorControllerParameterType type) {
        Name = n;
        FloatValue = val;
        Type = type;
    }

    public AnimationRecord(string n, int val, AnimatorControllerParameterType type) {
        Name = n;
        IntValue = val;
        Type = type;
    }

    public AnimationRecord(string n, bool val, AnimatorControllerParameterType type) {
        Name = n;
        BoolValue = val;
        Type = type;
    }
}
