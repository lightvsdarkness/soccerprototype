﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Frame {
    public GameObject _GameObject;
    public bool State;

    public Vector3 Position;
    public Quaternion Rotation;
    public Vector3 Scale;

    public List<AnimationRecord> AnimationRecords = new List<AnimationRecord>();

    public Frame(GameObject gameObject, Vector3 position, Quaternion rotation, Vector3 scale, List<AnimationRecord> animationRecords) {
        _GameObject = gameObject;
        State = gameObject.activeSelf;

        Position = position;
        Rotation = rotation;
        Scale = scale;

        AnimationRecords = animationRecords;
    }

}