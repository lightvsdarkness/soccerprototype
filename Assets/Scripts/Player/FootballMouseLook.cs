﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Soccer
{
    [Serializable]
    public class FootballMouseLook
    {
        public float XSensitivity = 2f;
        public float YSensitivity = 2f;
        public bool clampVerticalRotation = true;
        public float MinimumX = -90F;
        public float MaximumX = 90F;
        public bool smooth;
        public float smoothTime = 5f;
        public bool lockCursor = true;

        private Transform _character;
        private Quaternion m_CharacterTargetRot;
        private Quaternion m_CameraTargetRot;
        public bool CursorIsHidden = true;

        public float TimeToStopTrackingBall = 0.8f;
        public float deltaTimePassed = 0f;
        public Transform TrackedSphereWhenBallisControlled;
        private FootballPlayer _footballPlayer;

        public void Init(Transform character, Transform camera) {
            _character = character;
            _footballPlayer = _character.GetComponent<FootballPlayer>();

            m_CharacterTargetRot = character.localRotation;
            m_CameraTargetRot = camera.localRotation;
        }


        public void LookRotation(Transform character, Transform camera) {
            deltaTimePassed += Time.deltaTime;

            float yRot = CrossPlatformInputManager.GetAxis("Mouse X") * XSensitivity;
            float xRot = CrossPlatformInputManager.GetAxis("Mouse Y") * YSensitivity;
            //Debug.LogWarning("Mouse X" + yRot);
            //Debug.LogWarning("Mouse Y" + xRot);

            // Track Ball if no mouse input, stop tracking Ball if mouse input
            //if (yRot < 0.01f && xRot < 0.01f && deltaTimePassed >= TimeToStopTrackingBall)
            //{
            //    MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.Ball;
            //    if (ManagerGame.I.TrackedTransform != Ball.BallTransform)
            //    {
            //        ManagerGame.I.TrackedTransform = Ball.BallTransform;
            //    }
            //}
            //else if (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball)
            //{
            //    Debug.LogWarning("Setting to GameSettings.TrackingMode.None");
            //    deltaTimePassed = 0;
            //    MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.None;
            //}

            // Tracking on for ball IF not Admin
            /* ||
                (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Player ||
                MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Place */

            if (_footballPlayer._CameraView == CameraView.FirstPersonView) {

            }
            if (_footballPlayer._CameraView == CameraView.ThirdPersonView) {

            }
            // ACHTUNG ADMIN CHARACTER ROTATION
            if (_footballPlayer.State == FootballPlayerState.AdminAssumedControl)
            {
                m_CharacterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
                character.localRotation = Quaternion.Slerp(character.localRotation, m_CharacterTargetRot,
    smoothTime * Time.deltaTime);
            }

            // If Tracking Ball
            if ((MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball &&
                _footballPlayer._Ball == null ) &&
                ManagerGame.I.TrackedTransform != null) // Admin  && _footballPlayer._FootballNetPlayer.RoleByte != 0
            {
                var direction = ManagerGame.I.TrackedTransform.position - camera.transform.position;
                Quaternion rotation = Quaternion.LookRotation(direction);
                //m_CameraTargetRot = rotation;
                camera.rotation = Quaternion.Slerp(camera.rotation, rotation, 10f * Time.deltaTime);
            }
            //else if (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball &&
            //    _footballPlayer._Ball?.transform == Ball.BallTransform)
            //{
            //    var direction = TrackedSphereWhenBallisControlled.position - camera.transform.position;
            //    Quaternion rotation = Quaternion.LookRotation(direction);
            //    //m_CameraTargetRot = rotation;
            //    camera.rotation = Quaternion.Slerp(camera.rotation, rotation, 2.5f * Time.deltaTime);
            //}
            else if (_footballPlayer.State == FootballPlayerState.AdminAssumedControl)
                // && !_footballPlayer._PlayerInput.AdminAssumedControlInputModeMoving
            {
            }
            else
            { 
                m_CharacterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
                m_CameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);

                if (clampVerticalRotation)
                    m_CameraTargetRot = ClampRotationAroundXAxis(m_CameraTargetRot);

                if (smooth)
                {
                    character.localRotation = Quaternion.Slerp(character.localRotation, m_CharacterTargetRot,
                        smoothTime * Time.deltaTime);
                    //camera.localRotation = Quaternion.Slerp(camera.localRotation, m_CameraTargetRot, smoothTime * Time.deltaTime);
                }
                else
                {
                    character.localRotation = m_CharacterTargetRot;
                    camera.localRotation = m_CameraTargetRot;
                }

                    //UpdateCursorLock();
            }

            // ACHTUNG ADMIN CAMERA ROTATION
            if (_footballPlayer.State == FootballPlayerState.AdminAssumedControl && (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball ||
             MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Player ||
             MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Place) &&
             ManagerGame.I.TrackedTransform != null)
            {
                var direction = ManagerGame.I.TrackedTransform.position - camera.position;
                Quaternion rotation = Quaternion.LookRotation(direction);
                //m_CameraTargetRot = rotation;
                camera.rotation = Quaternion.Slerp(camera.rotation, rotation, 5 * Time.deltaTime);
            }
        }


        public void SetCursorLock(bool value) {
            lockCursor = value;
            if (!lockCursor)
            {//we force unlock the cursor if the user disable the cursor locking helper
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        public void UpdateCursorLock() {
            //if the user set "lockCursor" we check & properly lock the cursos
            if (lockCursor)
                InternalLockUpdate();
        }

        /// <summary>
        /// NOTE: Input
        /// </summary>
        private void InternalLockUpdate() {
            if (Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.Tab))
            {
                CursorIsHidden = !CursorIsHidden;
            }
            //else if (Input.GetMouseButtonUp(0))
            //{
            //    m_cursorIsLocked = true;
            //}

            // 
            if (CursorIsHidden)
            {
                //Debug.Log("Hiding cursor");
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else if (!CursorIsHidden)
            {
                //Debug.Log("Hiding cursor");
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        Quaternion ClampRotationAroundXAxis(Quaternion q) {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

        public void ReturnOriginalPosRot() {
            _footballPlayer.transform.localPosition = _footballPlayer._originalCameraPositionLocal;
            _footballPlayer.transform.localRotation = _footballPlayer._originalCameraRotationLocal;
        }

        public void SharpLookAtTargetTransform(Transform targetTransform, Transform camera) {
            var direction = targetTransform.position - camera.transform.position;

            camera.rotation = Quaternion.LookRotation(direction);
        }
    }
}
