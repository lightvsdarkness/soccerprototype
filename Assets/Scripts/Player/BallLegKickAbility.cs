﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;

namespace Soccer {
    public class BallLegKickAbility : MonoBehaviour {
        [SerializeField] private bool Debugging;

        [Header("Trajectory")]
        public int SelectedKickTransform = 0;
        public Transform IdealLegKickTransform;
        public List<Transform> LegKicksTransforms = new List<Transform>();
        public List<Vector3> KicksTrajectories = new List<Vector3> {new Vector3(0f, 0.05f, 1f), new Vector3(0f, 0.25f, 1f), new Vector3(0f, 0.55f, 1f) };
        public Transform IdealSoccerPlayerLegTransform; // Transform variable to hold the location where we will spawn our projectile
        //public List<int> KicksTrajectories = new List<int> { 0, 1, 2 };

        [Header("Strength")]
        public int SelectedStrength = 0;
        public List<float> KicksStrengths = new List<float> { 125f, 250f, 380f };
        public float ProjectileForce = 250f; // Float variable to hold the amount of force which we will apply to launch our projectiles

        [Header("Links")]
        public FootballPlayer _FootballPlayer;
        public AbilityCaster _AbilityCaster;
        public Ball _Ball; // Rigidbody variable to hold a reference to our projectile prefab


        private void Awake() {
            if (_FootballPlayer == null)
                _FootballPlayer = GetComponentInParent<FootballPlayer>();
            if (_AbilityCaster == null)
                _AbilityCaster = GetComponentInParent<AbilityCaster>();
        }

        public void Initialize(AbilityData abilityData, Transform legKickHolder, Transform soccerPlayerLegHolder) {
            IdealLegKickTransform = legKickHolder;
            IdealSoccerPlayerLegTransform = soccerPlayerLegHolder;
            var projectileAbilityData = abilityData as ProjectileAbilityData;

            ProjectileForce = projectileAbilityData.projectileForce;


            TuneAbilityStrength(0);
            TuneAbilityTrajectory(0);
        }

        public void TuneAbilityStrength(int strengthLevel) {
            SelectedStrength = strengthLevel;
            _AbilityCaster._UIKickLegController.IndicateChangedStrength(strengthLevel);
        }
        public void TuneAbilityTrajectory(int trajectoryLevel) {
            SelectedKickTransform = trajectoryLevel;
            _AbilityCaster._UIKickLegController.IndicateChangedTrajectory(trajectoryLevel);
        }

        public void UseAbility(Ball ball) {
            _Ball = ball;
            if (_Ball == null) return; // ACHTUNG: What if someone takes the ball during the waiting?
            Debug.Log("BallLegKickAbility. UseAbility", this);

            //Instantiate a copy of our projectile and store it in a new rigidbody variable called clonedBall
            //Rigidbody clonedBall = Instantiate(Projectile, IdealLegKickTransform.position, transform.rotation) as Rigidbody;

            //Add force to the instantiated bullet, pushing it forward away from the bulletSpawn location, using projectile force for how hard to push it away
            //_Ball.DepartPlayer();
            if (Debugging)
                Debug.LogWarning("UseAbility. KicksTrajectories[SelectedKickTransform]: " + KicksTrajectories[SelectedKickTransform]);
            if (Debugging)
                Debug.LogWarning("UseAbility. SelectedStrength: " + SelectedStrength);
            //LegKicksTransforms[SelectedKickTransform].transform.forward
            object[] info = new object[] {
                IdealLegKickTransform.position,
                KicksTrajectories[SelectedKickTransform] * KicksStrengths[SelectedStrength] / 20
            }; 
            _Ball._PhotonView.RPC("RPC_KickBallWithLeg", RpcTarget.AllViaServer, info); //MasterClient //All


            // NOTE:
            //_FootballPlayer._PhotonView.RPC("RPC_BallDeparted", RpcTarget.AllViaServer);


            //_Ball.transform.Translate(IdealLegKickTransform.forward * 0.2f);
            //_Ball.GetComponent<Rigidbody>().AddForce(IdealLegKickTransform.transform.forward * ProjectileForce);

            //RaycastHit hit;
            //if (Physics.Raycast(IdealSoccerPlayerLegTransform.position, IdealSoccerPlayerLegTransform.forward, out hit, 1f)) {
            //    _Ball.GetComponent<Rigidbody>()
            //       .AddForceAtPosition(IdealLegKickTransform.transform.forward*ProjectileForce, hit.point, ForceMode.Impulse);
            //}


        }
    }
}