﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTeleporter : MonoBehaviour
{

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow))
            TeleportTribune1();
        if (Input.GetKeyDown(KeyCode.DownArrow))
            TeleportTribune2();
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            TeleportTribune3();
        if (Input.GetKeyDown(KeyCode.RightArrow))
            TeleportTribune4();
    }


    void TeleportTribune1() {
        transform.position = ManagerGame.I.SpectatorInitialPosition.position;
    }
    void TeleportTribune2() {
        transform.position = ManagerGame.I.SpectatorPosition2.position;
    }
    void TeleportTribune3() {
        transform.position = ManagerGame.I.SpectatorPosition3.position;
    }
    void TeleportTribune4() {
        transform.position = ManagerGame.I.SpectatorPosition4.position;
    }
}
