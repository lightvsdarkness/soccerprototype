﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Soccer {
    public class FootballPlayerStateManager : MonoBehaviour {
        [SerializeField] private FootballPlayer _FootballPlayer;
        [SerializeField] private FootballNetPlayer _FootballNetPlayer;


        void Start() {
            if (_FootballPlayer == null)
                _FootballPlayer = GetComponent<FootballPlayer>();
            if (_FootballNetPlayer == null)
                _FootballNetPlayer = GetComponent<FootballNetPlayer>();
        }
        void Update() {
            // For reliability
            //if (_FootballPlayer.State == FootballPlayerState.AdminObserving &&
            //    _FootballPlayer.PreviousState != FootballPlayerState.AdminObserving) {
            //    _FootballPlayer.ResolveStateChange();
            //}

            _FootballPlayer.ResolveRoleChange();
            _FootballPlayer.ResolveStateChange();

        }
    }
}