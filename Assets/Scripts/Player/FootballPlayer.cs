﻿using System;
using System.Collections;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;


namespace Soccer {
    public enum FootballPlayerState {
        Playing,
        Ghost,
        Choosing,
        PlayerDeactivatedByAdmin,
        AdminObserving,
        AdminAssumedControl
    }
    public enum CameraView {
        FirstPersonView,
        ThirdPersonView
    }

    [RequireComponent(typeof (CharacterController))]
    [RequireComponent(typeof (AudioSource))]
    public class FootballPlayer : MonoBehaviour {
        public bool Debugging;
        public FootballPlayerState _state = FootballPlayerState.Choosing;
        private bool _flagStateChanged = false;

        public FootballPlayerState State {
            get { return _state;  }
            set {
                if (Debugging)Debug.Log("Changed State to: " + value, this);
                _flagStateChanged = true;
                //ResolveStateChange();
                //if (value == FootballPlayerState.AdminObserving)
                //    Debug.LogError("value");
                _state = value;
            }
        }
        public FootballPlayerState PreviousState = FootballPlayerState.Choosing;
        public bool ChoosingModel = true;

        [Header("Ball")]
        public Transform PointBall;
        public Ball _Ball;
        public float BallStayCount;

        public UIBall _UIBall;
        public UnityEngine.UI.Image _UIBallImage;
        public TextMeshProUGUI _UIBallText;

        [Header("Moving")]
        [SerializeField] private bool m_IsWalking;
        [SerializeField] private float m_WalkSpeed;
        [SerializeField] private float m_RunSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;

        [Header("Camera")]
        public CameraView _CameraView;
        [SerializeField] private FootballMouseLook m_MouseLook;
        //[SerializeField] private bool m_UseFovKick;
        //[SerializeField] private FOVKick m_FovKick = new FOVKick();
        [SerializeField] private bool m_UseHeadBob;
        [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();


        [Space]
        [SerializeField] private float m_StepInterval;

        [SerializeField] private AudioClip[] m_FootstepSounds;
            // an array of footstep sounds that will be randomly selected from.

        [SerializeField] private AudioClip m_JumpSound; // the sound played when character leaves the ground.
        [SerializeField] private AudioClip m_LandSound; // the sound played when character touches back on ground.

        [Header("Links")]
        public PhotonView _PhotonView;
        public FootballNetPlayer _FootballNetPlayer;
        public CameraSpectator _cameraSpectator;
        public AbilityCaster _AbilityCaster;
        public PlayerInput _PlayerInput;

        public PlayerMovementNavMesh _PlayerMovementNavMesh;
        //public NavMeshAgent _NavMeshAgent;
        public AdminController _AdminController;

        public FootballPlayerVisuals _FootballPlayerVisuals;
        [SerializeField] private GameObject Visuals;
        public GameObject VisualGoalkeeper;
        [HideInInspector] public Animator VisualGoalkeeperAnimator;
        public GameObject VisualMain;
        [HideInInspector] public Animator VisualMainAnimator;

        [SerializeField] private Camera m_Camera;
        [SerializeField] private AudioListener _audioListener;

        private bool m_Jump;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;

        [Header("Camera Position")]
        [SerializeField] private Vector3 _activeModel1stPersonCameraPosition;
        [SerializeField] private Vector3 _original1stPersonCameraPosition = new Vector3(0f, 0.75f, 0.4f);
        public Vector3 _originalCameraPositionLocal = new Vector3(0f, 1.78f, -2f);
        public Quaternion _originalCameraRotationLocal = Quaternion.identity;
        [SerializeField] private Vector3 _previousTurnCameraPositionLocal = Vector3.zero;

        [SerializeField] private float _lerpedBallDistModifier = 1f;
        [SerializeField] private float _calculatedBallDistModifier; // for debugging

        [SerializeField] private float _cameraDistanceModifier {get { return MasterManager.GameSettings.CameraDistanceModifier; } }
        [SerializeField] private float _ballDistanceModifier { get { return MasterManager.GameSettings.CameraDistanceBallModifier; } }
        

        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        private AudioSource m_AudioSource;


        private void Awake() {
            m_CharacterController = GetComponent<CharacterController>();
            if (_PhotonView == null)
                _PhotonView = GetComponent<PhotonView>();
            if (_FootballNetPlayer == null)
                _FootballNetPlayer = GetComponentInChildren<FootballNetPlayer>();
            if (m_Camera == null)
                m_Camera = GetComponentInChildren<Camera>();
            if (_audioListener == null)
                _audioListener = GetComponentInChildren<AudioListener>();
            if (m_AudioSource == null)
                m_AudioSource = GetComponent<AudioSource>();
            if (_PlayerInput == null)
                _PlayerInput = GetComponent<PlayerInput>();

            if (_PlayerMovementNavMesh == null)
                _PlayerMovementNavMesh = GetComponent<PlayerMovementNavMesh>();
            if (_AdminController == null)
                _AdminController = GetComponent<AdminController>();

            VisualGoalkeeperAnimator = VisualGoalkeeper.GetComponent<Animator>();
            VisualMainAnimator = VisualMain.GetComponent<Animator>();

            if (_UIBall == null)
                _UIBall = FindObjectOfType<UIBall>();
            if (_PhotonView.IsMine) {
                // Check for Local Player
                _UIBall._FootballPlayer = this;
            }
                

            m_HeadBob.ActiveModelEyes = _FootballPlayerVisuals.EyesOrdinaryT;


            if (!_PhotonView.IsMine)
            {
                m_Camera.enabled = false;
                _audioListener.enabled = false;

            }

            // Each instantiated prefab, if it's not player's, will try to get info about itself from MasterClient
            if (!_PhotonView.IsMine)
                _PhotonView.RPC("RPC_GetInfoAboutMe", RpcTarget.MasterClient);
        }

        private void Start() {
            _flagStateChanged = true;

            //m_OriginalCameraPosition = m_Camera.transform.position;
            _activeModel1stPersonCameraPosition = m_HeadBob.ActiveModelEyes.position;
            //_originalCameraPositionLocal = m_HeadBob.ActiveModelEyes.localPosition;
            //m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle/2f;
            m_Jumping = false;

            m_MouseLook.Init(transform, m_Camera.transform);
            _originalCameraRotationLocal = m_Camera.transform.localRotation;
        }


        private void Update() {
            if (Debugging)
                Debug.DrawRay(transform.position, transform.forward, Color.red);
            if (Debugging)
                Debug.Log("FootballPlayer. FootballNetPlayer.RoleByte: " + _FootballNetPlayer.RoleByte);

            ResolveRoleChange();

            // For reliability
            ResolveStateChange();



            if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Replay
               ) // && (State != FootballPlayerState.AdminObserving && State != FootballPlayerState.AdminAssumedControl)
            {
                ChangeVisualRepresentationIsActive(true);

                m_CharacterController.enabled = false;
                this.enabled = false;
                _cameraSpectator.SwitchCameraSpectatorState(true);
            }

            if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Play &&
                State == FootballPlayerState.Playing &&
                !ChoosingModel)
            {
                ChangeVisualRepresentationIsActive(true);
                m_CharacterController.enabled = true;
                this.enabled = true;
                _cameraSpectator.SwitchCameraSpectatorState(false);
            }

            PreviousState = State;


            if (!_PhotonView.IsMine) return;

            if (_Ball == null) {
                RPC_TurnBallUI(false);
            }

            //if (_FootballNetPlayer.RoleByte != (byte)GameSettings.Role.Ghost && _Ball != null)
            //    BallControl();

            m_MouseLook.UpdateCursorLock();

            // Camera Rotation - MouseLook 
            if (!MasterManager.GameSettings.MenuOpened)
                m_MouseLook.LookRotation(transform, m_Camera.transform);

            // Input. the jump state needs to read here to make sure it is not missed
            if (!m_Jump) {
                m_Jump = _PlayerInput.GetFootbalPlayerJumpInput();
            }

            if (!m_PreviouslyGrounded && m_CharacterController.isGrounded) {
                //StartCoroutine(m_JumpBob.DoBobCycle());
                PlayLandingSound();
                m_MoveDir.y = 0f;
                m_Jumping = false;
            }
            if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded) {
                m_MoveDir.y = 0f;
            }

            m_PreviouslyGrounded = m_CharacterController.isGrounded;
        }

        /// <summary>
        /// Moving & Camera
        /// </summary>
        private void FixedUpdate() {
            if (!_PhotonView.IsMine) return;

            m_Input = _PlayerInput.GetFootbalPlayerMovementInput();
            float modifiedSpeed;

            GetModifiedInput(m_Input, out modifiedSpeed);
            //Debug.LogWarning("movementInput " + movementInput);
            //Debug.LogWarning("modifiedSpeed " + modifiedSpeed);
            // Desired moving
            Vector3 desiredMove;
            if (_CameraView == CameraView.ThirdPersonView) {
                // always move along the camera forward as it is the direction that it being aimed at
                desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;
            }
            else
            {
                desiredMove = m_Camera.transform.forward * m_Input.y + transform.right * m_Input.x;
            }


            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                m_CharacterController.height / 2f, ~0, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            m_MoveDir.x = desiredMove.x * modifiedSpeed;
            m_MoveDir.z = desiredMove.z * modifiedSpeed;


            if (m_CharacterController.isGrounded)
            {
                m_MoveDir.y = -m_StickToGroundForce;

                //if (m_Jump) {
                //    m_MoveDir.y = m_JumpSpeed;
                //    PlayJumpSound();
                //    m_Jump = false;
                //    m_Jumping = true;
                //}
            }
            else
            {
                m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
            }
            //Debug.LogWarning("m_MoveDir " + m_MoveDir);
            // Finally, MOVE!
            m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);

            ProgressStepCycle(modifiedSpeed);


            // Camera stuff
            // TODO: HeadBob localPosition???
            if (State != FootballPlayerState.AdminAssumedControl)
            {
                //UpdateCameraPosition(speed);
                if (_CameraView == CameraView.FirstPersonView) {
                    UpdateCameraPosition1stPerson(MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball);
                }
                if (_CameraView == CameraView.ThirdPersonView) {
                    UpdateCameraPosition3rdPerson(MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball);
                }
            }
            //else if (State == FootballPlayerState.AdminAssumedControl && !_PlayerInput.AdminAssumedControlInputModeMoving) {
            //    //HoldCameraPosition();
            //}

        }

        public void ResolveRoleChange() {
            // Playing, Admin, or Ghost
            //var mySocNetPlayer = PhotonRoomMatch.I.FootballNetPlayers.Find(x => x.Nickname == PhotonNetwork.NickName);
            if (_FootballNetPlayer.RoleByte == (byte)GameSettings.Role.Player)
            {
                //_SoccerNetPlayer._Role == GameSettings.Role.Ghost
                State = FootballPlayerState.Playing;
            }
            else if (_FootballNetPlayer.RoleByte == (byte)GameSettings.Role.Administrator)
            {
                if (_AdminController.InitialObserving)
                {
                    if (Debugging) Debug.LogWarning("AdminObserving");
                    if (State != FootballPlayerState.AdminAssumedControl)
                    {
                        State = FootballPlayerState.AdminObserving;
                        ChoosingModel = false;
                    }

                    _AdminController.InitialObserving = false;
                }

            }
            else if (_FootballNetPlayer.RoleByte == (byte)GameSettings.Role.PlayerDeactivatedByAdmin)
            {
                State = FootballPlayerState.PlayerDeactivatedByAdmin;
            }
            else
            {
                State = FootballPlayerState.Ghost;
            }
            if (Debugging) Debug.Log("FootballPlayer. State: " + State);

            // Initial choosing
            if (ChoosingModel)
            {
                State = FootballPlayerState.Choosing;
            }
        }

        public void ResolveStateChange() {

            switch (State)
            {
                case FootballPlayerState.Playing:
                    SwitchToPlayerOrGhost(true);
                    _cameraSpectator.SwitchCameraSpectatorState(false);

                    if (PreviousState == FootballPlayerState.Ghost)
                        _cameraSpectator.ReturnToInitialPosition();
                    break;
                case FootballPlayerState.Ghost:
                    SwitchToPlayerOrGhost(false);
                    //Debug.LogWarning($"I am: {gameObject.name} _FootballNetPlayer.IsGoalkeeper: {_FootballNetPlayer.IsGoalkeeper}. Set VisualGoalkeeper to: {VisualGoalkeeper.activeSelf}", this);
                    ChangeVisualCompleteOffCrutch();
                    _cameraSpectator.enableMouseFireCapture = false;
                    break;
                case FootballPlayerState.PlayerDeactivatedByAdmin:
                    SwitchToPlayerOrGhost(false);
                    //Debug.LogWarning($"I am: {gameObject.name} _FootballNetPlayer.IsGoalkeeper: {_FootballNetPlayer.IsGoalkeeper}. Set VisualGoalkeeper to: {VisualGoalkeeper.activeSelf}", this);
                    ChangeVisualCompleteOffCrutch();
                    _cameraSpectator.enableMouseFireCapture = false;
                    break;

                case FootballPlayerState.Choosing:
                    SwitchToPlayerOrGhost(false);
                    _cameraSpectator.enableMouseFireCapture = true;
                    break;
                case FootballPlayerState.AdminObserving:
                    // 
                    //if (_flagStateChanged)
                    //{
                        SwitchToPlayerOrGhost(false);
                        _cameraSpectator.enableMouseFireCapture = false;
                        ChangeVisualCompleteOffCrutch();
                    //}
                    break;
                case FootballPlayerState.AdminAssumedControl:
                    // Admin took control 
                    //if (_flagStateChanged)
                    //{
                    ChangeVisualRepresentationIsActive(true);
                    m_CharacterController.enabled = _PlayerInput.AdminAssumedControlInputModeMoving;
                    this.enabled = _PlayerInput.AdminAssumedControlInputModeMoving;

                    _cameraSpectator.SwitchCameraSpectatorState(!_PlayerInput.AdminAssumedControlInputModeMoving);
                    _cameraSpectator.enableMouseFireCapture = false;

                    //}
                    break;

                default:
                    break;
            }
        }


        [PunRPC]
        public void RPC_GetInfoAboutMe(PhotonMessageInfo info) {
            if (Debugging)
                Debug.LogFormat("FootballPlayer. RPC_GetInfoAboutMe: {0}", info.Sender, this);

            _PhotonView.RPC("RPC_SendInfoAboutMe", info.Sender, ChoosingModel, State);
        }

        [PunRPC]
        public void RPC_SendInfoAboutMe(bool choosingModel, FootballPlayerState state, PhotonMessageInfo info) {
            // the photonView.RPC() call is the same as without the info parameter. the info.Sender is the player who called the RPC.
            if (Debugging)
                Debug.Log("FootballPlayer. RPC_SendInfoAboutMe. sender: " + info.Sender + " choosingModel: " + choosingModel + " State: " + state, this);
            ChoosingModel = choosingModel;

            State = state;
            if (State == FootballPlayerState.Playing)
                this.enabled = true;
            if (State == FootballPlayerState.Ghost)
                this.enabled = false;
        }

        /// <summary>
        /// True if player, false if ghost
        /// </summary>
        public void SwitchToPlayerOrGhost(bool playerIsActive) {
            ChangeVisualRepresentationIsActive(playerIsActive);
            m_CharacterController.enabled = playerIsActive;
            this.enabled = playerIsActive;
            _cameraSpectator.SwitchCameraSpectatorState(!playerIsActive);

            //_flagStateChanged = false;
        }
        //public void SwitchToAdminAssumingControl() {
        //    SwitchVisualRepresentationIsActive(true);
        //    m_CharacterController.enabled = true;
        //    this.enabled = true;
        //    _cameraSpectator.enabled = false;
        //}

        /// <summary>
        /// VERY IMPORTANT METHOD
        /// </summary>
        [PunRPC]
        public void RPC_IsRoleChanged() {
            if (Debugging) Debug.Log("FootballPlayer. RPC_RoleChanged" + _FootballNetPlayer.RoleByte);

            if (_FootballNetPlayer.RoleByte == 2) {
                _cameraSpectator.transform.position = ManagerGame.I.SpectatorInitialPosition.position;
            }

            if (_FootballNetPlayer.RoleByte == 0) {
                //State = FootballPlayerState.AdminObserving;
            }

            if (_FootballNetPlayer.RoleByte == 1) {
                State = FootballPlayerState.Playing;
                this.enabled = true; // ACHTUNG: Change the FootballPlayerStateManager
                ChangeVisualRepresentationIsActive(true);
                Debug.LogWarning("State: " + State);
            }
                

            if (_FootballNetPlayer.IsGoalkeeper) {
                ChangeVisualMainOrGoalkeeper(false);
                //Debug.LogWarning($"I am: {gameObject.name} _FootballNetPlayer.IsGoalkeeper: {_FootballNetPlayer.IsGoalkeeper}. Set VisualGoalkeeper to: {VisualGoalkeeper.activeSelf}", this);
            }
            else
                ChangeVisualMainOrGoalkeeper(true);
        }

        [PunRPC]
        public void RPC_IsGoalkeeperChanged() {
            if (Debugging) Debug.LogWarning($"I am: {gameObject.name} FootballPlayer. RPC_IsGoalkeeperChanged to: " + _FootballNetPlayer.IsGoalkeeper, this);

            if (_FootballNetPlayer.IsGoalkeeper) {
                m_HeadBob.ActiveModelEyes = _FootballPlayerVisuals.EyesGoalkeeperT;
                ChangeVisualMainOrGoalkeeper(false);
            }
            else {
                m_HeadBob.ActiveModelEyes = _FootballPlayerVisuals.EyesOrdinaryT;
                ChangeVisualMainOrGoalkeeper(true);
            }
            //
            _FootballNetPlayer.RefreshFNPTeam();
            _FootballNetPlayer.RefreshFNPNumber();
        }

        [PunRPC]
        public void RPC_MakeChoiceAndStartGame() {
            if (State == FootballPlayerState.Choosing && ChoosingModel)
            {
                if (Debugging)
                    Debug.Log("FootballPlayer. State = FootballPlayerState.Playing");
                ChoosingModel = false;
                State = FootballPlayerState.Playing;
                if (Debugging)
                    Debug.Log("FootballPlayer. RPC_MakeChoiceAndStartGame. old position: " + transform.position, this);
                transform.position = PhotonRoomMatch.I.SpawnPlayingPoints[0].position;
                if (Debugging)
                    Debug.Log("FootballPlayer. RPC_MakeChoiceAndStartGame. old position: " + transform.position, this);
                if (Debugging)
                    Debug.Log("FootballPlayer. RPC_MakeChoiceAndStartGame. old rotation: " + transform.rotation, this);
                transform.rotation = Quaternion.LookRotation(Ball.BallPosition - transform.position);
                m_Camera.transform.rotation = Quaternion.LookRotation(Ball.BallPosition - transform.position);
                if (Debugging)
                    Debug.Log("FootballPlayer. RPC_MakeChoiceAndStartGame. new rotation: " + transform.rotation, this);

                MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.Ball;
                ManagerGame.I.TrackedTransform = Ball.BallTransform;
                 this.enabled = true;
            }
        }

        [ContextMenu("GetRotation")]
        public void GetRotation() {
            if (Debugging)
                Debug.Log("FootballPlayer. RPC_MakeChoiceAndStartGame. current rotation: " + transform.rotation, this);
        }

        [PunRPC]
        private void RPC_TurnBallUI(bool state) {
            _UIBall.SwitchUIBall(state);
        }

        private void PlayLandingSound() {
            m_AudioSource.clip = m_LandSound;
            m_AudioSource.Play();
            m_NextStep = m_StepCycle + .5f;
        }


        [PunRPC]
        public void RPC_BallDeparted() {
            _Ball = null;
            foreach (var player in PhotonRoomMatch.I.FootballPlayers) {
                player._Ball = null;
            }

            //RPC_TurnBallUI(false);
            _UIBall.SwitchUIBall(false);

            if (Debugging)
                Debug.Log("FootballPlayer. RPC_BallDeparted end", this);
        }

        private void PlayJumpSound() {
            m_AudioSource.clip = m_JumpSound;
            m_AudioSource.Play();
        }


        private void ProgressStepCycle(float speed) {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0)) {
                m_StepCycle += (m_CharacterController.velocity.magnitude +
                                (speed*(m_IsWalking ? 1f : m_RunstepLenghten)))*
                               Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep)) {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }


        private void PlayFootStepAudio() {
            if (!m_CharacterController.isGrounded) {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }

        private void UpdateCameraPosition1stPerson(bool trackingBall) {
            Vector3 newCameraPosition = transform.localPosition;

            if ((MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball && _Ball == null) &&
                ManagerGame.I.TrackedTransform != null) // Admin  && _footballPlayer._FootballNetPlayer.RoleByte != 0
            {

                newCameraPosition = Vector3.Lerp(m_Camera.transform.position, m_HeadBob.ActiveModelEyes.position,
                    1f * Time.deltaTime);
            }
            else
            {
                newCameraPosition = Vector3.Lerp(m_Camera.transform.position, m_HeadBob.ActiveModelEyes.position,
                    1f*Time.deltaTime);
                //m_Camera.transform.position = newCameraPosition;
                m_Camera.transform.position = newCameraPosition;
            }
        }

        private void UpdateCameraPosition3rdPerson(bool trackingBall) {
            Vector3 newCameraPosition = transform.localPosition;

            if ((MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball && _Ball == null) &&
                ManagerGame.I.TrackedTransform != null) // Admin  && _footballPlayer._FootballNetPlayer.RoleByte != 0
            {
                // Magnitude
                var direction = (Ball.BallPosition - transform.position);
                _lerpedBallDistModifier = Mathf.Lerp(direction.magnitude/10f, 1f, 0.5f);
                _calculatedBallDistModifier = Mathf.Clamp(_lerpedBallDistModifier * _ballDistanceModifier, 0.85f, 2.5f);

                var directionNormalized = (Ball.BallPosition - transform.position).normalized;
                newCameraPosition = transform.position + directionNormalized * -3 * _calculatedBallDistModifier;
                var clampedY = Mathf.Clamp(newCameraPosition.y + 0.1f, 2.5f, 4);
                newCameraPosition = new Vector3(newCameraPosition.x, clampedY, newCameraPosition.z) * _cameraDistanceModifier;

                m_Camera.transform.position = newCameraPosition;
            }
            else {
                if (Debugging)
                    Debug.LogWarning("UpdateCameraPosition3rdPerson: Current Camera Local Position: " + m_Camera.transform.localPosition);
                if (Debugging)
                    Debug.LogWarning("UpdateCameraPosition3rdPerson: _originalCameraPositionLocal: " + _originalCameraPositionLocal);
                //newCameraPosition = m_Camera.transform.TransformPoint(Vector3.Lerp(m_Camera.transform.localPosition, _originalCameraPositionLocal, 1f * Time.deltaTime));
                newCameraPosition = Vector3.Lerp(m_Camera.transform.localPosition, _originalCameraPositionLocal, 1f * Time.deltaTime);
                //m_Camera.transform.position = newCameraPosition;
                m_Camera.transform.localPosition = newCameraPosition;
            }


            //if (trackingBall) {
            //    var directionNormalized = (Ball.BallPosition - transform.position).normalized;
            //    newCameraPosition = transform.position + directionNormalized * -3;
            //}
            //else {
            //    newCameraPosition = Vector3.Lerp(m_Camera.transform.position, m_OriginalCameraPosition, 1f * Time.deltaTime);
            //}
            //m_Camera.transform.position = newCameraPosition;
        }


        // 
        private void HoldCameraPosition() {
            // For the first cycle only
            if (_previousTurnCameraPositionLocal == Vector3.zero)
                _previousTurnCameraPositionLocal = m_Camera.transform.position;

            //Vector3 newCameraPosition = m_Camera.transform.position;

            m_Camera.transform.position = _previousTurnCameraPositionLocal;

            _previousTurnCameraPositionLocal = m_Camera.transform.position;
        }

        // Camera position - HeadBob
        private void UpdateCameraPosition(float speed) {
            if (!m_UseHeadBob) return;

            Vector3 newCameraPosition = m_Camera.transform.position;

            // HeadBob stuff
            //if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded) {
            //    m_Camera.transform.position =
            //        m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude + (speed*(m_IsWalking ? 1f : m_RunstepLenghten)));
            //    newCameraPosition = m_Camera.transform.position;

            //    newCameraPosition.y = m_Camera.transform.position.y - m_JumpBob.Offset();
            //}
            //else {
            //    //newCameraPosition = m_Camera.transform.position;
            //    //newCameraPosition = m_HeadBob.ActiveModelEyes.position;
            //    newCameraPosition = Vector3.Lerp(m_Camera.transform.position, m_HeadBob.ActiveModelEyes.position, 1f * Time.deltaTime);
            //    //newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            //}
            m_Camera.transform.position = newCameraPosition;
        }

        public void SwitchToFirstOrThirdPersonView() {
            if (_CameraView == CameraView.FirstPersonView)
                _CameraView = CameraView.ThirdPersonView;
            else
                _CameraView = CameraView.FirstPersonView;
            _UIBall.CheckUIBallImage();

            //
            if (_CameraView == CameraView.FirstPersonView) {
                m_Camera.transform.position = m_HeadBob.ActiveModelEyes.position;
            }
            if (_CameraView == CameraView.ThirdPersonView) {
                m_MouseLook.ReturnOriginalPosRot();
            }

            m_Camera.fieldOfView = 60;
            MasterManager.GameSettings.EventChangedTracking?.Invoke(MasterManager.GameSettings._trackingMode);
        }

        public void SharpLookAtMyself() {
            m_MouseLook.SharpLookAtTargetTransform(transform, m_Camera.transform);
        }
        private void GetModifiedInput(Vector2 m_Input, out float speed) {

            bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
            // set the desired speed to be walking or running
            speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1) {
                m_Input.Normalize();
            }

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            //if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0) {
            //    StopAllCoroutines();
            //    StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            //}
        }

        [PunRPC]
        public void RPC_TeleportPlayer() // object[] info
        { 
            //var pos = (Vector3) info[0];
            //var rot = (Vector3) info[1];
            //transform.position = pos;
            //transform.rotation = Quaternion.Euler(rot);
            transform.position = PhotonRoomMatch.I.AdminFootballPlayer.transform.position;
            transform.rotation = PhotonRoomMatch.I.AdminFootballPlayer.transform.rotation;
        }


        private void OnControllerColliderHit(ControllerColliderHit hit) {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below) {
                return;
            }

            if (body == null || body.isKinematic) {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }


        internal void TurnOffAnimators(bool state) {
            VisualGoalkeeperAnimator.enabled = state;
            VisualMainAnimator.enabled = state;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isMainModel">True if ordinary player</param>
        private void ChangeVisualMainOrGoalkeeper(bool isMainModel) {
            ChangeVisualRepresentationIsActive(true);
            VisualMain.SetActive(isMainModel);
            VisualGoalkeeper.SetActive(!isMainModel);
        }

        public void ChangeVisualRepresentationIsActive(bool state) {
            //Debug.LogWarning($"SwitchVisualRepresentationIsActive: {state}", this);
            Visuals.SetActive(state);
        }

        private void ChangeVisualCompleteOffCrutch() {
            VisualMain.SetActive(false);
            VisualGoalkeeper.SetActive(!false);
        }

        public void ReturnToPlayableStateAfterReplay() {
            VisualMain.transform.localPosition = Vector3.zero;
            VisualMain.transform.localRotation = Quaternion.identity;
            VisualGoalkeeper.transform.localPosition = Vector3.zero;
            VisualGoalkeeper.transform.localRotation = Quaternion.identity;

            _CameraView = CameraView.ThirdPersonView;
            m_MouseLook.ReturnOriginalPosRot();

            StartCoroutine(ReturnToPlayableStateAfterReplayCor());
        }

        IEnumerator ReturnToPlayableStateAfterReplayCor() {
            yield return null;
            VisualMain.transform.localPosition = Vector3.zero;
            VisualMain.transform.localRotation = Quaternion.identity;
            VisualGoalkeeper.transform.localPosition = Vector3.zero;
            VisualGoalkeeper.transform.localRotation = Quaternion.identity;

            _CameraView = CameraView.ThirdPersonView;
            m_MouseLook.ReturnOriginalPosRot();
        }
    }
}