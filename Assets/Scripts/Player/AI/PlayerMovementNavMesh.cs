﻿using UnityEngine;
using System.Collections;
using Photon.Pun;
using Soccer;


[DisallowMultipleComponent]
[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
public class PlayerMovementNavMesh : MonoBehaviour {
	private Vector3 targetPosition;     // Where we want to travel too

    //
    [SerializeField] private FootballPlayer _FootballPlayer;
    [SerializeField] private UnityEngine.AI.NavMeshAgent agent;

    private Vector3 prevforward;
    private float previousRealtimeSinceStartup;


    void Awake()
	{
        if (agent == null)
		    agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (_FootballPlayer == null)
            _FootballPlayer = GetComponent<FootballPlayer>();
	}

	void Start ()
	{
		targetPosition = transform.position;		//set the target postion to where we are at the start
	}

    void Update() {
        // Check for Local Player AND for Replay
        if (!_FootballPlayer._PhotonView.IsMine || MasterManager.GameSettings._GameMode != GameSettings.GameMode.Play) return;
        // Check for enabled agent
        if (!agent.enabled) return;

        //Debug.LogWarning("PlayerMovementNavMesh. agent.remainingDistance: " + agent.remainingDistance);
        //Debug.LogWarning("PlayerMovementNavMesh. agent.destination: " + agent.destination);

        //if the player clicked on the screen, find out where
        //if(Input.GetMouseButton(MIDDLE_MOUSE_BUTTON))
        //	SetTargetPosition();

        //if (targetPosition != null)
        //    MovePlayer();

        if (Time.realtimeSinceStartup - previousRealtimeSinceStartup > 0.1f)
        {
            // Update Ball position
            agent.SetDestination(Ball.BallPosition);

            // Stop
            if (((transform.position - Ball.BallPosition).magnitude < 1f) ||
                ((transform.position - Ball.BallPosition).magnitude < 1.5f && agent.remainingDistance <= agent.stoppingDistance))
            {
                //Debug.LogWarning("PlayerMovementNavMesh. Reached!");
                agent.enabled = false;
            }
        }


        //
        _FootballPlayer.VisualMainAnimator.SetFloat("Speed", agent.velocity.magnitude / agent.speed);
        if (_FootballPlayer.VisualGoalkeeperAnimator.enabled)
            _FootballPlayer.VisualGoalkeeperAnimator.SetFloat("Speed", agent.velocity.magnitude / agent.speed);

        float deltaangle = Vector3.Angle(prevforward, transform.right) - 90;

        _FootballPlayer.VisualMainAnimator.SetFloat("Direction", (deltaangle / Time.deltaTime) / agent.angularSpeed);
        if (_FootballPlayer.VisualGoalkeeperAnimator.enabled)
            _FootballPlayer.VisualGoalkeeperAnimator.SetFloat("Direction", (deltaangle / Time.deltaTime) / agent.angularSpeed);

        prevforward = transform.forward;
    }


    public void TrySwitchPathfindBall() {
        if (Time.realtimeSinceStartup - previousRealtimeSinceStartup <= 0.2f) return;

            previousRealtimeSinceStartup = Time.realtimeSinceStartup;

            agent.enabled = !agent.enabled;

            if (agent.enabled) {
                targetPosition = Ball.BallPosition;
                agent.SetDestination(targetPosition);
            }
        
    }

    public void SetSpeedWalkOrRun(bool running) {
        if (running)
            agent.speed = 15f;
        else
            agent.speed = 7.5f;
    }

    /// <summary>
    /// Sets the target position we will travel too.
    /// </summary>
    void SetTargetPositionMousePointer()
    {
	    Plane plane = new Plane(Vector3.up, transform.position);
	    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    float point = 0f;
		
	    if(plane.Raycast (ray, out point))
		    targetPosition = ray.GetPoint(point);
    }


	/// <summary>
	/// Moves the player in the right direction and also rotates them to look at the target position.
	/// When the player gets to the target position, stop them from moving.
	/// </summary>
	void MovePlayer()
	{
		agent.SetDestination(targetPosition);

		Debug.DrawLine(transform.position, targetPosition, Color.red);
	}
}