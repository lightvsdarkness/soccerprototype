﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


namespace Soccer {
    public class PlayerInput : MonoBehaviour {
        public bool Debugging;

        public string KickLegButtonName = "Fire1";
        public int KickHeadMouseButton = 1;
        public bool AdminAssumedControlInputModeMoving = true;

        [Header("Debug info")]
        public FootballPlayer _FootballPlayer;
        public AbilityCaster _AbilityCaster;
        private CameraSpectator _cameraSpectator;

        private CameraZoom _CameraZoom;

        [Space]
        public GameplayEscMenu _GameplayEscMenu;
        public PlayerListingsMenu _PlayerListingsMenu;

        [Header("Debug info")]
        [SerializeField] private float _lastTimeViewPointSwitched;
        [SerializeField] private float _lastTimeDirectControlSwitched;
        [SerializeField] private float _lastTimeNavigateToBallSwitched;

        public PhotonView _PhotonView;

        void Start() {
            if (!_PhotonView.IsMine) return;

            if (_PlayerListingsMenu == null)
                _PlayerListingsMenu = FindObjectOfType<PlayerListingsMenu>();
            if (_GameplayEscMenu == null)
                _GameplayEscMenu = FindObjectOfType<GameplayEscMenu>();

            if (_FootballPlayer == null)
                _FootballPlayer = GetComponent<FootballPlayer>();
            if (_cameraSpectator == null)
                _cameraSpectator = GetComponentInChildren<CameraSpectator>();
            if (_CameraZoom == null)
                _CameraZoom = GetComponentInChildren<CameraZoom>();
        }


        void Update() {
            if (!_PhotonView.IsMine) return;


            //RayCastObjectPosition();

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Debug.Log("PlayerInput. Pressed Esc");
                MasterManager.GameSettings.MenuOpened = !MasterManager.GameSettings.MenuOpened;
                _GameplayEscMenu.SwitchEscMenu();
            }
            //if (MasterManager.GameSettings._GameMode != GameSettings.GameMode.Play && MasterManager.GameSettings._GameMode != GameSettings.GameMode.Record) return;

            if (Input.GetKeyUp(KeyCode.Tab))
            {
                MasterManager.GameSettings.MenuOpened = !MasterManager.GameSettings.MenuOpened;
                _PlayerListingsMenu.SwitchPlayerListingMenu();
            }

            // Tracking
            GetInputTracking();
            

            // Stuff below shouldn't work when Menus are opened
            if (MasterManager.GameSettings.MenuOpened) return;

            // Kicks tuning
            GetInputKicksTuning();

            // Zoom
            GetInputZoom();

            if (!MasterManager.GameSettings.IsGamePlayed) return;


            // Teleporting Ball
            if (_FootballPlayer._FootballNetPlayer.RoleByte == 0) //PhotonNetwork.LocalPlayer.IsMasterClient
            {
                if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
                {
                    var ballNewPosition = RayCastObjectPosition();
                    //Debug.LogWarning("ballNewPosition" + ballNewPosition);
                    Ball.BallTransform.position = ballNewPosition;
                    Ball.BallTransform.GetComponent<Rigidbody>().velocity = Vector3.zero;
                }
            }

            // Switch between view modes
            if (Input.GetKeyUp(KeyCode.G)) {
                if ((Time.realtimeSinceStartup - _lastTimeViewPointSwitched) > 0.25f) {
                    _lastTimeViewPointSwitched = Time.realtimeSinceStartup;

                    _FootballPlayer.SwitchToFirstOrThirdPersonView();
                }

            }

            // Admin Assuming Direct Control
            if (Input.GetKeyUp(KeyCode.P) && _FootballPlayer._FootballNetPlayer.RoleByte == 0) {
                if ((Time.realtimeSinceStartup - _lastTimeDirectControlSwitched) > 0.25f) {
                    _lastTimeDirectControlSwitched = Time.realtimeSinceStartup;

                    _FootballPlayer._AdminController.TrySwitchAssumeDirectControl();
                }
            }
            // Admin switching Mode in Assumed Direct Control state
            if (Input.GetKeyUp(KeyCode.CapsLock) && _FootballPlayer.State == FootballPlayerState.AdminAssumedControl) {
                AdminAssumedControlInputModeMoving = !AdminAssumedControlInputModeMoving;
                //if (AdminAssumedControlInputModeMoving) {
                //    //Debug.LogWarning($"AdminAssumedControlInputModeMoving {AdminAssumedControlInputModeMoving}  _cameraSpectator.enabled {_cameraSpectator.enabled}");
                //}

                _cameraSpectator.SwitchCameraSpectatorState(!AdminAssumedControlInputModeMoving);
                _FootballPlayer.enabled = AdminAssumedControlInputModeMoving;
            }
            

            // Switch trying to capture the ball
                if (Input.GetKey(KeyCode.LeftControl)) {
                _FootballPlayer._FootballNetPlayer.ShouldCaptureBall = false;
            }
            else {
                _FootballPlayer._FootballNetPlayer.ShouldCaptureBall = true;
            }


            // --- Running to the ball
            if (Input.GetKeyUp(KeyCode.Space))
            {
                if ((Time.realtimeSinceStartup - _lastTimeNavigateToBallSwitched) > 0.25f) {
                    _lastTimeNavigateToBallSwitched = Time.realtimeSinceStartup;

                    //FootballPlayer._NavMeshAgent
                    _FootballPlayer._PlayerMovementNavMesh.TrySwitchPathfindBall();
                }
            }
            bool running = Input.GetKey(KeyCode.LeftShift);
            _FootballPlayer._PlayerMovementNavMesh.SetSpeedWalkOrRun(running);


            // Kicks casting
            // Leg kick
            if (!MasterManager.GameSettings.MenuOpened && Input.GetButtonDown(KickLegButtonName)) {
                if (Debugging)
                    Debug.LogWarning("Kicking ball, MenuOpened: " + MasterManager.GameSettings.MenuOpened);
                _AbilityCaster.CastAbilityKickLeg();
            }

            // Head kick
            if (Input.GetMouseButton(KickHeadMouseButton))
                _AbilityCaster.CastAbilityKickHead();
        }

        public void GetInputKicksTuning() {
            // Trajectory
            if (Input.GetKeyDown(KeyCode.V)) {
                _AbilityCaster.BallLegKickAbility.TuneAbilityTrajectory(0);
            }
            if (Input.GetKeyDown(KeyCode.F)) {
                _AbilityCaster.BallLegKickAbility.TuneAbilityTrajectory(1);
            }
            if (Input.GetKeyDown(KeyCode.R)) {
                _AbilityCaster.BallLegKickAbility.TuneAbilityTrajectory(2);
            }

            // Strength
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                _AbilityCaster.BallLegKickAbility.TuneAbilityStrength(0);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                _AbilityCaster.BallLegKickAbility.TuneAbilityStrength(1);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                _AbilityCaster.BallLegKickAbility.TuneAbilityStrength(2);
            }
        }

        public void GetInputTracking() {
            if (Input.GetKeyUp(KeyCode.Z))
            {
                if (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Place && ManagerGame.I.TrackedTransform?.GetComponent<TrackingBeacon>() != null)
                    Destroy(ManagerGame.I.TrackedTransform.gameObject);

                if (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball) {
                    MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.None;
                }
                else {
                    MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.Ball;
                    ManagerGame.I.TrackedTransform = Ball.BallTransform;
                }
            }
            if (Input.GetKeyUp(KeyCode.X))
            {
                if (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Place && ManagerGame.I.TrackedTransform?.GetComponent<TrackingBeacon>() != null)
                    Destroy(ManagerGame.I.TrackedTransform.gameObject);

                if (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Player) {
                    MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.None;
                }
                else
                {
                    var playerTransform = RayCastObjectTransform();
                    if (playerTransform != null && playerTransform.GetComponent<FootballPlayer>() != null) {
                        MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.Player;
                        ManagerGame.I.TrackedTransform = playerTransform;
                    }
                }
            }
            if (Input.GetKeyUp(KeyCode.C))
            {
                if (MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Place)
                {
                    MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.None;

                    if (ManagerGame.I.TrackedTransform?.GetComponent<TrackingBeacon>() != null)
                        Destroy(ManagerGame.I.TrackedTransform.gameObject);
                }
                else
                {
                    MasterManager.GameSettings._TrackingMode = GameSettings.TrackingMode.Place;
                    var placeTransform = RayCastObjectPosition();
                    if (placeTransform != Vector3.zero)
                    {
                        var objectTracked = Instantiate(MasterManager.GameSettings.TrackingBeaconPrefab, placeTransform,
                            Quaternion.identity);
                        ManagerGame.I.TrackedTransform = objectTracked.transform;
                    }
                }
            }
        }

        public void GetInputZoom() {
            float scrollWheelInput = Input.GetAxis("Mouse ScrollWheel");
            if (scrollWheelInput != 0)
            {
                _CameraZoom.Zoom(scrollWheelInput);

                if (MasterManager.GameSettings._GameMode == GameSettings.GameMode.Replay)
                {
                    _cameraSpectator.ReplayInput();
                }

            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                _CameraZoom.Zoom(1);
            }
            if (Input.GetKeyDown(KeyCode.Y))
            {
                _CameraZoom.Zoom(-1);
            }

        }

        public Vector2 GetFootbalPlayerMovementInput() {
            float horizontal = 0;
            float vertical = 0;
            if ((_FootballPlayer.State != FootballPlayerState.AdminAssumedControl) || (_FootballPlayer.State == FootballPlayerState.AdminAssumedControl && AdminAssumedControlInputModeMoving) ||
                (MasterManager.GameSettings._GameMode != GameSettings.GameMode.Pause && MasterManager.GameSettings._GameMode == GameSettings.GameMode.Replay)
                )
            {
                horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
                vertical = CrossPlatformInputManager.GetAxis("Vertical");
            }
            return new Vector2(horizontal, vertical);
        }
        public bool GetFootbalPlayerJumpInput() {
           return CrossPlatformInputManager.GetButtonDown("Jump");
        }

        public Vector3 RayCastObjectPosition() {
            Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f); // center of the screen
            float rayLength = 500f;

            // actual Ray
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //Ray ray = Camera.main.ViewportPointToRay(rayOrigin);

            // debug Ray
            Debug.DrawRay(ray.origin, ray.direction * rayLength, Color.blue);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, rayLength))
            {
                //Debug.Log("PlayerInput. RayCastObjectPosition. Hit point: " + hit.point);
                return hit.point;
            }
            return Vector3.zero;
        }

        public Transform RayCastObjectTransform() {
            Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f); // center of the screen
            float rayLength = 5000f;

            // actual Ray
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //Ray ray = Camera.main.ViewportPointToRay(rayOrigin);

            // debug Ray
            Debug.DrawRay(ray.origin, ray.direction * rayLength, Color.red);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, rayLength))
            {
                return hit.transform;
            }
            return null;
        }
    }
}