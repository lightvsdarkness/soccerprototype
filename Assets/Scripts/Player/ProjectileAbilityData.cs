﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Abilities/ProjectileAbility")]
public class ProjectileAbilityData : AbilityData {

    public float projectileForce = 500f;
    //public Rigidbody projectile;

    //private ProjectileShootTriggerable launcher;


    //public override void Initialize(GameObject obj) {
    //    launcher = obj.GetComponent<ProjectileShootAbility>();
    //    launcher.projectileForce = projectileForce;
    //    launcher.projectile = projectile;
    //}

    //public override void TriggerAbility() {
    //    launcher.Launch();
    //}

}