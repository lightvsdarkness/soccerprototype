﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace Soccer {
    public class FootballPlayerVisuals : MonoBehaviour {
        public bool _shouldEnableRenderers;

        public bool ShouldEnableRenderers {
            get { return _shouldEnableRenderers; }
            set {
                _shouldEnableRenderers = value;
                Debug.LogWarning($"_shouldEnableRenderers: {_shouldEnableRenderers}");
            }
        }
        public float smoothTime = 5f;
        public List<Renderer> Renderers = new List<Renderer>();
        public GameObject IndicatorAdminControlled;

        public Transform HeadGoalkeeperT;
        public Transform HeadOrdinaryT;

        public Transform EyesGoalkeeperT;
        public Transform EyesOrdinaryT;

        public bool ShouldCheckRotation;

        [Header("Links")]
        [SerializeField] private PhotonView _photonView;
        [SerializeField] private Transform cameraTransform;
        [SerializeField] private FootballPlayer _footballPlayer;

        public Action<byte> AdminAssumedControl;

        private void Start() {
            //if(_photonView.IsMine)
            MasterManager.GameSettings.EventChangedTracking += ListenerPlayerChanged;

            IndicatorAdminControlled?.SetActive(false);
        }

        private void OnDestroy() {
            //if (_photonView.IsMine)
            MasterManager.GameSettings.EventChangedTracking -= ListenerPlayerChanged;
        }

        private void FixedUpdate() {
            // TRACKING BALL IS BROKEN???
            if (_photonView.IsMine)
                foreach (var renderer in Renderers) {
                    renderer.enabled = ShouldEnableRenderers;
                }
        }

        public void SwitchAdminControlled(bool controlled, byte stolenTeamNumber) {
            //if (controlled) {
            //    if (_footballPlayer._FootballNetPlayer.Team == 0) {
            //        _clothColorChanger.SetMaterialsAdminTeam1();
            //    }
            //    else if (_footballPlayer._FootballNetPlayer.Team == 1) {
            //        _clothColorChanger.SetMaterialsAdminTeam2();
            //    }
            //}
            //else {
            //    if (_footballPlayer._FootballNetPlayer.Team == 0) {
            //        _clothColorChanger.SetMaterialsTeam1();
            //    }
            //    else if (_footballPlayer._FootballNetPlayer.Team == 1) {
            //        _clothColorChanger.SetMaterialsTeam2();
            //    }
            //}
            //Debug.LogWarning("SwitchAdminControlled", this);
            AdminAssumedControl?.Invoke(stolenTeamNumber);
        }

        public void ListenerPlayerChanged(GameSettings.TrackingMode _trackingMode) {
            //Debug.Log("FootballPlayerVisuals. ListenerPlayerChanged. cameraTransform.localRotation.y: " + cameraTransform.localRotation.y);
            //HeadGoalkeeperT.rotation = Quaternion.Slerp(HeadGoalkeeperT.rotation, cameraTransform.rotation, smoothTime*Time.deltaTime);
            //HeadOrdinaryT.rotation = Quaternion.Slerp(HeadGoalkeeperT.rotation, cameraTransform.rotation, smoothTime*Time.deltaTime);

            HeadGoalkeeperT.rotation = Quaternion.Slerp(HeadGoalkeeperT.rotation, cameraTransform.rotation,
                smoothTime*Time.deltaTime);

            //Debug.Log("FootballPlayerVisuals. ListenerPlayerChanged. HeadOrdinaryT.rotation before: " + HeadOrdinaryT.rotation);
            //Debug.Log("FootballPlayerVisuals. ListenerPlayerChanged. cameraTransform.rotation: " + cameraTransform.rotation);
            HeadOrdinaryT.rotation = Quaternion.Slerp(HeadGoalkeeperT.rotation, cameraTransform.rotation,
                smoothTime*Time.deltaTime);
            //Debug.Log("FootballPlayerVisuals. ListenerPlayerChanged. HeadOrdinaryT.rotation after: " + HeadOrdinaryT.rotation);

            // In third persone mode we always see the model
            if (_footballPlayer._CameraView == CameraView.ThirdPersonView) {
                ShouldEnableRenderers = true;
                return;
            }
                
            if (cameraTransform.localRotation.y > Mathf.Abs(0.45f) || cameraTransform.localRotation.y < -0.45f) {
                Debug.LogWarning($"FootballPlayerVisuals. cameraTransform.localRotation.y: {cameraTransform.localRotation.y}");
                ShouldCheckRotation = false;
                ShouldEnableRenderers = false;

                //if (_trackingMode != GameSettings.TrackingMode.None) {
                //    ShouldEnableRenderers = false;
                //}
                //else {
                //    StartCoroutine(EnableWithDelayCor());
                //}
            }
            else {
                if (_footballPlayer._CameraView == CameraView.FirstPersonView) {
                    ShouldCheckRotation = true;
                    StartCoroutine(EnableWithDelayCor());
                }
 
            }

            // If we look forward we always see the model
            if (_trackingMode == GameSettings.TrackingMode.None)
                ShouldEnableRenderers = true;

            //// If we 
            //if (_footballPlayer._CameraView == CameraView.FirstPersonView && ) {
            //    ShouldEnableRenderers = true;
            //}
        }


        public IEnumerator EnableWithDelayCor() {
            yield return new WaitForSeconds(.25f);
            if (ShouldCheckRotation) {
                ShouldEnableRenderers = true;
                ShouldCheckRotation = false;
            }
             
        }
    }
}