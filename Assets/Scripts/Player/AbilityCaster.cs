﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;


namespace Soccer {
    [RequireComponent(typeof (AudioSource))]
    public class AbilityCaster : MonoBehaviour {
        public bool CanCast;

        [Header("Abilities")]
        public AbilityData AbilityData;
        public BallLegKickAbility BallLegKickAbility;
        public BallHeadKickAbility BallHeadKickAbility;

        public List<float> KicksStrengths = new List<float> { 125f, 250f, 380f };


        private float coolDownDuration = 0.5f;
        private float nextReadyTime; // Number in seconds which controls how often the player can use ability
        private float coolDownTimeLeft;

        [Header("Links")]
        //public TextMeshProUGUI coolDownTextDisplay;
        //public Image darkMask;
        //private Image myButtonImage;
        public UIKickLegController _UIKickLegController;
        public AudioSource _abilityAudioSource;

        public FootballPlayer _FootballPlayer;
        public Transform _legKickHolder;
        public Transform _soccerPlayerLegHolder;



        public void Awake() {
            if (BallLegKickAbility == null)
                BallLegKickAbility = GetComponent<BallLegKickAbility>();
            if (BallHeadKickAbility == null)
                BallHeadKickAbility = GetComponent<BallHeadKickAbility>();

            //if (myButtonImage == null)
            //    myButtonImage = GetComponent<Image>();
            if (_UIKickLegController == null)
                _UIKickLegController = FindObjectOfType<UIKickLegController>();
            if (_abilityAudioSource == null)
                _abilityAudioSource = GetComponent<AudioSource>();
            if (_FootballPlayer == null)
                _FootballPlayer = GetComponent<FootballPlayer>();
        }

        public void Start() {
            CanCast = true;
            
            Initialize(AbilityData);
        }

        public void Initialize(AbilityData selectedAbilityData) {
            AbilityData = selectedAbilityData;

            // myButtonImage.sprite = AbilityData.aSprite;
            //darkMask.sprite = AbilityData.aSprite;

            coolDownDuration = AbilityData.BaseCoolDown;
            BallLegKickAbility.Initialize(selectedAbilityData, _legKickHolder, _soccerPlayerLegHolder);
            BallHeadKickAbility.Initialize(selectedAbilityData);

            SwitchAbilityReady(true);
        }


        void Update() {
            bool coolDownComplete = (Time.time > nextReadyTime);
            if (coolDownComplete) {
                SwitchAbilityReady(true);
            }
            else {
                CoolDown();
            }
        }

        private void SwitchAbilityReady(bool state) {
            CanCast = state;
            //coolDownTextDisplay.enabled = !state;
            //darkMask.enabled = !state;
        }

        private void CoolDown() {
            coolDownTimeLeft -= Time.deltaTime;
            float roundedCd = Mathf.Round(coolDownTimeLeft);
            //coolDownTextDisplay.text = roundedCd.ToString();
            //darkMask.fillAmount = (coolDownTimeLeft / coolDownDuration);
        }

        public void CastAbilityKickLeg() {
            if (!CanCast) return;
            if (_FootballPlayer?._Ball == null) return;
            //Debug.Log("CastAbility");

            nextReadyTime = coolDownDuration + Time.time;
            coolDownTimeLeft = coolDownDuration;
            SwitchAbilityReady(false);

            _abilityAudioSource.clip = AbilityData.aSound;
            if (_abilityAudioSource.clip != null)
                _abilityAudioSource.Play();

            BallLegKickAbility.UseAbility(_FootballPlayer._Ball);
        }

        public void CastAbilityKickHead() {
            if (!CanCast) return;
            //if (_FootballPlayer?._Ball == null) return;
            //Debug.Log("CastAbility");

            nextReadyTime = coolDownDuration + Time.time;
            coolDownTimeLeft = coolDownDuration;
            SwitchAbilityReady(false);

            _abilityAudioSource.clip = AbilityData.aSound;
            if (_abilityAudioSource.clip != null)
                _abilityAudioSource.Play();

            BallHeadKickAbility.TryUseAbility();
        }
    }
}