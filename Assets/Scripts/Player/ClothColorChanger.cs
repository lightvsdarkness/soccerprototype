﻿using System;
using System.Collections.Generic;
using Photon.Pun;
using Soccer;
using UnityEngine;

public class ClothColorChanger : MonoBehaviour {
    public bool IsGoalkeeperModel;

    public List<Texture> NumberTextures = new List<Texture>();

    [Space]
    public List<Texture> TexturesTeam1 = new List<Texture>();
    public List<Texture> TexturesTeam2 = new List<Texture>();
    //public Color CurrentColor;
    [Space]
    public List<Texture> TexturesAdminTeam1 = new List<Texture>();
    public List<Texture> TexturesAdminTeam2 = new List<Texture>();
    

    public Material[] _Materials;
    public FootballPlayer _FootballPlayer;
    public FootballNetPlayer _FootballNetPlayer;

    [SerializeField] private bool _firstInit = true;

    public PhotonView _PhotonView;
    //public List<Renderer> Renderers = new List<Renderer>();
    public Renderer _Renderer = null;

    private void OnEnable() {
        if (!_firstInit) {
            if (_FootballNetPlayer == null)
                _FootballNetPlayer = GetComponentInParent<FootballPlayer>()._FootballNetPlayer;

            _FootballNetPlayer.TeamChanged += TeamChangedListener;
            _FootballNetPlayer.NumberChanged += NumberChangedListener;

            _FootballPlayer._FootballPlayerVisuals.AdminAssumedControl += SwitchAdminMaterials;
        }
    }
    private void Awake() {
        if (_FootballPlayer == null)
            _FootballPlayer = GetComponentInParent<FootballPlayer>();
        if (_FootballNetPlayer == null)
            _FootballNetPlayer = _FootballPlayer.GetComponent<FootballNetPlayer>();
    }
    private void Start() {
        _firstInit = false;

        if (_PhotonView == null)
            _PhotonView = GetComponent<PhotonView>();
        if (_Renderer == null)
            _Renderer = GetComponent<Renderer>();

        //Color random = Random.ColorHSV();
        //photonView.RPC("ChangeColour", RpcTarget.All, new Vector3(CurrentColor.r, CurrentColor.g, CurrentColor.b));

        _Materials = _Renderer.materials;

        if (_FootballNetPlayer == null)
            _FootballNetPlayer = GetComponentInParent<FootballPlayer>()._FootballNetPlayer;
        _FootballNetPlayer.TeamChanged += TeamChangedListener;
        _FootballNetPlayer.NumberChanged += NumberChangedListener;

        Invoke("Initialize", 0.1f); // Because order can change
    }
    public void OnDisable() {

        if (_FootballNetPlayer?.TeamChanged != null)
            _FootballNetPlayer.TeamChanged -= TeamChangedListener;
        if (_FootballNetPlayer?.NumberChanged != null)
            _FootballNetPlayer.NumberChanged -= NumberChangedListener;

        if (_FootballPlayer?._FootballPlayerVisuals.AdminAssumedControl != null)
            _FootballPlayer._FootballPlayerVisuals.AdminAssumedControl -= SwitchAdminMaterials;
    }

    private void Initialize() {
        var localNetPlayer = PhotonRoomMatch.I.FootballNetPlayers.Find(x => x.Nickname == PhotonNetwork.LocalPlayer.NickName);
        TeamChangedListener(localNetPlayer.Team);
        NumberChangedListener(localNetPlayer.Number);
    }

    //[ContextMenu("SetNumber 23")]
    //public void SetNumber23() { //int num
    //    var num = 23;
    //    _PhotonView.RPC("RPC_SetNumber", RpcTarget.AllBufferedViaServer, num);
    //}
    public void NumberChangedListener(byte number) { //int num
        _PhotonView.RPC("RPC_SetNumber", RpcTarget.AllBufferedViaServer, number);
    }
    [PunRPC]
    public void RPC_SetNumber(byte number) {
        try {
            if (number > 9)
            {
                var digits = new List<int>();
                double n = number / 10d;
                while (n >= 0.1)
                {
                    digits.Add((int)Math.Round((n - Math.Truncate(n)) * 10d));
                    n = Math.Truncate(n) / 10d;
                }

                for (int i = digits.Count - 1; i >= 0; i--)
                {
                    int digit = digits[i];
                    Debug.Log("SetNumber: " + digit, this);
                }

                _Materials[2].SetTexture("_DecalTex", NumberTextures[digits[1]]);
                _Materials[4].SetTexture("_DecalTex", NumberTextures[digits[0]]);
            }
            else
            {
                _Materials[2].SetTexture("_DecalTex", NumberTextures[0]);
                _Materials[4].SetTexture("_DecalTex", NumberTextures[number]);
            }
        }
        catch (Exception e)
        {
            Debug.LogWarning($"RPC_SetMaterialsTeam1: '{e}'");
        }
    }

    public void TeamChangedListener(byte teamNumber) {
        if (_FootballPlayer.State == FootballPlayerState.AdminAssumedControl)
        {
            SwitchAdminMaterials(teamNumber);
        }
        else
        {
            if (teamNumber == 0)
                SetMaterialsTeam1();
            else
                SetMaterialsTeam2();
        }
    }

    //[ContextMenu("SetMaterialsTeam1")]
    public void SetMaterialsTeam1() {
        _PhotonView.RPC("RPC_SetMaterialsTeam1", RpcTarget.All);
    }

    [PunRPC]
    public void RPC_SetMaterialsTeam1() {
        try {
            if (IsGoalkeeperModel && _FootballNetPlayer.IsGoalkeeper) {
            _Materials[8].mainTexture = TexturesTeam1[5];
            _Materials[5].mainTexture = TexturesTeam1[3];
            }
            else {
                _Materials[8].mainTexture = TexturesTeam1[6];
                _Materials[5].mainTexture = TexturesTeam1[7];
            }

            _Materials[1].mainTexture = TexturesTeam1[0];
            _Materials[2].mainTexture = TexturesTeam1[0];
            _Materials[4].mainTexture = TexturesTeam1[0];

            _Materials[3].SetTexture("_DecalTex", TexturesTeam1[2]);
            _Materials[3].mainTexture = TexturesTeam1[0];

            _Materials[7].mainTexture = TexturesTeam1[4];
        }
        catch (Exception e) {
            Debug.LogWarning($"RPC_SetMaterialsTeam1: '{e}'");
        }
    }

    //[ContextMenu("SetMaterialsTeam2")]
    public void SetMaterialsTeam2() {
        _PhotonView.RPC("RPC_SetMaterialsTeam2", RpcTarget.All);
    }
    [PunRPC]
    public void RPC_SetMaterialsTeam2() {
        try {
            if (IsGoalkeeperModel && _FootballNetPlayer.IsGoalkeeper) {
                _Materials[8].mainTexture = TexturesTeam2[5];
                _Materials[5].mainTexture = TexturesTeam2[3];
            }
            else {
                _Materials[8].mainTexture = TexturesTeam2[6];
                _Materials[5].mainTexture = TexturesTeam2[7];
            }

            _Materials[1].mainTexture = TexturesTeam2[0];
            _Materials[2].mainTexture = TexturesTeam2[0];
            _Materials[4].mainTexture = TexturesTeam2[0];

            _Materials[3].SetTexture("_DecalTex", TexturesTeam2[2]);
            _Materials[3].mainTexture = TexturesTeam2[0];

            _Materials[7].mainTexture = TexturesTeam2[4];
        }
        catch (Exception e) {
            Debug.LogWarning($"RPC_SetMaterialsTeam2: '{e}'");
        }
    }


    ////
    //[ContextMenu("ChangeColourToDark")]
    //private void ChangeColourToDark() {
    //    var colorVect = new Vector3(0.1f, 0.1f, 0.1f);
    //    ChangeColour(colorVect);
    //}
    //private void ChangeColour(Vector3 colorVect) {
    //    _Renderer.material.SetColor("_Color", new Color(colorVect.x, colorVect.y, colorVect.z));
    //}

    private void SwitchAdminMaterials(byte teamNumber) {
        Debug.LogWarning("ClothColorChanger.SwitchAdminMaterials", this);
        if (teamNumber == 0) {
            SetMaterialsAdminTeam1();
        }
        else if (teamNumber == 1) {
            SetMaterialsAdminTeam2();
        }
    }

    [ContextMenu("SetMaterialsAdminTeam1")]
    public void SetMaterialsAdminTeam1() {
        _PhotonView.RPC("RPC_SetMaterialsAdminTeam1", RpcTarget.All);
        //RPC_SetMaterialsAdminTeam1();
    }

    [PunRPC]
    public void RPC_SetMaterialsAdminTeam1() {
        try
        {
            if (IsGoalkeeperModel && _FootballNetPlayer.IsGoalkeeper) {
                _Materials[8].mainTexture = TexturesAdminTeam1[5];
                _Materials[5].mainTexture = TexturesAdminTeam1[3];
            }
            else {
                _Materials[8].mainTexture = TexturesAdminTeam1[6];
                _Materials[5].mainTexture = TexturesAdminTeam1[7];
            }

            _Materials[1].mainTexture = TexturesAdminTeam1[0];
            _Materials[2].mainTexture = TexturesAdminTeam1[0];
            _Materials[4].mainTexture = TexturesAdminTeam1[0];

            _Materials[3].SetTexture("_DecalTex", TexturesAdminTeam1[2]);
            _Materials[3].mainTexture = TexturesAdminTeam1[0];

            _Materials[7].mainTexture = TexturesAdminTeam1[4];
        }
        catch (Exception e)
        {
            Debug.LogWarning($"RPC_SetMaterialsTeam1: '{e}'");
        }
    }

    [ContextMenu("SetMaterialsAdminTeam2")]
    public void SetMaterialsAdminTeam2() {
        _PhotonView.RPC("RPC_SetMaterialsAdminTeam2", RpcTarget.All);
        //RPC_SetMaterialsAdminTeam2();
    }

    [PunRPC]
    public void RPC_SetMaterialsAdminTeam2() {
        try
        {
            if (IsGoalkeeperModel && _FootballNetPlayer.IsGoalkeeper) {
                _Materials[8].mainTexture = TexturesAdminTeam2[5];
                _Materials[5].mainTexture = TexturesAdminTeam2[3];
            }
            else {
                _Materials[8].mainTexture = TexturesAdminTeam2[6];
                _Materials[5].mainTexture = TexturesAdminTeam2[7];
            }

            _Materials[1].mainTexture = TexturesAdminTeam2[0];
            _Materials[2].mainTexture = TexturesAdminTeam2[0];
            _Materials[4].mainTexture = TexturesAdminTeam2[0];

            _Materials[3].SetTexture("_DecalTex", TexturesAdminTeam2[2]);
            _Materials[3].mainTexture = TexturesAdminTeam2[0];

            _Materials[7].mainTexture = TexturesAdminTeam2[4];
        }
        catch (Exception e)
        {
            Debug.LogWarning($"RPC_SetMaterialsTeam1: '{e}'");
        }
    }
}