﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Soccer {
    public class CameraAdmin : MonoBehaviour {
        public CameraSpectator _CameraSpectator;
        public Transform CameraTransform;


        void Start() {
            if (_CameraSpectator == null)
                _CameraSpectator = GetComponent<CameraSpectator>();
            if (CameraTransform == null)
                CameraTransform = transform;
        }


        void Update() {
            if (false)
                CameraTransform.position = _CameraSpectator.LastPosition;
        }
    }
}