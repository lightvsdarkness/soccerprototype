﻿using System.Runtime.InteropServices;
using Photon.Pun;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Soccer {
    public class CameraSpectator : MonoBehaviour {
        public Vector3 InitialLocalPositon;

        [Space]
        public bool enableMouseFireCapture;
        //public bool enableInputCapture = true;
        //public bool holdRightMouseCapture = false;

        public float lookSpeed = 5f;
        public float moveSpeed = 5f;
        public float sprintSpeed = 50f;

        [SerializeField] private bool _capturingMovingInput;
        [SerializeField] private float m_yaw;
        [SerializeField] private float m_pitch;

        [Space]
        [SerializeField] private float currentTime;
        [SerializeField] private float deltaTime;

        public Vector3 LastPosition = Vector3.zero;

        public FootballPlayer _FootballPlayer;
        public Camera _camera;


        void Awake() {
            if (_FootballPlayer == null)
                _FootballPlayer = GetComponentInParent<FootballPlayer>();
            if (_camera == null)
                _camera = GetComponentInParent<Camera>();

            //enabled = enableInputCapture;
            InitialLocalPositon = transform.localPosition;
        }

        //void OnValidate() {
            //if (Application.isPlaying)
            //    enabled = enableInputCapture;
        //}


        void Update() {
            //Debug.LogWarning("_capturingMovingInput -: " + _capturingMovingInput, this);
            if (!_FootballPlayer._PhotonView.IsMine) {
                //Debug.LogWarning("_FootballPlayer._PhotonView.IsMine -: " + _FootballPlayer._PhotonView.IsMine, this);
                return;
            }
            //Debug.LogWarning("_capturingMovingInput --: " + _capturingMovingInput, this); 

            // if NOT capturing
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1)) {
                _capturingMovingInput = !_capturingMovingInput;
            }

            if (_capturingMovingInput)
                CaptureMovingInput();
            else
                ReleaseMovingInput();

            if (enableMouseFireCapture)
                CaptureMouseFire();

            if (!_capturingMovingInput) {
                return;
            }

            if (!MasterManager.GameSettings.IsGamePlayed) return;
            //Debug.Log("_capturingMovingInput: " + _capturingMovingInput, this);

            deltaTime = Time.realtimeSinceStartup - currentTime;
            currentTime = Time.realtimeSinceStartup;


            // ROTATION
            if (_FootballPlayer.State == FootballPlayerState.PlayerDeactivatedByAdmin) {
                Vector3 direction = new Vector3();
                if(PhotonRoomMatch.I.AdminFootballPlayer != null)
                    direction = PhotonRoomMatch.I.AdminFootballPlayer.transform.position - transform.position;
                Quaternion rotation = Quaternion.LookRotation(direction);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 5 * deltaTime);
            }
            // Tracking
            else if ((MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Ball ||
                 MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Player ||
                 MasterManager.GameSettings._TrackingMode == GameSettings.TrackingMode.Place) &&
                 ManagerGame.I.TrackedTransform != null)
            {
                var direction = ManagerGame.I.TrackedTransform.position - transform.position;
                Quaternion rotation = Quaternion.LookRotation(direction);
                //m_CameraTargetRot = rotation;
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 5 * deltaTime);
            }
            else
            {
                var rotStrafe = Input.GetAxis("Mouse X");
                var rotFwd = Input.GetAxis("Mouse Y");

                m_yaw = (m_yaw + lookSpeed * rotStrafe) % 360f;
                m_pitch = (m_pitch - lookSpeed * rotFwd) % 360f;
                transform.rotation = Quaternion.AngleAxis(m_yaw, Vector3.up) * Quaternion.AngleAxis(m_pitch, Vector3.right);
            }
            // ROTATION ENDS

            // POSITION
            //Debug.Log("CameraSpectator Position Starts", this);
            float speed;
            if (MasterManager.GameSettings.IsGamePlayed)
                speed = (Input.GetKey(KeyCode.LeftShift) ? sprintSpeed : moveSpeed) * deltaTime;
            else {
                speed = (Input.GetKey(KeyCode.LeftShift) ? sprintSpeed : moveSpeed) * deltaTime * 80;
            }
            var forward = speed*Input.GetAxis("Vertical");
            var right = speed*Input.GetAxis("Horizontal");
            //Debug.Log("forward: " + forward, this);
            //Debug.Log("right: " + right, this);

            var up = speed*((Input.GetKey(KeyCode.E) ? 1f : 0f) - (Input.GetKey(KeyCode.Q) ? 1f : 0f)) * moveSpeed * 16 * deltaTime;
            //
            var movingDelta = transform.forward * forward + transform.right * right + Vector3.up * up;
            //Debug.LogWarning("movingDelta: " + movingDelta);
            transform.position += movingDelta;
            //Debug.Log("CameraSpectator movingDelta: " + movingDelta, this);
            LastPosition = transform.position;
        }

        private void CaptureMouseFire() {
            if (Input.GetMouseButtonDown(0)) //&& !EventSystem.current.IsPointerOverGameObject()
            {
                RaycastHit hit;
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                Debug.Log("SpectatorCamera. CaptureMouseFire");
                if (Physics.Raycast(ray, out hit, 10f, MasterManager.GameSettings.MaskExceptBorders)) {
                    Debug.Log("SpectatorCamera. hit: " + hit.transform.name, this);
                    if (hit.transform.name == "ModelStartGame") {
                        Debug.Log("SpectatorCamera. hit.transform.name == ModelStartGame");
                        hit.transform.gameObject.SetActive(false);
                        _FootballPlayer._PhotonView.RPC("RPC_MakeChoiceAndStartGame", RpcTarget.All);
                    }
                }
            }
        }

        public void ReplayInput() {
            CaptureMovingInput();
        }

        // Turn off cursor and make travel
        void CaptureMovingInput() {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            m_yaw = transform.eulerAngles.y;
            m_pitch = transform.eulerAngles.x;

            //enableMouseFireCapture = false;
        }

        // Turn on cursor
        void ReleaseMovingInput() {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            //enableMouseFireCapture = true;
        }

        //void OnApplicationFocus(bool focus) {
        //    if (_capturingInput && !focus)
        //        ReleaseInput();
        //}

        public void ReturnToInitialPosition() {
            transform.localPosition = InitialLocalPositon;
        }


        public void SwitchCameraSpectatorState(bool state) {
            enabled = state;
        }

        // Deprecated
        public void SpectateReplay(Vector3 newPosition, Quaternion newRotation) {
            transform.position = newPosition;
            transform.rotation = newRotation;
            Debug.LogWarning($"Sync newPosition: {newPosition} newRotation: {newRotation}", this);
        }
    }
}