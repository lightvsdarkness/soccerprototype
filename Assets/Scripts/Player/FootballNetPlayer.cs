﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using Soccer;
using UnityEngine;

[Serializable]
public class FootballNetPlayer : MonoBehaviourPun, IPunObservable {
    public PhotonView _PhotonView;
    public FootballPlayer _FootballPlayer;
    //public Photon.Realtime.Player _photonPlayer = null;
    //public Photon.Realtime.Player PhotonPlayer { get { return _photonPlayer; } set { _photonPlayer = value; } }
    //public Photon.Realtime.Player PhotonPlayer { get { return _photonPlayer = PhotonNetwork.PlayerList.First(x => x.IsLocal); } }
    //public Photon.Realtime.Player PhotonPlayer { get { return _photonPlayer = PhotonNetwork.LocalPlayer; } }
    //public GameSettings.Role Role {
    //    get { return _Role = MasterManager.GameSettings.MyRole; }
    //    set { _Role = MasterManager.GameSettings.MyRole = value; }
    //}


    // Syncing
    public string Nickname;
    public byte RoleByte; // 0 - Admin, 1 - Player, 2 - Spectator, 3 - CapturedControl
    public byte Team; // 0 - // 1 - 
    public byte Number;
    public bool IsGoalkeeper;

    public bool ShouldCaptureBall = true;

    // NOT syncing
    public bool ShouldWatchReplay = true;
    

    // Events
    public Action<byte> TeamChanged;
    public Action<byte> NumberChanged;
    public Action<bool> IsGoalkeeperChanged;

    public Action<bool> ShouldWatchReplayChanged;

    private void Start() {
        if (_PhotonView == null)
            _PhotonView = GetComponent<PhotonView>();

        if (_FootballPlayer == null)
            _FootballPlayer = GetComponentInParent<FootballPlayer>();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting)
        {
            stream.SendNext(Nickname);

            stream.SendNext(RoleByte);
            stream.SendNext(Team);
            stream.SendNext(Number);
            stream.SendNext(IsGoalkeeper);

            stream.SendNext(ShouldCaptureBall);
        }
        else if (stream.IsReading)
        {
            Nickname = (string)stream.ReceiveNext();

            RoleByte = (byte)stream.ReceiveNext();
            Team = (byte)stream.ReceiveNext();
            Number = (byte)stream.ReceiveNext();
            IsGoalkeeper = (bool)stream.ReceiveNext();

            ShouldCaptureBall = (bool)stream.ReceiveNext();
        }
    }

    [PunRPC]
    public void RPC_ChangeFNPRole(byte role) {
        Debug.LogWarning($"{_FootballPlayer} RPC_ChangeFNPRole: {role}");
        RoleByte = role;
        // RPC
        _FootballPlayer._PhotonView.RPC("RPC_IsRoleChanged", RpcTarget.AllBufferedViaServer);
    }

    public void RefreshFNPTeam() {
        TeamChanged?.Invoke(Team);
    }
    [PunRPC]
    public void RPC_ChangeFNPTeam(byte teamIndex) {
        //Debug.LogWarning($"{_FootballPlayer} RPC_ChangeFNPTeam: {teamIndex}");
        Team = teamIndex;
        TeamChanged?.Invoke(teamIndex);
    }

    public void RefreshFNPNumber() {
        NumberChanged?.Invoke(Number);
    }
    [PunRPC]
    public void RPC_ChangeFNPNumber(byte soccerplayerNumber) {
        //Debug.LogWarning($"{_FootballPlayer} RPC_ChangeFNPNumber: {soccerplayerNumber}");
        Number = soccerplayerNumber;
        NumberChanged?.Invoke(soccerplayerNumber);
    }

    [PunRPC]
    public void RPC_ChangeFNPIsGoalkeeper(bool isGoalkeeper) {
        //Debug.LogWarning($"{_FootballPlayer} RPC_ChangeFNPIsGoalkeeper: {isGoalkeeper}");
        IsGoalkeeper = isGoalkeeper;
        // RPC
        _FootballPlayer._PhotonView.RPC("RPC_IsGoalkeeperChanged", RpcTarget.AllBufferedViaServer);
        // Local Event
        IsGoalkeeperChanged?.Invoke(isGoalkeeper); // Не нужно, всё сделано через RPC?

        //TeamChanged?.Invoke(Team);
        //NumberChanged?.Invoke(Number);
    }

    [PunRPC]
    public void RPC_ChangeShouldWatchReplay(bool shouldWatchReplay) {
        ShouldWatchReplay = shouldWatchReplay;
        ShouldWatchReplayChanged?.Invoke(shouldWatchReplay);
        //if (shouldWatchReplay) {
            
        //}


    }
}