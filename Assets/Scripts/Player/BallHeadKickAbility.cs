﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;

namespace Soccer {
    public class BallHeadKickAbility : MonoBehaviour {
        [SerializeField] private bool Debugging;

        public Transform HeadTransformFootballPlayer;
        public Transform HeadTransformFootballGoalkeeper;
        public Transform CurrentHeadTransform;

        public float DistanceToCatchBall = 0.6f;

        [Header("Trajectory")]
        //public int SelectedKickTransform = 0;
        //public List<Vector3> KicksTrajectories = new List<Vector3> {new Vector3(0f, 0.05f, 1f), new Vector3(0f, 0.25f, 1f), new Vector3(0f, 0.55f, 1f) };
        public Vector3 ForceVector = new Vector3(0f, 0.05f, 1f);

        [Header("Strength")]
        public int SelectedStrength = 0;
        public List<float> KicksStrengths = new List<float> { 125f, 250f, 380f };
        public float ProjectileForce = 250f; // Float variable to hold the amount of force which we will apply to launch our projectiles

        [Header("Links")]
        public FootballPlayer _FootballPlayer;
        public AbilityCaster _AbilityCaster;
        public Ball _Ball; // Rigidbody variable to hold a reference to our projectile prefab

        [SerializeField] private bool KickAnimationIsDone;


        private void Awake() {
            if (_FootballPlayer == null)
                _FootballPlayer = GetComponentInParent<FootballPlayer>();
            if (_AbilityCaster == null)
                _AbilityCaster = GetComponentInParent<AbilityCaster>();
        }


        public void Initialize(AbilityData abilityData) {
            var projectileAbilityData = abilityData as ProjectileAbilityData;

            ProjectileForce = projectileAbilityData.projectileForce;


            CurrentHeadTransform = HeadTransformFootballPlayer;
            TuneAbilityStrength(0);
            //TuneAbilityTrajectory(0);
        }

        public void TuneAbilityStrength(int strengthLevel) {
            SelectedStrength = strengthLevel;
            _AbilityCaster._UIKickLegController.IndicateChangedStrength(strengthLevel);
        }
        //public void TuneAbilityTrajectory(int trajectoryLevel) {
        //    SelectedKickTransform = trajectoryLevel;
        //    _AbilityCaster._UIKickLegController.IndicateChangedTrajectory(trajectoryLevel);
        //}

        public void TryUseAbility() {
            Debug.Log("BallHeadKickAbility. TryUseAbility");
            if (PhotonRoomMatch.I._Ball.CurrentPlayer != null) return;
            if (transform.position.y < 0.5) return;

            _Ball = null;

            Collider[] hitColliders = Physics.OverlapSphere(CurrentHeadTransform.position, DistanceToCatchBall);
            for (int j = 0; j < hitColliders.Length; j++) {
                if (hitColliders[j].tag == "Ball") {
                    _Ball = hitColliders[j].GetComponent<Ball>();
                }
            }

            if (_Ball == null) return;

            KickAnimationIsDone = false;


            Debug.Log("BallHeadKickAbility. UseAbility", this);

            if (HeadTransformFootballPlayer.gameObject.activeInHierarchy) {
                CurrentHeadTransform = HeadTransformFootballPlayer;
            }
            else {
                CurrentHeadTransform = HeadTransformFootballGoalkeeper;
            }

            //Instantiate a copy of our projectile and store it in a new rigidbody variable called clonedBullet
            ////Rigidbody clonedBullet = Instantiate(Projectile, IdealLegKickTransform.position, transform.rotation) as Rigidbody;

            //if (Debugging)
            //    Debug.Log("UseAbility. KicksTrajectories[SelectedKickTransform]: " + KicksTrajectories[SelectedKickTransform]);
            if (Debugging)
                Debug.Log("UseAbility. SelectedStrength: " + SelectedStrength);

            object[] info = new object[] { _FootballPlayer._FootballNetPlayer.Nickname, ForceVector, KicksStrengths[SelectedStrength] }; //ProjectileForce
            _Ball._PhotonView.RPC("RPC_AlignWithPlayerHead", RpcTarget.AllViaServer, info); //MasterClient //All



            //_Ball.transform.Translate(IdealLegKickTransform.forward * 0.2f);
            //_Ball.GetComponent<Rigidbody>().AddForce(IdealLegKickTransform.transform.forward * ProjectileForce);

            //RaycastHit hit;
            //if (Physics.Raycast(IdealSoccerPlayerLegTransform.position, IdealSoccerPlayerLegTransform.forward, out hit, 1f)) {
            //    _Ball.GetComponent<Rigidbody>()
            //       .AddForceAtPosition(IdealLegKickTransform.transform.forward*ProjectileForce, hit.point, ForceMode.Impulse);
            //}
        }

        //private IEnumerator MoveBallTowardsHead() {
        //    while (!KickAnimationIsDone) {
        //        yield return null;

        //    }

        //}

    }
}