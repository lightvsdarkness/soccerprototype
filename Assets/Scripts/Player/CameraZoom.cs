﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour {
    [SerializeField] private float _zoomFactor = 10f;
    [SerializeField] private float _minzoom = 60f;
    [SerializeField] private float _maxzoom = 120f;
    [SerializeField] private Camera _Camera;


    void Start() {
        if (_Camera == null)
            _Camera = GetComponent<Camera>();

    }


    void Update()
    {
        
    }

    public void Zoom(float zoomInput) {
        var factoredZoomInput = zoomInput * _zoomFactor;
        var clampedNewZoomLevel = Mathf.Clamp(_Camera.fieldOfView += factoredZoomInput, _minzoom, _maxzoom);
        _Camera.fieldOfView = clampedNewZoomLevel;

    }
}
