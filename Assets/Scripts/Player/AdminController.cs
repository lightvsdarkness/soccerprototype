﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using Photon.Realtime;
using Soccer;
using UnityEngine;

namespace Soccer {
    [DisallowMultipleComponent]
    public class AdminController : MonoBehaviour {
        [SerializeField] private bool Debugging;

        public bool InitialObserving;
        [SerializeField] private Vector3 _memorizedCameraPosition;

        [Space]
        public FootballPlayer ControlledPlayer;

        [Header("Stolen Characteristics")]
        public bool StolenGoalKeeperStatus ;
        public byte StolenPlayerNumber;
        public byte StolenTeamNumber;

        [Header("Links")]
        public FootballPlayer MyFootballPlayer;
        [SerializeField] private PhotonView _photonView;

        //public Action<byte> TeamChanged;
        //public Action<byte> NumberChanged;
        //public Action<bool> IsGoalkeeperChanged;

        void Awake() {
            if (_photonView == null)
                _photonView = GetComponent<PhotonView>();
        }

        void Update() {

        }

        public void TrySwitchAssumeDirectControl()
        {
            if (MyFootballPlayer.State == FootballPlayerState.AdminObserving)
                TryRaycastForPlayer();
            else {
                ReleaseDirectControl();
            }
        }

        private void TryRaycastForPlayer()
        {
            Debug.Log("AdminController. TryRaycastForPlayer", this);

            float rayLength = 5000f;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //Debug.DrawRay(ray.origin, ray.direction * rayLength, Color.red);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, rayLength, MasterManager.GameSettings.MaskExceptBorders))
            {
                FootballPlayer footballPlayer = hit.transform.GetComponent<FootballPlayer>();
                if (footballPlayer != null)
                    DoAssumeDirectControl(footballPlayer);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="footballPlayer"></param>
        private void DoAssumeDirectControl(FootballPlayer footballPlayer)
        { 
            Debug.Log("AdminController. AssumeDirectControl", this);

            _memorizedCameraPosition = MyFootballPlayer._cameraSpectator.transform.position;

            StolenGoalKeeperStatus = footballPlayer._FootballNetPlayer.IsGoalkeeper;
            StolenPlayerNumber = footballPlayer._FootballNetPlayer.Number;
            StolenTeamNumber = footballPlayer._FootballNetPlayer.Team;

            // MyFootballPlayer
            MyFootballPlayer.transform.position = footballPlayer.transform.position;
            MyFootballPlayer.transform.rotation = footballPlayer.transform.rotation;

            //_FootballNetPlayer
            MyFootballPlayer._FootballNetPlayer.IsGoalkeeper = StolenGoalKeeperStatus;
            MyFootballPlayer._FootballNetPlayer.Number = StolenPlayerNumber;
            MyFootballPlayer._FootballNetPlayer.Team = StolenTeamNumber;

            // Controlled one
            ControlledPlayer = footballPlayer;
            ControlledPlayer.State = FootballPlayerState.PlayerDeactivatedByAdmin;
            _photonView.RPC("RPC_SendInfoControlledPlayerState", RpcTarget.AllViaServer, ControlledPlayer._FootballNetPlayer.Nickname, FootballPlayerState.PlayerDeactivatedByAdmin);
            // 
            SetControlledPlayerRole(3);

            // Admin
            MyFootballPlayer.State = FootballPlayerState.AdminAssumedControl;
            MyFootballPlayer.enabled = true;

            _photonView.RPC("RPC_SendInfoAdminState", RpcTarget.AllViaServer, FootballPlayerState.AdminAssumedControl);

            // BROADCAST
            MyFootballPlayer._FootballNetPlayer._PhotonView.RPC("RPC_ChangeFNPIsGoalkeeper", RpcTarget.AllBufferedViaServer, StolenGoalKeeperStatus);
            Invoke("DelayedRpcBroadcast", 0.1f);

            // Camera
            SyncCamera();
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReleaseDirectControl() {
            Debug.Log("AdminController. ReleaseDirectControl", this);

            // MyFootballPlayer   Doing it in SetControlledPlayerRole
            //ContolledPlayer.transform.position = MyFootballPlayer.transform.position;
            //ContolledPlayer.transform.rotation = MyFootballPlayer.transform.rotation;


            if (ControlledPlayer != null) {
                // Controlled one
                ControlledPlayer.State = FootballPlayerState.Playing;
                _photonView.RPC("RPC_SendInfoControlledPlayerState", RpcTarget.AllViaServer, ControlledPlayer._FootballNetPlayer.Nickname, FootballPlayerState.Playing);
                // 
                SetControlledPlayerRole(1);

                ControlledPlayer = null;
            }

            // Admin
            MyFootballPlayer.State = FootballPlayerState.AdminObserving;
            _photonView.RPC("RPC_SendInfoAdminState", RpcTarget.AllViaServer, FootballPlayerState.AdminObserving);
            SharpLookAtMyself();
        }

        [ContextMenu("SyncCamera")]
        public void SyncCamera() {
            StartCoroutine(SyncCameraCor());
        }
        //[ContextMenu("SharpLookAtMyself")]
        private void SharpLookAtMyself() {
            MyFootballPlayer.SharpLookAtMyself();
        }
        private IEnumerator SyncCameraCor() {
            SharpLookAtMyself();
            yield return new WaitForSeconds(0.09f);

            MyFootballPlayer._cameraSpectator.transform.position = _memorizedCameraPosition;
            SharpLookAtMyself();
            yield return new WaitForSeconds(0.01f);

            //MyFootballPlayer._cameraSpectator.transform.rotation = Quaternion.LookRotation(ControlledPlayer.transform.position);
            MyFootballPlayer._cameraSpectator.transform.position = _memorizedCameraPosition;
            SharpLookAtMyself();
        }

        // Меняем состояние объекта игрока, у к. перехватили контроль у всех игроков
        [PunRPC]
        public void RPC_SendInfoControlledPlayerState(string controlledPlayerNickname, FootballPlayerState state, PhotonMessageInfo info) {
            // the photonView.RPC() call is the same as without the info parameter. the info.Sender is the player who called the RPC

            // Finding Admin
            var footballPlayer = PhotonRoomMatch.I.FootballPlayers.Find(x => x._FootballNetPlayer.Nickname == controlledPlayerNickname);
            footballPlayer.State = state;
        }

        // Меняем состояние объекта Админа у всех игроков
        [PunRPC]
        public void RPC_SendInfoAdminState(FootballPlayerState state, PhotonMessageInfo info) {
            // the photonView.RPC() call is the same as without the info parameter. the info.Sender is the player who called the RPC

            Debug.LogWarning("RPC_SendInfoAdminState. FootballPlayerState: " + state);

            // Finding Admin
            var footballPlayer = PhotonRoomMatch.I.FootballPlayers.Find(x => x._FootballNetPlayer.Nickname == info.Sender.NickName);
            footballPlayer.State = state;
            footballPlayer.enabled = true;
            footballPlayer._FootballPlayerVisuals.SwitchAdminControlled(state == FootballPlayerState.AdminAssumedControl, StolenTeamNumber);
        }


        /// <summary>
        /// Меняем состояние (и если надо, телепортируем) игрока, у к. захватываем контроль
        /// </summary>
        /// <param name="roleIndex"></param>
        public void SetControlledPlayerRole(int roleIndex)
        {
            if (ControlledPlayer._FootballNetPlayer.RoleByte == 0) return; // Admin can't deny being Admin

            ControlledPlayer._FootballNetPlayer.RoleByte = (byte) roleIndex;
            Debug.Log("SetControlledPlayerRole. Role: " + (GameSettings.Role)roleIndex);

            var photonControlledPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == ControlledPlayer._FootballNetPlayer.Nickname);

            if (photonControlledPlayer == null) {
                PhotonRoomMatch.I.GetFootballNetPlayers();

                photonControlledPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == ControlledPlayer._FootballNetPlayer.Nickname);
            }

            if (photonControlledPlayer != null) {
                ControlledPlayer._FootballNetPlayer._PhotonView.RPC("RPC_ChangeFNPRole", photonControlledPlayer, ControlledPlayer._FootballNetPlayer.RoleByte);

                //object[] posAndRotInfo = new object[] {MyFootballPlayer.transform.position, MyFootballPlayer.transform.rotation}; 
                ControlledPlayer._PhotonView.RPC("RPC_TeleportPlayer", photonControlledPlayer);
            }
            else
                Debug.LogError("PlayerListing. SetPlayerRole: photonPlayer is null");

        }

        private void DelayedRpcBroadcast() {
            Debug.LogWarning("DelayedRpcBroadcast");
            MyFootballPlayer._FootballNetPlayer._PhotonView.RPC("RPC_ChangeFNPTeam", RpcTarget.AllBufferedViaServer, StolenTeamNumber);
            MyFootballPlayer._FootballNetPlayer._PhotonView.RPC("RPC_ChangeFNPNumber", RpcTarget.AllBufferedViaServer, StolenPlayerNumber);
        }
    }
}