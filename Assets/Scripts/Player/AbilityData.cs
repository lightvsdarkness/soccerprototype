﻿using UnityEngine;
using System.Collections;

public abstract class AbilityData : ScriptableObject {
    public string aName = "New Ability";
    public Sprite UISprite;
    public AudioClip aSound;
    public float BaseCoolDown = 1f;

    //public abstract void Initialize(GameObject obj);
    //public abstract void TriggerAbility();
}