﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;


namespace Soccer {
    public class Ball : MonoBehaviourPun, IPunObservable {
        public bool Debugging;
        public static Vector3 BallPosition;
        public static Transform BallTransform;

        public Vector3 _networkPosition;
        public Quaternion _networkRotation;

        [SerializeField] private bool ShouldReturn = true;
        [SerializeField] private Vector3 _startingPosition;
        public Transform PlayerHeadTransform;

        [Header("Links")]
        public PhotonView _PhotonView;

        public FootballPlayer CurrentPlayer;
        public BallTrigger _BallTrigger;
        public BallTrigger _BallTriggerGoalkeeper;

        public SphereCollider BallPhysCollider;
        public Rigidbody _Rigidbody;
        public LineRenderer _LineRenderer;
        public float Distance = 10f;
        public float multiplier = 1;

        [Header("Ball capture availability timer")]
        public float PeriodBallCanBeCapturedAgain;

        public float TimeBallWasPreviouslyCaptured;

        public Vector3 forwardTenMetersPos = Vector3.zero;
        public Quaternion specialRotation = new Quaternion();
        public bool WasKicked = false;
        [SerializeField] private bool _recentlyCaptured;

        void Awake() {
            _startingPosition = transform.position;

            if (_PhotonView == null)
                _PhotonView = GetComponent<PhotonView>();
            if (_BallTrigger == null)
                _BallTrigger = GetComponentInChildren<BallTrigger>();
            if (BallPhysCollider == null)
                BallPhysCollider = GetComponentInChildren<SphereCollider>();
            if (_Rigidbody == null)
                _Rigidbody = GetComponent<Rigidbody>();
            BallTransform = transform;
        }

        private void Start() {
            transform.position = _startingPosition;
        }

        private void Update() {
            if (CurrentPlayer == null) {

                _LineRenderer.enabled = false;

                //if (WasKicked)
                //{
                //    Debug.LogWarning($"forwardTenMetersPos: {forwardTenMetersPos}");
                //    Debug.LogWarning($"transform.position: {transform.position}");

                //    var difference = forwardTenMetersPos - transform.position;
                //    Debug.LogWarning($"difference: {difference}");
                //    var localDifference = transform.InverseTransformPoint(difference);

                //    Debug.LogWarning($"localDifference: {localDifference}");
                //    //transform.localPosition = new Vector3(transform.localPosition.x + localDifference.x, transform.localPosition.x + localDifference.y, transform.localPosition.x + localDifference.z);
                //    transform.rotation = specialRotation;
                //    _Rigidbody.AddRelativeForce(new Vector3(localDifference.x, 0, 0));
                //}

                return;
            }

            if(_PhotonView.IsMine)
                _LineRenderer.enabled = true;

            Ray();
        }

        private void Ray() {
            RaycastHit hit;

            Vector3 forwardVector = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.TransformDirection(Vector3.forward)*Distance;

            if (Physics.Raycast(transform.position, forwardVector, out hit)) {

                if (hit.point != null) {
                    _LineRenderer.SetPosition(1, hit.point);
                }
                else {
                    _LineRenderer.SetPosition(1, transform.position + forwardVector);
                }
                _LineRenderer.SetPosition(0, transform.position);

            }
            forwardTenMetersPos = _LineRenderer.GetPosition(1);
        }

        void FixedUpdate() {
            //    if (!PhotonNetwork.LocalPlayer.IsMasterClient) {
            //        _Rigidbody.useGravity = false;
            //        //return;
            //    }
            //    else
            //        _Rigidbody.useGravity = true;

            if (PhotonNetwork.InRoom) {
                if (PlayerHeadTransform != null) {
                    //_Rigidbody.MovePosition(PlayerHeadTransform.position);
                    _Rigidbody.position = Vector3.Lerp(transform.position, PlayerHeadTransform.position, 0.2f);

                    return;
                }

                if (!_PhotonView.IsMine)
                {
                    if (CurrentPlayer != null)  // When player has ball
                    {
                        if ((CurrentPlayer.PointBall.position - _networkPosition).magnitude > 1) {
                            transform.position = _networkPosition;
                            transform.rotation = _networkRotation;
                        }

                        //transform.position = Vector3.MoveTowards(CurrentPlayer.PointBall.position, _networkPosition, Time.fixedDeltaTime * multiplier);
                        //transform.rotation = Quaternion.RotateTowards(CurrentPlayer.PointBall.rotation, _networkRotation, Time.fixedDeltaTime * multiplier * 3);
                        //transform.position = Vector3.Lerp(CurrentPlayer.PointBall.position, _networkPosition, Time.fixedDeltaTime * multiplier / 3);
                        //transform.rotation = Quaternion.Lerp(CurrentPlayer.PointBall.rotation, _networkRotation, Time.fixedDeltaTime * multiplier);
                    }
                    else // When ball is moving by itself (forces)                        
                    {
                        if ((transform.position - _networkPosition).magnitude > 1)
                        {
                            transform.position = _networkPosition;
                            transform.rotation = _networkRotation;
                        }

                        transform.position = Vector3.MoveTowards(transform.position, _networkPosition, Time.fixedDeltaTime * multiplier);
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, _networkRotation, Time.fixedDeltaTime * multiplier);
                    }
                }
                //else
                //{
                //    if (CurrentPlayer != null) { // && PhotonNetwork.IsMasterClient
                //        transform.position = CurrentPlayer.PointBall.position;
                //        transform.rotation = CurrentPlayer.PointBall.rotation;
                //    }
                //}

                // TEMP SOLUTION
                // IF BALL UNDER PLAYER CONTROL - FOLLOW PLAYER
                if (CurrentPlayer != null) {
                    transform.position = CurrentPlayer.PointBall.position;
                    transform.rotation = CurrentPlayer.PointBall.rotation;
                    if(_recentlyCaptured)
                        _Rigidbody.velocity = Vector3.zero;

                    //transform.position = Vector3.Lerp(transform.position, CurrentPlayer.PointBall.position, Time.fixedDeltaTime * multiplier / 3);
                    //transform.rotation = Quaternion.Lerp(transform.rotation, CurrentPlayer.PointBall.rotation, Time.fixedDeltaTime * multiplier);
                }

                //if(CurrentPlayer == null) // When ball is moving by itself (forces)                        
                //{
                //    if ((transform.position - _networkPosition).magnitude > 1) {
                //        transform.position = _networkPosition;
                //        transform.rotation = _networkRotation;
                //    }

                //    transform.position = Vector3.MoveTowards(transform.position, _networkPosition, Time.fixedDeltaTime * multiplier);
                //    transform.rotation = Quaternion.RotateTowards(transform.rotation, _networkRotation, Time.fixedDeltaTime * multiplier);
                //}
            }

            if (Floor.I.transform.position.y > transform.position.y)
                transform.position = new Vector3(transform.position.x, Floor.I.transform.position.y + 0.1f, transform.position.z);

            BallPosition = transform.position;
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
            if (stream.IsWriting)
            {
                if (CurrentPlayer != null) {
                    stream.SendNext(CurrentPlayer.PointBall.position);
                    stream.SendNext(CurrentPlayer.PointBall.rotation);
                    stream.SendNext(Vector3.zero);
                }
                stream.SendNext(transform.position);
                stream.SendNext(transform.rotation);
                stream.SendNext(_Rigidbody.velocity);
            }
            else
            {
                if (CurrentPlayer != null) {
                    _networkPosition = CurrentPlayer.PointBall.position;
                    _networkRotation = CurrentPlayer.PointBall.rotation;
                    _Rigidbody.velocity = Vector3.zero;
                }

                _networkPosition = (Vector3)stream.ReceiveNext();
                _networkRotation = (Quaternion)stream.ReceiveNext();
                _Rigidbody.velocity = (Vector3)stream.ReceiveNext();

                if (CurrentPlayer == null) {
                    float lag = Mathf.Abs((float)(PhotonNetwork.Time - info.SentServerTime));
                    _networkPosition += (_Rigidbody.velocity * lag);
                }

            }
        }


        public void AlignWithPlayer(FootballPlayer player) {
            var newPhotonPlayer = PhotonNetwork.PlayerList.First(x => x.NickName == player._FootballNetPlayer.Nickname);
            player._PhotonView.RPC("RPC_TurnBallUI", newPhotonPlayer, true); // NOTE: change to photonView?

            _PhotonView.TransferOwnership(newPhotonPlayer);
            WasKicked = false;
            //TimeBallWasPreviouslyCaptured = Time.realtimeSinceStartup; // Maybe shold change because pause maybe can mess it up

            CurrentPlayer = null;
            foreach (var _player in PhotonRoomMatch.I.FootballPlayers) {
                _player._Ball = null;
            }
            CurrentPlayer = player;
            CurrentPlayer._Ball = this;
            BallPhysCollider.enabled = false;
            _Rigidbody.useGravity = false;
            _Rigidbody.velocity = Vector3.zero;

            transform.position = CurrentPlayer.PointBall.position;
            transform.rotation = CurrentPlayer.PointBall.rotation;

            // For ball return from borders
            ShouldReturn = true;
            StopAllCoroutines();

            //if (PhotonNetwork.LocalPlayer.NickName == player._FootballNetPlayer.Nickname) {
                _PhotonView.RPC("RPC_AlignWithPlayer", RpcTarget.AllBufferedViaServer);
                Debug.LogWarning($"Calling RPC_AlignWithPlayer for: {player.name}", this);
            //}

            StartCoroutine(StopVelocity());
        }

        [PunRPC]
        public void RPC_AlignWithPlayer(PhotonMessageInfo info) {
            if (PhotonNetwork.LocalPlayer.NickName == info.Sender.NickName) return;

            //Debug.LogWarning("RPC_AlignWithPlayer", this);

            TimeBallWasPreviouslyCaptured = Time.realtimeSinceStartup; // Maybe shold change because pause maybe can mess it up

            CurrentPlayer = null;
            foreach (var _player in PhotonRoomMatch.I.FootballPlayers){
                _player._Ball = null;
            }
            var player = PhotonRoomMatch.I.FootballPlayers.Find(x => x._FootballNetPlayer.Nickname == info.Sender.NickName); // WORKS ONLY BECAUSE if (PhotonNetwork.LocalPlayer.NickName != contactedFootballPlayer._FootballNetPlayer.Nickname) return; check
            CurrentPlayer = player;
            CurrentPlayer._Ball = this;
            BallPhysCollider.enabled = false;
            _Rigidbody.useGravity = false;
            _Rigidbody.velocity = Vector3.zero;

            transform.position = CurrentPlayer.PointBall.position;
            transform.rotation = CurrentPlayer.PointBall.rotation;

            // For ball return from borders
            ShouldReturn = true;
            StopAllCoroutines();

            StartCoroutine(StopVelocity());
        }

        [PunRPC]
        public void RPC_AlignWithPlayerHead(object[] info) {
            string playerName = (string)info[0];

            FootballPlayer footballPlayer = PhotonRoomMatch.I.FootballPlayers.Find(x => x._FootballNetPlayer.Nickname == playerName);

            PlayerHeadTransform = footballPlayer._AbilityCaster.BallHeadKickAbility.CurrentHeadTransform;
            _Rigidbody.useGravity = false;
            BallPhysCollider.enabled = false;
            _BallTrigger.gameObject.SetActive(false);
            _BallTriggerGoalkeeper.gameObject.SetActive(false);

            var forceVector = (Vector3)info[1];
            var forceMultiplier = (float)info[2];
            //Debug.LogWarning("RPC_AlignWithPlayerHead. forceVector: " + forceVector + " forceMultiplier: " + forceMultiplier, this);
            StartCoroutine(KickBallWithHeadCor(forceVector, forceMultiplier));
        }

        [PunRPC]
        public void RPC_BallCapturedFromCurrentPlayer() {
            if (CurrentPlayer != null) {
                CurrentPlayer._UIBall.SwitchUIBall(false);

                CurrentPlayer._Ball = null;

                CurrentPlayer.BallStayCount = 0f;
            }
            CurrentPlayer = null;
        }

        [PunRPC]
        public void RPC_KickBallWithLeg(object[] info) {

            if (CurrentPlayer != null) {
                CurrentPlayer._UIBall.SwitchUIBall(false);
                // 
                CurrentPlayer._Ball = null;

                CurrentPlayer.BallStayCount = 0f;
            }
            
            var ballPosition = (Vector3) info[0];
            var forceVector = (Vector3) info[1];
            //var forceMultiplier = (float) info[1];

            if(Debugging)
                Debug.LogWarning("RPC_KickBallWithLeg. forceVector: " + forceVector, this);
            //_Rigidbody.AddForce(transform.TransformDirection(forceVector) * forceMultiplier / 20, ForceMode.Force);
            
            //_Rigidbody.position = new Vector3(_Rigidbody.position.x, _Rigidbody.position.y + 0.1f, _Rigidbody.position.z + 0.1f);

            //
            StartCoroutine(KickBallWithLegCor(ballPosition, forceVector));
        }

        private IEnumerator KickBallWithLegCor(Vector3 newBallPosition, Vector3 forceVector) {
            WasKicked = true;
            //yield return new WaitForSeconds(0.3f);
            gameObject.layer = LayerMask.NameToLayer("Ball");
            _BallTrigger.gameObject.layer = LayerMask.NameToLayer("Ball");
            _BallTrigger.DepartPlayer();

            //Debug.LogWarning("CurrentPlayer" +  CurrentPlayer);
            //Debug.LogWarning("transform.position " + transform.position);
            //Debug.LogWarning("ballPosition " + newBallPosition);

            if (CurrentPlayer != null) {
                transform.position = newBallPosition;
                _Rigidbody.rotation = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.rotation;
                specialRotation = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.rotation;
                //_Rigidbody.position = new Vector3(_Rigidbody.position.x, _Rigidbody.position.y + 0.1f, _Rigidbody.position.z + 0.1f);

                yield return null;

                transform.rotation = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.rotation;

                //Debug.LogWarning("KickBallWithLegCor forceVector: " + transform.TransformDirection(forceVector) * forceMultiplier / 20);
                //_Rigidbody.AddForce(transform.TransformDirection(forceVector) * forceMultiplier / 20, ForceMode.Impulse);

                //Debug.LogWarning("KickBallWithLegCor Ball Local forceVector: " + forceVector * forceMultiplier / 20);

                //Debug.LogWarning("KickBallWithLegCor Ball World forceVector: " + transform.TransformDirection(forceVector) * forceMultiplier / 20);
                //Debug.LogWarning("KickBallWithLegCor IdealLegKickTransform World forceVector: " + CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.TransformDirection(forceVector) * forceMultiplier / 20);

                //Debug.LogWarning($"Ball {transform.rotation}");
                //Debug.LogWarning($"IdealLegKickTransform {CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.rotation}");

                //Debug.LogWarning($"NetLocal Ball Rot: {transform.rotation}");
                //Debug.LogWarning($"NetMaster Ball Rot: {_networkRotation}");

                //if (!PhotonNetwork.LocalPlayer.IsMasterClient) {
                //_Rigidbody.AddForce(CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.TransformDirection(new Vector3(0, forceVector.y, forceVector.z)) * forceMultiplier / 20, ForceMode.Impulse);
                //}


                //Vector3 forwardVector = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.forward * 10f;
                //Vector3 forwardVector = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.TransformDirection(Vector3.forward) * 10f;

                //Vector3 globalForwardVector = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.TransformDirection(Vector3.forward) * Distance;
                //var globalForwardVectorNormalized = globalForwardVector.normalized;
                //Debug.LogWarning($"Localized forwardVector: {transform.InverseTransformDirection(globalForwardVector)}");



                //Vector3 globalForceVector = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.TransformDirection(new Vector3(forceVector.x, forceVector.y * forceMultiplier / 40, forceVector.z * forceMultiplier / 20));

                
                Vector3 globalForceVector = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.TransformDirection(new Vector3(0, forceVector.y, forceVector.z));

                // transform.Translate(newGlobalPosition, Space.World) ???
                var globalPoint = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.position + globalForceVector;
                var direction = globalPoint - transform.position;

                //var forwardVector = CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform

                //if (Debugging) Debug.LogWarning($"Localized globalForceVector: {transform.InverseTransformDirection(globalForceVector)}");

                //var GlobalVectorForce = transform.TransformDirection(new Vector3(forceVector.z, forceVector.y, forceVector.z));
                //_Rigidbody.AddForce(globalForceVector, ForceMode.Impulse);
                _Rigidbody.velocity = globalForceVector;

                //Debug.LogWarning("transform.rotation " + transform.rotation);
                //Debug.LogWarning("CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.rotation " + CurrentPlayer._AbilityCaster.BallLegKickAbility.IdealLegKickTransform.rotation);
                
                yield return null;
                CurrentPlayer = null;

                _Rigidbody.useGravity = true;
                BallPhysCollider.enabled = true;

                foreach (var player in PhotonRoomMatch.I.FootballPlayers) {
                    player._Ball = null;
                }
                if (Debugging) Debug.Log("Ball. RPC_KickBallWithLeg end", this);
            }

        }



        private IEnumerator KickBallWithHeadCor(Vector3 forceVector, float forceMultiplier) {
            yield return new WaitForSeconds(0.3f);

            //Debug.LogWarning("KickBallWithHeadCor. local forceVector: " + forceVector + " forceMultiplier: " + forceMultiplier, this);
            //Debug.LogWarning("KickBallWithHeadCor. in world forceVector: " + transform.TransformDirection(forceVector), this);

            //_Rigidbody.position = new Vector3(_Rigidbody.position.x, _Rigidbody.position.y + 0.1f, _Rigidbody.position.z + 0.1f);
            _Rigidbody.AddForce(PlayerHeadTransform.TransformDirection(forceVector) * forceMultiplier / 20, ForceMode.Impulse);
            PlayerHeadTransform = null;

            yield return StartCoroutine(DepartPlayerHeadCor());
        }
        private IEnumerator DepartPlayerHeadCor() {
            WasKicked = true;
            yield return new WaitForSeconds(0.1f);

            _Rigidbody.useGravity = true;
            BallPhysCollider.enabled = true;
            _BallTrigger.gameObject.SetActive(true);
            _BallTriggerGoalkeeper.gameObject.SetActive(true);
        }

        //private void RPC_SyncBallPosRot() {
            
        //}

        private void OnCollisionEnter() {
            WasKicked = false;
        }

        private void OnTriggerEnter(Collider other) {
            if (Debugging)
                Debug.Log("Ball. OnTriggerEnter: " + other.name, this);

            if (ShouldReturn && other.tag == "Borders") {
                ShouldReturn = false;
                StartCoroutine(BallReturnToField(transform.position, other, 5f));
            }
        }
        //[ContextMenu("ReturnToField")]
        //public void ReturnToField() {
        //    Debug.Log("Ball. ReturnToField", this);

        //    BallReturnToField

        //    //RaycastHit hit;
        //    //var direction = ManagerGame.I.Center.position - transform.position;
        //    //Ray ray = new Ray(transform.position, direction);
        //    //Physics.Raycast(ray, out hit, 1000f, MasterManager.GameSettings.MaskBorders);

        //    ////Ray TrueRay = new Ray(hit.point, ManagerGame.I.Center.position - hit.point);

        //    ////Physics.Raycast(ray, out hit, 1f, MasterManager.GameSettings.WithoutBorders);
        //    //transform.position = hit.point + direction.normalized;
        //}

        private IEnumerator BallReturnToField(Vector3 ballPosition, Collider other, float time) {
            if (Debugging) Debug.Log("Ball. BallReturnToField: " + other.name + " from Position: " + ballPosition, this);
            yield return new WaitForSeconds(0.5f);

            if (CurrentPlayer == null) {
                Vector3 closestPoint = other.ClosestPoint(ballPosition);
                var correctedClosestPoint = new Vector3(closestPoint.x, Mathf.Clamp(closestPoint.y, 0.25f, 3f), closestPoint.z);
                var directionNormalized = (ManagerGame.I.Center.position - correctedClosestPoint).normalized;
                //transform.position = closestPoint;
                transform.position = closestPoint + directionNormalized;
                _Rigidbody.velocity = Vector3.zero;
            }

            yield return new WaitForSeconds(0.25f);

            if (CurrentPlayer == null) {
                _Rigidbody.AddForce((ManagerGame.I.Center.position - transform.position).normalized*100);
                //var direction = ManagerGame.I.Center.position - closestPoint;
                //Ray ray = new Ray(transform.position, direction);

                //transform.position = ray.GetPoint(2f);
            }

            yield return new WaitForSeconds(0.25f);

            ShouldReturn = true;
        }

        private IEnumerator StopVelocity() {
            _recentlyCaptured = true;

            //yield return null;
            //transform.position = CurrentPlayer.PointBall.position;
            //transform.rotation = CurrentPlayer.PointBall.rotation;
            //_Rigidbody.velocity = Vector3.zero;

            //yield return new WaitForFixedUpdate();
            //transform.position = CurrentPlayer.PointBall.position;
            //transform.rotation = CurrentPlayer.PointBall.rotation;
            //_Rigidbody.velocity = Vector3.zero;

            //yield return new WaitForFixedUpdate();
            //transform.position = CurrentPlayer.PointBall.position;
            //transform.rotation = CurrentPlayer.PointBall.rotation;
            //_Rigidbody.velocity = Vector3.zero;

            yield return new WaitForSeconds(0.1f);
            _recentlyCaptured = false;
        }
    }
}