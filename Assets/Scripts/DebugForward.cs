﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (LineRenderer))]
public class DebugForward : MonoBehaviour {
    public bool Repeat;
    public Transform Target;
    public Vector3 RotationDelta;
    public float Distance = 10;

    [SerializeField] private LineRenderer lineRend;

    void Start() {
        lineRend = GetComponent<LineRenderer>();
    }


    void Update() {
        Debug.DrawRay(transform.position, transform.forward*Distance, Color.blue);
        if (Repeat) {
            if (Target != null) {
                //transform.position = Target.position;
                //transform.rotation = new Vector3(Target.rotation.x + RotationDelta.x, Target.rotation.y + RotationDelta.y, Target.rotation.z + RotationDelta.z);
                transform.rotation = Target.rotation;
            }
        }

        Ray();
    }

    private void Ray() {
        RaycastHit hit;

        Vector3 forwardVector = transform.TransformDirection(Vector3.forward)*Distance;

        if (Physics.Raycast(transform.position, forwardVector, out hit)) {

            if (hit.point != null) {
                lineRend.SetPosition(1, hit.point);
            }
            else {
                lineRend.SetPosition(1, transform.position + transform.forward*Distance);
            }
            lineRend.SetPosition(0, transform.position);

        }


    }
}