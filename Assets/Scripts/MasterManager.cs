﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Soccer {
    [CreateAssetMenu(menuName = "Singletons/MasterManager")]
    public class MasterManager : SingletonScriptableObject<MasterManager> {
        [SerializeField] private GameSettings _gameSettings;

        public static GameSettings GameSettings {
            get { return I._gameSettings; }
        }

        [SerializeField] private List<NetworkedPrefab> _networkedPrefabs = new List<NetworkedPrefab>();

        public static GameObject NetworkInstantiate(GameObject obj, Vector3 position, Quaternion rotation) {
            foreach (NetworkedPrefab networkedPrefab in I._networkedPrefabs) {
                if (networkedPrefab.Prefab == obj) {
                    if (networkedPrefab.Path != string.Empty) {
                        GameObject result = PhotonNetwork.Instantiate(networkedPrefab.Path, position, rotation);
                        return result;
                    }
                    else {
                        Debug.LogError("Path is empty for gameobject name " + networkedPrefab.Prefab);
                        return null;
                    }
                }
            }

            return null;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void PopulateNetworkedPrefabs() {
#if UNITY_EDITOR
            I._networkedPrefabs.Clear();

            GameObject[] results = Resources.LoadAll<GameObject>("");
            for (int i = 0; i < results.Length; i++) {
                if (results[i].GetComponent<PhotonView>() != null) {
                    string path = AssetDatabase.GetAssetPath(results[i]);
                    I._networkedPrefabs.Add(new NetworkedPrefab(results[i], path));
                }
            }
#endif
        }

    }
}