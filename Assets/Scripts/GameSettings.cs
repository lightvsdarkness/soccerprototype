﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;

namespace Soccer {
    [CreateAssetMenu(menuName = "Manager/GameSettings")]
    public class GameSettings : ScriptableObject {
        public enum TrackingMode {
            None,
            Ball,
            Player,
            Place
        }
        public enum GameMode {
            Play,   // Just playing, doing nothing
            Record, // Playing and recording
            Replay, // Paused, then replaying
            Pause,  // Paused, and doing nothing 
        }
        public enum Role {
            Administrator,
            Player,
            Ghost,
            PlayerDeactivatedByAdmin,
            NotDefined,
        }

        public bool Debugging = false;

        public GameMode _gameMode;

        public GameMode _GameMode {
            get {
                return _gameMode;
            }
            set {
                if(Debugging) Debug.Log("_GameMode set to: " + value, this);
                _gameMode = value;
            }
        }

        [Header("Player")]
        public FootballPlayer MyFootballPlayer;
        public FootballNetPlayer MyFootballNetPlayer;
        //public Role MyRole = Role.NotDefined;

        [Header("Tracking")]
        public GameObject TrackingBeaconPrefab;
        public TrackingMode _trackingMode;

        public TrackingMode _TrackingMode {
            get { return _trackingMode;}
            set {
                _trackingMode = value;
                EventChangedTracking?.Invoke(_trackingMode);
            }
        }

        [Header("PlayerCamera")]
        public ExitGames.Client.Photon.Hashtable _customProperties = new ExitGames.Client.Photon.Hashtable();
        public float CameraDistanceModifier = 0.96f;
        public float CameraDistanceBallModifier = 1f;

        public string FieldCollider = "Floor";

        [Space]
        public bool MenuOpened;

        public bool IsGamePlayed { get { return _GameMode == GameMode.Play || _GameMode == GameMode.Record; } }

        //public bool TrackBall;


        [Space]
        public LayerMask MaskOnlyBorders;
        public LayerMask MaskExceptBorders;

        public int MultiplayerScene1 = 1;

        [SerializeField] private string _gameVersion = "0.0.0";
        public string GameVersion { get { return _gameVersion; } }

        [SerializeField] private string _nickname = "Nickname";
        public string Nickname {
            get {
                return _nickname;
            }
            set {
                _nickname = value;
            }
        }



        [Header("Actions")]
        public UnityAction<TrackingMode> EventChangedTracking;

        public void Initialize() {
            MenuOpened = false;
            _TrackingMode = TrackingMode.None;
            _GameMode = GameMode.Play;
        }

        public void InitMatchStart() {
            SetCameraZoom();
        }

        public void EnforceNewUserSettings() {
            Debug.Log("RPC_EnforceNewUserSettings" , this);
            SetCameraZoom();
        }

        public string RandomizeAndSetNickname(string nickname) {
            int random = Random.Range(0, 9999);
            _nickname = nickname + "_" + random.ToString();
            return _nickname;
        }

        public static int ConvertRoleToInt(GameSettings.Role roleIndex) {
            return (int)roleIndex;
        }


        public void SliderCameraDistanceMod(float cameraDistance) {
            _customProperties["CameraDistanceModifier"] = CameraDistanceModifier = cameraDistance;
            //PhotonNetwork.LocalPlayer.CustomProperties = _customProperties;
            PhotonNetwork.LocalPlayer.SetCustomProperties(_customProperties);

            PlayerPrefsUtils.SaveFloat(PlayerPrefsUtils.CameraDistanceModifier, cameraDistance);
        }
        public void SliderCameraDistanceBallMod(float cameraDistanceBallMod) {
            _customProperties["CameraDistanceBallModifier"] = CameraDistanceBallModifier = cameraDistanceBallMod;
            //PhotonNetwork.LocalPlayer.CustomProperties = _customProperties;
            PhotonNetwork.LocalPlayer.SetCustomProperties(_customProperties);

            PlayerPrefsUtils.SaveFloat(PlayerPrefsUtils.CameraDistanceBallModifier, cameraDistanceBallMod);
        }

        /// <summary>
        /// NOTE: ACHTUNG! Смена админа больше не работает, т.к. у нового админа нет больше CustomProperties["CameraDistanceModifier"];
        /// </summary>
        private void SetCameraZoom()
        {
            if (PhotonNetwork.LocalPlayer.IsMasterClient) {
                //
                if (PlayerPrefsUtils.LoadFloat(PlayerPrefsUtils.CameraDistanceModifier) != null)
                    CameraDistanceModifier = (float)PlayerPrefsUtils.LoadFloat(PlayerPrefsUtils.CameraDistanceModifier);
                else
                    CameraDistanceModifier = 0.96f;
                _customProperties["CameraDistanceModifier"] = CameraDistanceModifier;
                //PhotonNetwork.LocalPlayer.CustomProperties = _customProperties;
                PhotonNetwork.LocalPlayer.SetCustomProperties(_customProperties);

                //
                if (PlayerPrefsUtils.LoadFloat(PlayerPrefsUtils.CameraDistanceBallModifier) != null)
                    CameraDistanceBallModifier = (float) PlayerPrefsUtils.LoadFloat(PlayerPrefsUtils.CameraDistanceBallModifier);
                else
                    CameraDistanceBallModifier = 1f;
                _customProperties["CameraDistanceBallModifier"] = CameraDistanceBallModifier;
                //PhotonNetwork.LocalPlayer.CustomProperties = _customProperties;
                PhotonNetwork.LocalPlayer.SetCustomProperties(_customProperties);
            }
            else
            {
                if(Debugging) Debug.LogWarning("SetCameraZoom. PhotonNetwork.MasterClient: " + PhotonNetwork.MasterClient);
                if (Debugging) Debug.LogWarning("SetCameraZoom. PhotonNetwork.MasterClient.CustomProperties: " + PhotonNetwork.MasterClient.CustomProperties);
                CameraDistanceModifier = (float)PhotonNetwork.MasterClient.CustomProperties["CameraDistanceModifier"];
                CameraDistanceBallModifier = (float)PhotonNetwork.MasterClient.CustomProperties["CameraDistanceBallModifier"];
            }
        }
    }
}