﻿using Photon.Pun;
using UnityEngine;

namespace IG
{
    public class SingletonMonoBehaviourPunCallbacks<T> : MonoBehaviourPunCallbacks where T : Component {
        public bool Debugging;

        protected static T instance;

        public static T I {
            get {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null)
                    {
                        GameObject obj = new GameObject();
                        obj.name = typeof(T).Name;
                        instance = obj.AddComponent<T>();
                    }
                }
                return instance;
            }
        }

        protected virtual void Awake() {
            if (instance == null)
            {
                instance = this as T;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        //protected virtual void Start() {

        //}
    }
}
