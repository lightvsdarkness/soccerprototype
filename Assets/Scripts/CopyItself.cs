﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CopyItself : MonoBehaviour {
    public float PlusSizeX;
    public float PlusSizeY;
    public float PlusSizeZ;
    public int XTimes;

    void Start()
    {
        
    }

    [ContextMenu("CopyX")]
    void CopyX() {
        for (int i = 1; i <= XTimes; i++) {
            Instantiate(this.gameObject,
                new Vector3(transform.position.x + PlusSizeX * i, transform.position.y + PlusSizeY * i, transform.position.z + PlusSizeZ * i), transform.rotation,
                transform.parent);
        }
    }
}
